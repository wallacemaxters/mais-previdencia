## Melhorias

- Colaboradores deverão assinar um POST
- Colocar opção de trocar a imagem do produto
- Colocar a opção de associar o Produto a Categoria
- No painel, trocar Categoria por Categoria do Produto
- Alguns títulos estão em inglês no Painel.
- Atualizar a versão do SLAV utilizado no Painel de Adminstração da TGL
- Trocar o título do Painel de Administração para Gerenciamento da Comunicação
