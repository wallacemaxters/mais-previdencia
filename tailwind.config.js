function adjustBrightness(hex, amount = 0) {

    const parts = hex.match(/\w{2}/g).map(item => {
        const value = parseInt(item, 16)
        const increment = parseInt(value * amount);

        return Math.max(0, Math.min(255, value + increment));
    });

    const result = parts.map(item => item.toString(16).padStart(2, '0').toUpperCase()).join('')

    return `#${result}`;
}

function createColors(baseHex) {

    const colors = Object.fromEntries([50, 100, 200, 300, 400, 500, 600, 700, 800, 900].map(number => {
        const value = ((number / 500) - 1) * -1;
        return [number, adjustBrightness(baseHex, value)]
    }))

    return {
        ...colors,
        'DEFAULT': colors[500]
    }
}

module.exports = {

    content: [
        "./resources/views/**/*.blade.php",
        "./resources/scss/app.scss",
        "./storage/app/temporary/form-*.html"
    ],
    darkMode: 'class', // or 'media' or 'class'

    theme: {

        extend: {

            backgroundImage: {
                'image-glass': "url('/static/img/glass.webp')"
            },


            colors: {
                primary: createColors('f58233'),
                secondary: createColors('292F36'),
            },

            container: {
                padding: {
                    DEFAULT: '1.5rem',
                    'lg' : '4rem',
                    '2xl': '1rem'
                }
            },

            keyframes: {
                beat: {
                    '0%, 100%': {
                        transform: 'scale(1)'
                    },
                    '50%': {
                        transform: 'scale(1.5)'
                    },
                },

                'bg-size': {
                    '0%' : {
                        backgroundSize: '100%'
                    },

                    '100%': {
                        backgroundSize: '150%'
                    }
                }
            },

            animation: {
                beat: 'beat 1s ease-in-out infinite',
                'bg-size': 'bg-size 5s linear infinite alternate'
            }
        },

        fontFamily: {
            sans: ['Ubuntu', 'sans-serif'],
            secondary: ['Montserrat', 'sans-serif'],
        },


        container: {
            screens: {
                sm: '100%',
                md: '100%',
                lg: '1280px'
            }
        }


    },
    plugins: [],
}
