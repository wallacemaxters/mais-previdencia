const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

mix.js('resources/js/app.js', 'static/js/app.js')

mix.sass('resources/scss/app.scss', 'static/css/app.css')
.options({
    processCssUrls: false,
    postCss: [ tailwindcss('./tailwind.config.js') ]
})
.setPublicPath('public')
