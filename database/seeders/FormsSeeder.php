<?php

namespace Database\Seeders;

use App\Models\Form;
use App\Models\FormField;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ViewErrorBag;

class FormsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->tgl();
    }

    public function tgl()
    {
        $form = Form::updateOrCreate(['id' => 1], ['name' => 'FICHA DE ATENDIMENTO / DOCUMENTAÇÃO']);

        $groups[1] = [
            'index' => 1,
            'name' => 'Informações Pessoais',
            'attributes' => ['class' => 'grid grid-cols-1 md:grid-cols-5 gap-6'],
            'fields' => [
                [
                    'name' => 'nome_completo',
                    'type' => 'text',
                    'label' => 'Nome Completo',
                    'attributes' => [
                        'labelClass' => 'md:col-span-2'
                    ]
                ],
                [
                    'name' => 'data_nascimento',
                    'type' => 'text',
                    'label' => 'Data de Nascimento',
                    'attributes' => [
                        'hint'      => 'Ex: 99/99/9999',
                        'data-mask' => '99/99/9999',
                    ],
                    'rules' => ['date_format:d/m/Y']
                ],

                [
                    'name' => 'cpf',
                    'type' => 'text',
                    'label' => 'CPF',
                    'attributes' => [
                        'data-mask' => '999.999.999-99',
                    ],
                    'rules' => ['cpf']
                ],

                [
                    'name' => 'rg',
                    'type' => 'text',
                    'label' => 'RG',
                    'attributes' => [
                    ]
                ]
            ]
        ];

        $groups[2] = [
            'index' => 2,
            'name'  => 'Dados Contato',
            'attributes' => ['class' => 'grid md:grid-cols-3 gap-6'],
            'fields' => [
                [
                    'name' => 'email',
                    'type' => 'email',
                    'label' => 'E-mail',
                ],
                 [
                    'name' => 'telefone',
                    'type' => 'text',
                    'label' => 'Telefone',
                    'attributes' => [
                        'data-mask' => '(99) 9999-9999',
                        'required' => false
                    ],
                    'rules' => ['telefone_com_ddd'],
                ],
                [
                    'name' => 'celular',
                    'type' => 'text',
                    'label' => 'Celular',
                    'attributes' => [
                        'data-mask' => '(99) 99999-9999',
                        'required' => false
                    ],
                    'rules' => ['celular_com_ddd'],
                ]
            ]
        ];


        $groups[3] = [
            'index' => 2,
            'name'  => 'Dados Profissionais',
            'attributes' => ['class' => 'grid md:grid-cols-2 gap-6'],
            'fields' => [
                [
                    'name' => 'crm',
                    'type' => 'text',
                    'label' => 'Nº do CRM',
                ],
                [
                    'name' => 'atividade_profissional',
                    'type' => 'text',
                    'label' => 'Atividade Profissional/Especialidade'
                ]
            ]
        ];


        $groups[4] = [
            'index' => 3,
            'name'  => 'Endereço',
            'attributes' => ['class' => 'grid md:grid-cols-3 gap-6'],
            'fields' => [
                [
                    'name' => 'cep',
                    'type' => 'text',
                    'label' => 'CEP',
                    'rules' => ['formato_cep'],
                    'attributes' => [
                        'hint'  => '32.400-000',
                        'data-mask' => '99.999-999',
                        'data-cep'  => true,

                    ]
                ],
                [
                    'name' => 'logradouro',
                    'type' => 'text',
                    'label' => 'Logradouro',
                    'attributes' => [
                        'data-logradouro' => true,
                    ]
                ],
                [
                    'name' => 'numero',
                    'type' => 'text',
                    'label' => 'Número'
                ],

                [
                    'name' => 'bairro',
                    'type' => 'text',
                    'label' => 'Bairro',
                    'attributes' => [
                        'data-bairro' => true,
                    ]
                ],

                [
                    'name' => 'cidade',
                    'type' => 'text',
                    'label' => 'Cidade',
                    'attributes' => ['data-localidade' => true],
                ],

                [
                    'name' => 'uf',
                    'type' => 'select',
                    'label' => 'Estado',
                    'attributes' => [
                        'data-estado' => true,
                        'items' => config('site.brazil_states')
                    ]
                ]

            ]
        ];

        $groups[5] = [
            'index' => 5,
            'name' => 'NIT/PIS/PASEP',
            'attributes' => ['class' => 'grid md:grid-cols-3 gap-6'],
            'fields' => [
                [
                    'name' => 'nit',
                    'type' => 'text',
                    'label' => 'NIT',
                ],
                [
                    'name' => 'pis',
                    'type' => 'text',
                    'label' => 'PIS',
                ],
                [
                    'name' => 'pasep',
                    'type' => 'text',
                    'label' => 'PASEP',
                ]
            ],
        ];

        $groups[6] = [
            'index' => 6,
            'name'  => 'Documentos Necessários',
            'attributes'  => ['class' => 'grid md:grid-cols-3 gap-6'],
            'fields' => [
                [
                    'name' => 'anexo_procuracao_inss',
                    'label' => 'Procuração INSS devidamente assinada',
                    'type' => 'upload',
                    'attributes' => [
                        'required' => false
                    ]
                ],
                [
                    'name'  => 'anexo_comprovante_residencia',
                    'label' => 'Comprovante de residência recente (máximo 3 meses)',
                    'type'  => 'upload',
                    'attributes' => [
                        'required' => false
                    ]

                ],
                [
                    'name'  => 'anexo_declaracao_irpf',
                    'label' => 'Cópia das duas últimas declarações de IRPF completas',
                    'type'  => 'upload',
                    'attributes' => [
                        'required' => false
                    ]
                ],
                [
                    'name'  => 'anexo_carta_concessao',
                    'label' => 'Se for aposentado, anexar a carta de concessão com a memória de calculo.',
                    'type'  => 'upload',
                    'attributes' => [
                        'required' => false
                    ]
                ],
                [
                    'name'  => 'anexo_comprovante_inscricao_inss',
                    'label' => 'Comprovante de inscrição individual - INSS (se possuir)',
                    'type'  => 'upload',
                    'attributes' => [
                        'required' => false
                    ]
                ],
                [
                    'name'  => 'anexo_pis_pased_nit',
                    'label' => 'Cópia do PIS, PASEP ou NIT',
                    'type' => 'upload',
                    'attributes' => [
                        'required' => false
                    ]
                ],
                [
                    'name'  => 'anexo_ctc',
                    'label' => 'Se emitiu certidão para órgão público, anexar cópia da CTC',
                    'type'  => 'upload',
                    'attributes' => [
                        'required' => false
                    ]
                ],
                [
                    'name'  => 'anexo_crm',
                    'label' => 'Cópia do CRM',
                    'type'  => 'upload',
                    'attributes' => [
                        'required' => false
                    ]

                ]
            ]
        ];

        $fieldId = 1;

        foreach ($groups as $groupId => $item) {

            $groupData = $item;
            unset($groupData['fields']);

            $group = $form->groups()->updateOrCreate(['id' => $groupId], $groupData);

            foreach ($item['fields'] as $itemField) {
                echo $fieldId;
                $field = $group->fields()->firstOrNew(['id' => $fieldId]);
                $field->fill($itemField)->save();
                ++$fieldId;
            }
        }


        view()->share('errors', new ViewErrorBag());

        $html = view('form-data.create', ['form' => $form, 'honeyPot' => ''])->render();

        app()->isLocal() && Storage::disk('local')->put("temporary/form-{$form->id}.html", $html);
    }
}
