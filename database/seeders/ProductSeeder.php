<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!app()->isLocal() && Product::query()->count()) {
         //   return;
        }

        $map_categories = [
            1 => [1, 2, 3, 4, 5]
        ];

        foreach ($this->getProducts() as $item) {
            $product = Product::firstOrNew([
                'id' => $item['id']
            ]);

            $item['content'] = $this->template($item['id']);

            $item['slug']    = Str::slug($item['name']);

            if (empty($item['excerpt'])) {
                $item['excerpt'] = Str::words(strip_tags($item['content']), 30);
            }

            unset($item['template']);

            $product->fill($item)->save();

            $product->categories()->detach();

            foreach ($map_categories as $category_id => $products) {
                if (in_array($product->id, $products)) {
                    $product->categories()->attach($category_id);
                }
            }
        }
    }

    protected function template(string $name): string
    {
        $filename = __DIR__ . '/templates/products/' . $name . '.html';

        return file_exists($filename) ? file_get_contents($filename) : '';
    }

    protected function getProducts(): array
    {
        return (array) json_decode(file_get_contents(__DIR__ . '/json/product.json'), true);
    }

    protected function fakeText()
    {
        $items = [
            'Enfatiza-se que a valorização de fatores subjetivos representa uma abertura para a melhoria da gestão de risco. Pensando mais a longo prazo, o aumento significativo da velocidade dos links de Internet não pode mais se dissociar das direções preferenciais na escolha de algorítimos. Do mesmo modo, a criticidade dos dados em questão apresenta tendências no sentido de aprovar a nova topologia da garantia da disponibilidade.',
            'Percebemos, cada vez mais, que a utilização de recursos de hardware dedicados acarreta um processo de reformulação e modernização dos procedimentos normalmente adotados. Ainda assim, existem dúvidas a respeito de como o uso de servidores em datacenter ainda não demonstrou convincentemente que está estável o suficiente dos equipamentos pré-especificados. A implantação, na prática, prova que o índice de utilização do sistema afeta positivamente o correto provisionamento dos requisitos mínimos de hardware exigidos. O cuidado em identificar pontos críticos no novo modelo computacional aqui preconizado agrega valor ao serviço prestado das ferramentas OpenSource.'
        ];

        return $items[array_rand($items)];
    }
}
