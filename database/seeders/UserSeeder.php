<?php

namespace Database\Seeders;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            1 => [
                'name'      => config('site.title'),
                'email'     => 'contato@grupotgl.com',
                'password'  => bcrypt('357951'),
                'api_token' => '@' . Str::random(78) . '@',
            ],
        ];

        foreach ($items as $id => $item) {
            $user = User::firstOrNew(['id' => $id]);

            $user->exists || $user->fill($item)->save();
        }

        foreach (Employee::doesntHave('user')->get() as $employee) {

            if (! $employee->email) continue;

            $user = User::firstOrNew(['email' => $employee->email], [
                'name'      => $employee->full_name,
                'password'  => bcrypt('@tgl!' . $employee->id),
                'api_token' => Str::random(1) . '$' . hash('sha256', $employee->id)
            ]);

            $user->exists || $user->save();

            $employee->user()->associate($user);

            $employee->save();

            echo "O usuário $user->id foi criado\n";
        }
    }
}
