<?php

namespace Database\Seeders;

use App\Models\Contact;
use App\Models\ContactType;
use Illuminate\Database\Seeder;

class ContactTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            1 => [
                'name' => 'Contato do Site',
            ],
            2 => [
                'name' => 'LP Mais Previdência',
            ],
            3 => [
                'name' => 'LP Mais Previdência Novos Colaboradores'
            ],
            4 => [
                'name' => 'LP TGL Suit'
            ],
            5 => [
                'name' => 'Palestra Cootes'
            ],
            6 => [
                'name' => 'LP Imposto de Renda 2022'
            ]
        ];

        foreach ($items as $id => $item) {
            $type = ContactType::firstOrNew(['id' => $id]);

            $type->exists || $type->fill($item)->save();
        }

        Contact::whereNull('contact_type_id')->where('subject', '=', 'Landing Page Mais Previdência')->update([
            'contact_type_id' => 2,
        ]);

        Contact::whereNull('contact_type_id')->update([
            'contact_type_id' => 1,
        ]);
    }
}
