Quando eu estava lendo sobre alguns frameworks Javascript, como por exemplo o [React](https://github.com/Wildhoney/ReactShadow), li algumas vezes a expressão *shadow DOM* em alguns artigos.

Parece inclusive que é possível, nas configurações de console do Chrome, ativar esse tal de *Shadow Dom*.

[![Shadow DOM no Chrome][1]][1]

Mas o que é *Shadow DOM* e pra que serve?

Tem alguma coisa a ver apenas com o Framework citado acima, ou não?


  [1]: https://i.stack.imgur.com/2XED1.png
