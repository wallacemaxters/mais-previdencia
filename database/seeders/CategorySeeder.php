<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            1 => [
                'name'        => 'Seguros',
                'slug'        => 'seguros',
                'description' => 'Para cuidar de você e de sua família.',
                'video'       => 'categories/servicos-financeiros.mp4',
                //'video_icon'  => 'categories/icones/icone_1.mp4'
            ],
            // 2 => [
            //     'name'        => 'Serviços Contábeis',
            //     'slug'        => 'servicos-contabeis',
            //     'description' => 'Para cuidar do seu patrimônio.',
            //     'video'       => 'categories/servicos-contabeis.mp4',
            //     //'video_icon'  => 'categories/icones/icone_2.mp4'
            // ],
            // 3 => [
            //     'name'        => 'Seguros e Benefícios',
            //     'slug'        => 'seguros',
            //     'description' => 'Para cuidar do seu futuro e de sua família.',
            //     'video'       => 'categories/seguros.mp4',
            //     //'video_icon'  => 'categories/icones/icone_3.mp4'
            // ],
        ];

        foreach ($items as $id => $item) {
            $category = Category::firstOrNew(['id' => $id]);

            (!app()->isLocal() && $category->exists) || $category->fill($item)->save();
        }
    }
}
