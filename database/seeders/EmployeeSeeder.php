<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            1 => [
                'image'      => 'employees/rogerio.webp',
                'first_name' => 'Rogério',
                'last_name'  => 'Araújo',
                'position'   => 'Diretor Comercial',
                'instagram'  => 'https://www.instagram.com/rogeriotgl/',
                'linkedin'   => 'https://www.linkedin.com/in/rogerio-ara%C3%BAjo-5638392b/',
                'email'      => 'diretoria1@grupotgl.com',
                'phone'      => '31991523164',
                'whatsapp'   => '31991523164',

                'order'      => 1,
            ],

            2 => [
                'image'      => 'employees/hesteice.webp',
                'first_name' => 'Hesteice',
                'last_name'  => 'Zatte',
                'position'   => 'Técnica de seguros',
                'email'      => 'hesteice.priscila@grupotgl.com',
                'whatsapp'   => '31992027887',
                'phone'      => '31992027887',

                'order'      => 6,
            ],

            3 => [
                'first_name' => 'Claudia',
                'last_name'  => 'Cupertino',
                'linkedin'   => 'https://www.linkedin.com/in/claudia-cupertino-43a644176',
                'email'      => 'claudia.cupertino@grupotgl.com',
                'phone'      => '31996013061',
                'whatsapp'   => '31996013061',
                'image'      => 'employees/claudia-cupertino.webp',
                'position'   => 'Consultora',
                'order'      => 3,
                // 'skill' => 'Especialista em seguro de responsabilidade civil',
            ],
            4 => [
                'image'      => 'employees/priscila-felix.webp',
                'first_name' => 'Priscila',
                'last_name'  => 'Félix',
                'position'   => 'Consultora',
                'whatsapp'   => '31998542707',
                'phone'      => '31998542707',
                'email'      => 'priscila.felix@grupotgl.com',
                'order'      => 3,
                // Consultora Comercial
            ],

            5 => [
                'image'      => 'employees/bruno-guimaraes.webp',
                'first_name' => 'Bruno',
                'last_name'  => 'Guimarães',
                'linkedin'   => 'https://www.linkedin.com/in/bruno-angelo-cirino-guimar%C3%A3es-a7a8521a3',
                'position'   => 'Consultor',
                'phone'      => '31992084954',
                'whatsapp'   => '31992084954',
                'email'      => 'bruno.guimaraes@grupotgl.com',
                'instagram'  => 'https://www.instagram.com/brunoangelocguimaraes/',
                'order'      => 3,
            ],

            6 => [
                'image'      => 'employees/geraldo-aguiar.webp',
                'first_name' => 'Geraldo',
                'email'      => 'geraldo.aguiar@grupotgl.com',
                'last_name'  => 'Aguiar',
                'position'   => 'Gerente Comercial',
                'phone'      => '31992773421',
                'whatsapp'   => '31992773421',
                'skill'      => 'Especialidade em Auto & R.E',
                'order'      => 2.1,
            ],

            7  => [
                'image' => 'employees/lucas-rodrigues.webp',
                'first_name' => 'Lucas',
                'last_name'  => 'Rodrigues',
                'phone'      => '31983906966',
                'whatsapp'   => '31983906966',
                'position'   => 'Consultor',
                'linkedin'   => 'https://www.linkedin.com/in/lucas-rodrigues-luciano-5703b9140/',
                'email'      => 'lucas.rodrigues@grupotgl.com',
                'order'      => 3.5,
            ],

            8 => [
                'image'      => 'employees/fernando-souza.webp',
                'first_name' => 'Fernando',
                'last_name'  => 'Souza',
                'position'   => 'Consultor',
                'skill'      => 'Especialista em Benefícios',
                'phone'      => '31997119004',
                'whatsapp'   => '31997119004',
                'email'      => 'fernando.souza@grupotgl.com',
                'order'      => 3,
            ],

            9 => [
                'image'      => 'employees/diulyanne-lirio.webp',
                'first_name' => 'Diulyanne',
                'last_name'  => 'Lírio',
                'whatsapp'   => '31984592685',
                'phone'      => '31984592685',
                'linkedin'   => 'https://www.linkedin.com/in/diulyanne-da-paz-lirio-188725223',
                'email'      => 'd.lirio@grupotgl.com',
                'position'   => 'Consultora',
                'order'      => 3,
            ],

            10 => [
                'image'      => 'employees/alex-silva.webp',
                'email'      => 'alex.silva@grupotgl.com',
                'whatsapp'   => '31998908447',
                'phone'      => '31998908447',
                'position'   => 'Consultor',
                'first_name' => 'Alex',
                'last_name'  => 'Silva',
                'order'      => 3,

            ],

            11 => [
                'image'      => 'employees/paula-perucci.webp',
                'email'      => 'paula.borba@grupotgl.com',
                'phone'      => '31986284084',
                'whatsapp'   => '31986284084',
                'first_name' => 'Paula',
                'last_name'  => 'Borba Perucci',
                'position'   => 'Gerente Comercial',
                'order'      => 2,
            ],

            12 => [

                'image'      => 'employees/adriana-guedes.webp',
                'first_name' => 'Adriana',
                'last_name'  => 'Guedes',
                'email'      => 'adriana.guedes@grupotgl.com',
                'position'   => 'Técnica de Seguros',
                'linkedin'   => 'https://www.linkedin.com/in/adriana-guedes-4693904a',
                'whatsapp'   => '31983122400',
                'phone'      => '31983122400',
                'skill'      => 'Especialista em Benefícios',
                'order'      => 6,
            ],

            13 => [
                'image'      => 'employees/fernanda-ribeiro.webp',
                'position'   => 'Consultora',
                'first_name' => 'Fernanda',
                'last_name'  => 'Ribeiro',
                'linkedin'   => 'https://www.linkedin.com/in/fernanda-fernandes-ribeiro-6786b81b2',
                'email'      => 'fernanda.ribeiro@grupotgl.com',
                'phone'      => '31983907292',
                'whatsapp'   => '31983907292',
                'order'      => 3,
            ],

            14 => [
                'image'      => 'employees/lucas-kellen-rodrigues.webp',
                'first_name' => 'Lorena',
                'last_name'  => 'Rodrigues',
                'position'   => 'Consultora',
                'whatsapp'   => '31992670352',
                'phone'      => '31992670352',
                'email'      => 'lorena.rodrigues@grupotgl.com',
                'linkedin'   => 'https://www.linkedin.com/in/lorena-rodrigues-9654301b0/',
                // 'instagram'  => 'https://www.instagram.com/lorenarodrigues.consultora/',
                'instagram' => null,
                'order'      => 3,
            ],

            15 => [
                'image'      => 'employees/leonardo-abdo.webp',
                'first_name' => 'Leonardo',
                'last_name'  => 'Abdo',
                'position'   => 'Consultor',
                'phone'      => '31983907027',
                'whatsapp'   => '31983907027',
                'email'      => 'leonardo.abdo@grupotgl.com',
                'linkedin'   => 'https://www.linkedin.com/in/leonardo-abdo-259336216/',
                'order'      => 3,
            ],

            16   => [
                'image'      => 'employees/george-resende.webp',
                'first_name' => 'George',
                'last_name'  => 'Resende',
                'whatsapp'   => '31983152699',
                'phone'      => '31983152699',
                'linkedin'   => 'http://www.linkedin.com/in/george-resende',
                'position'   => 'Consultor',
                'email'      => 'george.resende@grupotgl.com',
                'order'      => 3,
            ],

            17 => [
                'image'      => 'employees/kenia.webp',
                'first_name' => 'Kênia',
                'last_name'  => 'Araújo',
                'position'   => 'Consultor',
                'email'      => 'kenia.araujo@grupotgl.com',
                'phone'      => '31984833077',
                'whatsapp'   => '31984833077',
                'linkedin'   => 'https://www.linkedin.com/in/kenia-ara%C3%BAjo-7a8b051b3/',
                'order'      => 3,
            ],

            18 => [
                'image'      => 'employees/pamela.webp',
                'email'      => 'pamela.leite@grupotgl.com',
                'first_name' => 'Pamela',
                'last_name'  => 'Zimbra Leite',
                'whatsapp'   => '31993110190',
                'phone'      => '31993110190',
                'position'   => 'Consultora',
                'linkedin'   => 'https://www.linkedin.com/in/pamela-zimbra-leite-ab0766220/',
                'order'      => 3,
            ],

            19 => [
                'first_name' => 'Charlison',
                'last_name'  => 'Almeida',
                'image'      => 'employees/charlison.webp',
                'position'   => 'Consultor',
                'email'      => 'charlison.carvalho@grupotgl.com',
                'whatsapp'   => '31983114938',
                'phone'      => '31983114938',
                'linkedin'   => 'https://www.linkedin.com/in/charlison-carvalho-58a88b228/',
                'order'      => 3,
            ],

            20 => [
                'image'      => 'employees/ana-luiza.webp',
                'first_name' => 'Ana Luiza',
                'last_name'  => 'Fernandes',
                'position'   => 'Consultora',
                'whatsapp'   => '31993124842',
                'phone'      => '31993124842',
                'linkedin'   => 'https://www.linkedin.com/in/ana-luiza-fernandes-17531b185/',
                'email'      => 'analuiza.fernandes@grupotgl.com',
                'instagram'  => 'https://www.instagram.com/anafe.consultora/',
                'order'      => 3.0,
            ],

            21 => [
                'image'      => 'employees/bruno-cadeu.webp',
                'phone'      => '31973025159',
                'whatsapp'   => '31973025159',
                'first_name' => 'Bruno',
                'last_name'  => 'Cadeu',
                'instagram'  => 'https://www.instagram.com/brunocadeu.consultor/',
                'email'      => 'bruno.cadeu@grupotgl.com',
                'linkedin'   => 'https://www.linkedin.com/in/bruno-cadeu-a750b0155/',
                'position'   => 'Consultor',


                'order' => 3,
            ],

            22 => [
                'first_name' => 'Nádia',
                'last_name'  => 'Silva',
                'whatsapp'   => '31999523471',
                'phone'      => '31999523471',
                'position'   => 'Gerente de Relacionamento',
                'email'      => 'gerencia@tglcontabil.com',
                'order'      => 2.3,
                'image'      => 'employees/nadia-silva.webp',
            ],

            23 => [
                'first_name' => 'César',
                'last_name'  => 'Dias',
                'position'   => 'Coord. de Legalização',
                'phone'      => '31984402933',
                'whatsapp'   => '31984402933',
                'image'      => 'employees/cesar-dias.webp',
                'order'      => 4
            ],

            24 => [
                'first_name' => 'Bárbara',
                'last_name'  => 'Saragoça',
                'whatsapp'   => '31983906677',
                'phone'      => '31983906677',
                'position'   => 'Coord. Seguros',
                'image'      => 'employees/barbara-saragoca.webp',
                'order'      => 4,
            ],

            25 => [

                'first_name' => 'Luana',
                'last_name'  => 'Gomes',
                'position'   => 'Relacionamento Cliente',
                'whatsapp'   => '3199930597',
                'phone'      => '3199930597',
                'email'      => 'relacionamento1@tglcontabil.com.br',
                'image'      => 'employees/luana-gomes.webp',
                'order'      => 5,
            ],


            26 => [
                'first_name' => 'Gilberty',
                'last_name'  => 'Vilele',
                'email'      => 'gerencia.contabil@tglcontabil.com.br',
                'phone'      => '3197611658',
                'whatsapp'   => '3197611658',
                'position'   => 'Gerente Contábil',
                'image'      => 'employees/gilberty-vilele.webp',
                'order'      => 2.99,
            ],

            27 => [
                'first_name' => 'Tatiane',
                'last_name'  => 'Barbosa',
                'position'   => 'Gerente de Investimentos',
                'image'      => 'employees/tatiane-barbosa.webp',
                'order'      => 2.3,
                'phone'   => '11989639100',
                'whatsapp'   => '11989639100',
                'linkedin'   => 'https://www.linkedin.com/in/tatiane-barbosa-b70808aa',
                'email'      => 'gerenciainvestimento@grupotgl.com',
            ],

            28 => [
                'first_name' => 'Luana',
                'last_name'  => 'Gonçalves',
                'image'      => 'employees/luana-goncalves.webp',
                'position'   => 'Relacionamento Cliente',
                'order'      => 5,
                'email'      => 'notafiscal@tglcontabil.com',
                'phone'      => '3199930597',
                'whatsapp'   => '3199930597',
                'linkedin'   => 'http://linkedin.com/in/luana-aparecida-gon%C3%A7alves-do-amparo-b75563144',

            ],

            29 => [
                'first_name' => 'Sarahyane',
                'last_name'  => 'Oshima',
                'phone'      => '31986487424',
                'whatsapp'   => '31986487424',
                'position'   => 'Supervisora Dep. Fiscal',
                'linkedin'   => 'http://linkedin.com/in/sarahyane-oshima-52541118b',
                'email'      => 'fiscal2@tglcontabil.com.br',
                'image'      => 'employees/sarahyane.webp',
                'order'      => 3
            ],

            30 => [
                'image'      => 'employees/wesley-alves.webp',
                'position'   => 'Coordenador Contábil',
                'first_name' => 'Wesley',
                'last_name'  => 'Alves',
                'order'      => 4,
                'email'      => 'coord.contabil@tglcontabil.com',
                'phone'      => '31996095676',
                'whatsapp'   => '31996095676',
            ],

            31 => [

                'image'      => 'employees/marcelo-barroso.webp',
                'first_name' => 'Marcelo',
                'last_name'  => 'Barroso',
                'position'   => 'Consultoria Tributária PF',
                'order'      => 9,
                'linkedin'   => 'https: //www.linkedin.com/in/marcelo-augusto-1a8973190',
                'email'      => 'coord.ir@tglcontabil.com.br',
                'whatsapp'   => '31997199902',
                'phone'      => '31997199902',
            ],

            32 => [
                'first_name' => 'Andreza',
                'last_name'  => 'Zanandrez',
                'image'      => 'employees/andreza-zanandrez.webp',
                'order'      => 5,
                'position'   => 'Relacionamento Cliente',
                'email'      => 'relacionamento@tglcontabil.com.br',
                'phone'      => '31984720231',
                'whatsapp'      => '31984720231'
            ],

            33 => [
                'first_name' => 'Leonardo',
                'last_name'  => 'Cândido',
                'image'      => 'employees/leonardo-candido.webp',
                'position'   => 'Advogado',
                'email'      => 'juridico@tglcontabil.com.br',
                'phone'      => '31991466335',
                'whatsapp'   => '31991466335',
                'order'      => 8
            ],

            34 => [
                'first_name' => 'Viviane',
                'last_name'  => 'Barbosa',
                'email'      => 'viviane.barbosa@tglcontabil.com.br',
                'phone'      => '31997092233',
                'whatsapp'   => '31997092233',
                'position'   => 'Diretora Administrativa',
                'order'      => 1.1,
                'image'      => 'employees/viviane.webp',
            ],

            35 => [
                'first_name' => 'Marcela',
                'last_name'  => 'Barbosa',
                'position'   => 'Comercial',
                'phone'      => '31993110182',
                'whatsapp'   => '31993110182',
                'email'      => 'marcela.rocha@grupotgl.com',
                'image'      => 'employees/marcela.webp',
                'order'      => 3.4
            ]
        ];


        foreach ($items as $id => $item) {
            $item['slug'] = Str::slug($item['first_name'] . ' ' . $item['last_name']);

            $model = Employee::firstOrNew(['id' => $id]);

            $model->exists || $model->fill($item)->save();
        }
    }
}
