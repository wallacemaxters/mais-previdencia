<?php

namespace Database\Seeders;

use App\Models\Post;
use DOMDocument;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class WPPostsSeeder extends Seeder
{
    public function run()
    {
        $importedPosts = include __DIR__ . '/posts.php';

        foreach ($importedPosts as $item) {
            $item['is_draft'] = false;

            $post = Post::firstOrNew(['id' => $item['id']]);

            $item['slug'] = Arr::last(explode('/', $item['slug']));

            $srcReplaces = $this->imageReplaceAndDownload($item['content']);

            $item['content'] = strtr($item['content'], $srcReplaces);

            $post->fill($item)->save();

            $post->cover && $this->fetchImage($post->cover);
        }
    }

    public function fetchImage(string $path)
    {
        $url = 'https://grupotgl.com/storage/' . $path;

        $disk = Storage::disk('public');

        if ($disk->exists($path)) {
            return;
        }

        $disk->put($path, (string) Http::get($url)->getBody());

        echo $path, PHP_EOL;
    }

    public function imageReplaceAndDownload(string $html): array
    {
        $dom = new DOMDocument();

        @$dom->loadHTML($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $replaces = [];

        foreach ($dom->getElementsByTagName('img') as $img) {
            $src = Str::replaceFirst('/storage/', '', $img->getAttribute('src'));

            $srcDecoded = urldecode($src);

            $this->fetchImage($srcDecoded);

            $replaces[$src] = $srcDecoded;
        }

        return $replaces;
    }
}
