<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->text(30);

        $image = $this->faker->image(storage_path('app/public/posts'));


        return [
            'title'     => $title,
            'slug'      => Str::slug($title),
            'content'   => $this->faker->realText(2000),
            'excerpt'   => $this->faker->realText(170),
            'cover'     => 'posts/' . basename($image),
            'posted_at' => $this->faker->date(),
            'user_id'   => User::factory()->create(),
            'is_draft'  => false
        ];
    }
}
