<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence(3);

        $image = $this->faker->image(storage_path('app/public/products'), 400, 400, 'products');

        return [
            'name'    => $title,
            'slug'    => Str::slug($title),
            'excerpt' => $this->faker->paragraph(5),
            'content' => $this->faker->text(400),

            'image' => basename($image)

        ];
    }
}
