<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactDatasheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_datasheets', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('birth_date');
            $table->string('cpf', 11)->index();
            $table->string('rg');
            $table->string('phone', 11);
            $table->string('email');
            $table->string('zip_code', 8);
            $table->char('state', 2)->index();
            $table->string('street');
            $table->string('district');
            $table->string('city');
            $table->string('crm');
            $table->string('position');
            $table->string('nit_number');
            $table->string('pis_number');
            $table->string('pasep_number');
            $table->enum('ir_type', ['C', 'S'])->comment('C para Completo, S para simplificado');
            $table->boolean('has_private_pension_contribution');
            $table->integer('number_of_children');
            $table->string('childrens_age');
            $table->decimal('monthly_income', 12, 2);
            $table->string('procuracao_inss_attachment')->nullable();
            $table->string('residency_proof_attachment')->nullable();
            $table->string('last_irpf_attachment')->nullable();
            $table->string('retired_concession_letter_attachment')->nullable();
            $table->string('enrollment_proof_inss_attachment')->nullable();
            $table->string('pasep_or_pis_or_nit_attachment')->nullable();
            $table->string('ctc_attachment')->nullable();
            $table->string('crm_attachment')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_datasheets');
    }
}
