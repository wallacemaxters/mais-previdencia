<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterContactDatasheetsTableAddNullables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact_datasheets', function (Blueprint $table) {
            $table->string('street')->nullable()->change();
            $table->string('district')->nullable()->change();
            $table->string('city')->nullable()->change();
            $table->string('crm')->nullable()->change();
            $table->string('position')->nullable()->change();
            $table->string('nit_number')->nullable()->change();
            $table->string('pis_number')->nullable()->change();
            $table->string('pasep_number')->nullable()->change();
        });
    }
}
