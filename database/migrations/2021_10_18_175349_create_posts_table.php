<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('posts');

        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->unique();
            $table->timestamp('posted_at')->nullable();
            $table->foreignId('user_id')->nullable()->constrained('users');
            $table->string('title');
            $table->text('excerpt')->nullable();
            $table->mediumText('content');
            $table->string('cover')->nullable();
            $table->unsignedBigInteger('views_count')->default(0);
            $table->boolean('is_draft')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
