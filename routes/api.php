<?php

use App\Http\Controllers\Api\CategoriesController;
use App\Http\Controllers\Api\ContactsController;
use App\Http\Controllers\Api\EmployeesController;
use App\Http\Controllers\Api\FormDatasController;
use App\Http\Controllers\Api\FormFieldsController;
use App\Http\Controllers\Api\FormGroupsController;
use App\Http\Controllers\Api\FormsController;
use App\Http\Controllers\Api\PostsController;
use App\Http\Controllers\Api\ProductsController;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\ContactDatasheetController;
use App\Http\Controllers\Api\ContactTypesController;
use Illuminate\Support\Facades\Route;

Route::get('users/me', [UsersController::class, 'me']);
Route::post('users/login', [UsersController::class, 'login']);

Route::group(['middleware' => 'auth:api'], function () {
    Route::resource('categories', CategoriesController::class)->parameters([
        'categories' => 'category'
    ]);

    Route::post('posts/upload-cover', [PostsController::class, 'uploadCover']);
    Route::patch('posts/{post}/draft', [PostsController::class, 'draft']);

    Route::apiResource('forms', FormsController::class);
    Route::get('forms/{form}/form-groups', [FormsController::class, 'formGroups']);
    Route::get('forms/{form}/form-fields', [FormsController::class, 'formFields']);
    Route::apiResource('form-groups', FormGroupsController::class);
    Route::apiResource('form-fields', FormFieldsController::class);
    Route::apiResource('form-datas', FormDatasController::class);
    Route::apiResource('posts', PostsController::class);
    Route::apiResource('contacts', ContactsController::class)->except(['create', 'update']);
    Route::apiResource('contact-types', ContactTypesController::class)->only('index');
    Route::apiResource('products', ProductsController::class);
    Route::apiResource('employees', EmployeesController::class);
    Route::apiResource('contact-datasheets', ContactDatasheetController::class);

    Route::apiResource('users', UsersController::class);
});
