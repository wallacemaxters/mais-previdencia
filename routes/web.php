<?php

use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\ContactsController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\FormDataController;

Route::get('/', [PagesController::class, 'index'])->name('pages.home');

Route::get('/sobre', [PagesController::class, 'about'])->name('pages.about');
Route::get('/aviso-sobre-privacidade', [PagesController::class, 'aboutPrivacity'])->name('pages.about_privacity');

Route::get('/aviso-sobre-cookies', [PagesController::class, 'aboutCookies'])->name('pages.about_cookies');

// Route::get('/idap', [PagesController::class, 'idap'])->name('pages.idap');

Route::get('mais-previdencia', [PagesController::class, 'maisPrevidencia'])->name('pages.mais_previdencia');
Route::get('mais-previdencia-novos-colaboradores', [PagesController::class, 'maisPrevidenciaNovosColaboradores'])->name('pages.mais_previdencia_novos_colaboradores');

// Route::get('cootes', [PagesController::class, 'cootes']);
// Route::get('ficha-atendimento-cootes', [PagesController::class, 'contactCootes'])->name('pages.contact_cootes');

// Route::get('ir', [PagesController::class, 'ir']);

Route::get('blog', [PostsController::class, 'index'])->name('posts.index');
Route::get('blog/{slug}', [PostsController::class, 'showBySlug'])->name('posts.show_by_slug');

Route::get('produto/{slug}', [ProductsController::class, 'showBySlug'])->name('products.show_by_slug');
Route::get('produtos', [ProductsController::class, 'index'])->name('products.index');

Route::get('categoria/{slug}', [CategoriesController::class, 'showBySlug'])->name('categories.show_by_slug');

Route::get('formulario/{form}', [FormDataController::class, 'create']);
Route::post('formulario/{form}', [FormDataController::class, 'store']);

Route::get('contato', [ContactsController::class, 'create'])->name('contacts.create');
Route::post('contato', [ContactsController::class, 'store'])
    ->middleware('antispam')
    ->name('contacts.store');

Route::post('contato/mais-previdencia', [ContactsController::class, 'storeMaisPrevidencia'])
    ->middleware('antispam')
    ->name('contacts.store_mais_previdencia');


Route::post(
    'contato/mais-previdencia-novos-colaboradores',
    [ContactsController::class, 'storeMaisPrevidenciaNovosColaboradores']
)
->middleware('antispam')
->name('contacts.store_mais_previdencia_novos_colaboradores');


// Route::post('contato/cootes', [ContactsController::class, 'storeCootes'])->name('contacts.store_cootes');

// Route::post('contact-datasheets', [ContactDatasheetController::class, 'store'])->name('contact_datasheets.store');
// Route::get('contact-datasheets/{contactDatasheet}/file/{name:string}', [ContactDatasheetController::class, 'file'])
// ->name('contact_datasheets.file');

Route::get('colaboradores', [EmployeesController::class, 'index'])->name('employees.index');

// Route::get('form-data/{formData}/file/{name:string}', [FormDataController::class, 'file'])->name('form_data.file');


Route::get('/sitemap.xml', function () {
    $urls = (function () {
        $routes = [
            'pages.home',
            'pages.idap',
            'pages.about',
            'products.index',
            'posts.index',
            'contacts.create',
        ];

        foreach ($routes as $route) {
            yield route($route);
        }

        yield from Category::select(['slug'])->cursor()->map(fn ($item) => route('categories.show_by_slug', $item->slug));
        yield from Product::select(['slug'])->cursor()->map(fn ($item) => route('products.show_by_slug', $item->slug));
    })();


    $posts = \App\Models\Post::published()->where('is_draft', false)->cursor();

    $sitemap = view('sitemap', compact('posts', 'urls'))->render();

    return response($sitemap, 200, ['content-type' => 'application/xml']);
});
