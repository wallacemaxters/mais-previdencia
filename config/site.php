<?php

return [

    'title'             => 'Mais Previdência',
    'description'       => 'A Mais Previdência é a marca comercial da CASFAM (Caixa de Assistência e Previdência Fábio de Araújo Motta), entidade de previdência complementar fechada com mais de 40 anos, com planos patrocinados e instituídos pelas empresas que compõem a FIEMG (Federação das Indústrias do Estado de Minas Gerais).',
    'short_description' => 'A Mais Previdência é a marca comercial da CASFAM (Caixa de Assistência e Previdência Fábio de Araújo Motta), entidade de previdência complementar fechada com mais de 40 anos, com planos patrocinados e instituídos pelas empresas que compõem a FIEMG (Federação das Indústrias do Estado de Minas Gerais).',

    'address'           => 'Rua Bernardo Guimarães, 63 - Funcionários Belo Horizonte, MG - 30140-080',

    'emails' => [
        'contact' => 'contato@maisprevidencia.com.br'
    ],

    'phones' => [
        'whatsapp' => [
            'corretora' => '666666666666',
            'contabil'  => '444444444444',
        ],
        'landline' => '(31) 3284-8407',
    ],


    'brazil_states' => [
        'AC' => 'Acre',
        'AL' => 'Alagoas',
        'AP' => 'Amapá',
        'AM' => 'Amazonas',
        'BA' => 'Bahia',
        'CE' => 'Ceará',
        'DF' => 'Distrito Federal',
        'ES' => 'Espirito Santo',
        'GO' => 'Goiás',
        'MA' => 'Maranhão',
        'MS' => 'Mato Grosso do Sul',
        'MT' => 'Mato Grosso',
        'MG' => 'Minas Gerais',
        'PA' => 'Pará',
        'PB' => 'Paraíba',
        'PR' => 'Paraná',
        'PE' => 'Pernambuco',
        'PI' => 'Piauí',
        'RJ' => 'Rio de Janeiro',
        'RN' => 'Rio Grande do Norte',
        'RS' => 'Rio Grande do Sul',
        'RO' => 'Rondônia',
        'RR' => 'Roraima',
        'SC' => 'Santa Catarina',
        'SP' => 'São Paulo',
        'SE' => 'Sergipe',
        'TO' => 'Tocantins',
    ]
];
