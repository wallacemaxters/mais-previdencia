<?php

namespace App\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name' => ['required', 'string', 'max: 25'],
            'last_name'  => ['required', 'string', 'max: 50'],
            'position'   => ['required', 'string'],
            'phone'      => ['required'],
            'linkedin'   => ['required', 'url'],
            'whatsapp'   => ['required'],
            'instagram'  => ['required', 'url'],
            'skill'      => ['nullable', 'string'],
        ];
    }
}
