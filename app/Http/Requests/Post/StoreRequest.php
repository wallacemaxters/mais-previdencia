<?php

namespace App\Http\Requests\Post;

use App\Models\Post;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'content'   => ['required', 'string'],
            'title'     => ['required', 'string'],
            'excerpt'   => ['required', 'string', 'max:170'],
            'slug'      => [
                'required',
                'unique' => Rule::unique(Post::class, 'slug'),
                'regex' => 'regex: /^[a-zA-Z0-9\-]+$/'
            ],
            'posted_at' => ['required', 'date'],
            'is_draft'  => ['required', 'boolean'],
            // 'tags'      => ['required', 'array'],
            // 'tags.*'    => ['required', 'numeric', 'exists:tags,id'],
            'cover'     => [
                'required',
                static function ($_, $value, $failed) {
                    Storage::disk('public')->exists($value) || $failed('Erro ao selecionar a imagem');
                }
            ]
        ];
    }
}
