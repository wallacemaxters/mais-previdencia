<?php

namespace App\Http\Requests\Post;

class UpdateRequest extends StoreRequest
{

    public function rules()
    {
        $rules = parent::rules();

        $rules['slug']['unique']->ignore($this->route('post.id'));

        return $rules;
    }
}
