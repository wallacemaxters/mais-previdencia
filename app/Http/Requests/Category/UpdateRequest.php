<?php

namespace App\Http\Requests\Category;

class UpdateRequest extends StoreRequest
{
    public function rules()
    {
        $rules = parent::rules();

        $rules['slug']['unique']->ignore($this->route('category'));

        return $rules;
    }
}
