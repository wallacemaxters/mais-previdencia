<?php

namespace App\Http\Requests\Category;

use App\Models\Category;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'        => ['required', 'string'],
            'description' => ['required', 'string'],
            'slug'        => ['required', 'unique' => Rule::unique(Category::class, 'slug')]
        ];
    }
}
