<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostsController extends Controller
{
    public function index(Request $request)
    {
        $searchTerm = $request->get('q');

        $search = static function ($query) use ($searchTerm) {
            if ($searchTerm) {
                $value = "%{$searchTerm}%";
                $query->where('content', 'LIKE', $value);
                $query->orWhere('title', 'LIKE', $value);
            }
        };

        $posts = Post::latest('posted_at')->published()->where($search)->paginate();

        return view('posts.index', compact('posts', 'searchTerm'));
    }

    protected function renderViewModel(Post $post)
    {
        if (! $post->is_draft) {
            $post->timestamps = false;
            $post->increment('views_count');
            $post->timestamps = true;
        }

        $OG = [
            'description' => $post->excerpt,
            'image'       => $post->cover_url,
        ];

        return view('posts.show', compact('post', 'OG'))->render();
    }

    public function show(int $id)
    {
        $post = Post::findOrFail($id);

        return $this->renderViewModel($post);
    }

    public function showBySlug(string $slug)
    {
        $post = Post::where(compact('slug'))->firstOrFail();

        return $this->renderViewModel($post);
    }
}
