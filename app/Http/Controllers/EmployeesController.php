<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeesController extends Controller
{
    public function index()
    {
        $employees = Employee::oldest('order')
                             ->oldest('first_name')
                             ->get();

        return view('employees.index', ['employees' => $employees]);
    }

    public function showBySlug(string $slug)
    {
        $employee = Employee::where(['slug' => $slug])->firstOrFail();

        return view('employees.show', ['employee' => $employee]);
    }
}
