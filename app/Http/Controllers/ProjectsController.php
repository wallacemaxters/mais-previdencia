<?php

namespace App\Http\Controllers;

use App\Models\Project;

class ProjectsController extends Controller
{
    public function index()
    {
        $projects = Project::paginate();

        return view('projects.index', compact('projects'))->render();
    }
}
