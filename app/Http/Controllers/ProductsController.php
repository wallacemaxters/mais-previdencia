<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::get();

        return view('products.index', compact('products'));
    }

    public function showBySlug(string $slug)
    {
        $product = Product::where(['slug' => $slug])->firstOrFail();

        return view('products.show', compact('product'));
    }
}
