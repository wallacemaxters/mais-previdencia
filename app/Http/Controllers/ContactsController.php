<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Swift_TransportException;
use Illuminate\Validation\Rule;
use App\Http\Middleware\AntiSpam;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ContactsController extends Controller
{
    public function create()
    {
        return view('contacts.create', [
            'honeyPot' => AntiSpam::generateHoneyPot()
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name'    => ['required', 'string'],
            'email'   => ['required', 'email'],
            'phone'   => ['required', 'string', 'celular_com_ddd'],
            'subject' => ['required', 'string', 'max:300'],
            'message' => ['required', 'string'],
        ]);

        $data['phone'] = preg_replace('/\D+/', '', $data['phone']);
        $data['contact_type_id'] = 1;

        $contact = Contact::create($data);

        try {
            Mail::to('contato@grupotgl.com')->send(new ContactMail($contact));
        } catch (Swift_TransportException $e) {
            Log::error($e);
        }

        return redirect()->back();
    }

    public function storeMaisPrevidencia(Request $request)
    {
        $data = $request->validate([
            'name'    => ['required', 'string'],
            'email'   => ['required', 'email', 'unique:contacts'],
            'phone'   => ['required', 'string', 'celular_com_ddd'],
        ]);

        $data = [
            'contact_type_id' => 2,
            'subject' => 'Landing Page Mais Previdência',
            'message' => 'Contato enviado através da landing page do Mais Previdência',
            'phone' => preg_replace('/\D+/', '', $data['phone'])
        ] + $data;

        $contact = Contact::create($data);

        try {
            Mail::to('manutencaodebeneficios@grupotgl.com')
                ->send(new ContactMail($contact));
        } catch (Swift_TransportException $e) {
            Log::error($e);
        }

        return redirect()->back()->with(['success' => true]);
    }


    public function storeMaisPrevidenciaNovosColaboradores(Request $request)
    {
        $data = $request->validate([
            'name'                => ['required', 'string'],
            'email'               => ['required', 'email', 'unique:contacts'],
            'phone'               => ['required', 'string', 'celular_com_ddd'],
            'previdencia_privada' => ['required', 'in:0,1']
        ]);

        $data = [
            'contact_type_id' => 3,
            'subject'         => 'Landing Page Mais Previdência de Novos Colaboradores',
            'message'         => sprintf(
                'Já investe em previdência? %s',
                $data['previdencia_privada'] == '1' ? 'Sim' : 'Não'
            ),
            'phone'           => preg_replace('/\D+/', '', $data['phone'])
        ] + $data;

        $data['metadata'] = [
            'ja_investe_previdencia' => $data['previdencia_privada'] == '1'
        ];

        $contact = Contact::create($data);

        try {
            Mail::to('manutencaodebeneficios@grupotgl.com')->send(new ContactMail($contact));
        } catch (Swift_TransportException $e) {
            Log::error($e);
        }

        return redirect()->back()->with(['success' => true]);
    }


    public function storeTglSuit(Request $request)
    {
        $data = $request->validate([
            'name'                => ['required', 'string'],
            'email'               => ['required', 'email'],
            'phone'               => ['required', 'string', 'celular_com_ddd'],
            'oab'                 => ['required', 'string']
        ]);

        $data = [
            'contact_type_id' => 4,
            'subject'         => 'TGL Suit',
            'message'         => "Contato enviado da Landing Page do TGL Suit.\nOab Informado: {$data['oab']}.",
            'phone'           => preg_replace('/\D+/', '', $data['phone'])
        ] + $data;

        $data['metadata'] = [
            'oab' => $data['oab']
        ];

        $contact = Contact::create($data);

        try {
            Mail::to('contato@grupotgl.com')->send(new ContactMail($contact));
        } catch (Swift_TransportException $e) {
            Log::error($e);
        }

        return redirect()->back()->with(['success' => true]);
    }

    public function storeCootes(Request $request)
    {
        $data = $request->validate([
            'name'    => ['required', 'string'],
            'email'   => ['required', 'email'],
            'phone'   => ['required', 'string', 'celular_com_ddd'],
        ]);

        $data = [
            'contact_type_id' => 5,
            'subject'         => 'Palestra do Cootes dia 30/03 às 20h',
            'message'         => '',
            'phone'           => preg_replace('/\D+/', '', $data['phone'])
        ] + $data;

        $contact = Contact::create($data);

        try {

            Mail::to('cootes@cootes.com.br')
                ->bcc('comunicacao@grupotgl.com')
                ->send(new ContactMail($contact));

        } catch (Swift_TransportException $e) {
            Log::error($e);
        }

        return redirect()->back()->with(['success' => true]);
    }


    public function storeIr(Request $request)
    {
        $data = $request->validate([
            'name'    => ['required', 'string'],
            'email'   => ['required', 'email'],
            'phone'   => ['required', 'string', 'celular_com_ddd'],
        ]);

        $data = [
            'contact_type_id' => 6,
            'subject'         => 'LP IR 2022',
            'message'         => '',
            'phone'           => preg_replace('/\D+/', '', $data['phone'])
        ] + $data;

        $contact = Contact::create($data);

        try {
            Mail::to('contato@grupotgl.com')->send(new ContactMail($contact));
        } catch (Swift_TransportException $e) {
            Log::error($e);
        }

        return redirect()->back()->with(['success' => true]);

    }
}
