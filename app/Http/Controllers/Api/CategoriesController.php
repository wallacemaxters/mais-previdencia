<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index(Request $request)
    {
        return Category::withCount('products')->filter($request)->paginate();
    }

    public function store(StoreRequest $request)
    {
        return Category::create($request->validated());
    }

    public function show(Category $category)
    {
        return $category->load('products');
    }

    public function update(UpdateRequest $request, Category $category)
    {
        $category->update($request->validated());


        return $category;
    }

    public function destroy(Category $category)
    {
        $category->delete();
    }
}
