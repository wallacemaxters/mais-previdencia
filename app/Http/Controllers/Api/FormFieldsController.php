<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FormField\StoreRequest;
use App\Http\Requests\FormField\UpdateRequest;
use App\Models\FormField;
use Illuminate\Http\Request;

class FormFieldsController extends Controller
{
    public function index(Request $request)
    {
        return FormField::filter($request)->paginate();
    }

    public function store(StoreRequest $request)
    {
        return FormField::create($request->validated());
    }

    public function show(FormField $formField)
    {
        return $formField;
    }

    public function update(UpdateRequest $request, FormField $formField)
    {
        $formField->update($request->validated());

        return $formField;
    }

    public function destroy(FormField $formField)
    {
        $formField->delete();
    }
}
