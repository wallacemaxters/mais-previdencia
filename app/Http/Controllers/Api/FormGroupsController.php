<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FormGroup\StoreRequest;
use App\Http\Requests\FormGroup\UpdateRequest;
use App\Models\FormGroup;
use Illuminate\Http\Request;

class FormGroupsController extends Controller
{
    public function index(Request $request)
    {
        return FormGroup::filter($request)->paginate();
    }

    public function store(StoreRequest $request)
    {
        return FormGroup::create($request->validated());
    }

    public function show(FormGroup $formGroup)
    {
        return $formGroup;
    }

    public function update(UpdateRequest $request, FormGroup $formGroup)
    {
        $formGroup->update($request->validated());

        return $formGroup;
    }

    public function destroy(FormGroup $formGroup)
    {
        $formGroup->delete();
    }
}
