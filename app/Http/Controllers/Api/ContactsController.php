<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contact\StoreRequest;
use App\Http\Requests\Contact\UpdateRequest;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    public function index(Request $request)
    {
        return Contact::with(['type'])->filter($request)->latest('id')->paginate(60);
    }

    public function show(Contact $contact)
    {
        return $contact->load('type');
    }

    public function destroy(Contact $contact)
    {
        $contact->delete();
    }
}
