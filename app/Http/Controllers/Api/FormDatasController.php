<?php

namespace App\Http\Controllers\Api;

use App\Models\FormData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\FormData\StoreRequest;
use App\Http\Requests\FormData\UpdateRequest;

class FormDatasController extends Controller
{
    public function index(Request $request)
    {
        return FormData::with('form.fields')->filter($request)->paginate();
    }

    public function store(StoreRequest $request)
    {
        return FormData::create($request->validated());
    }

    public function show(FormData $formData)
    {
        return $formData->load('form.fields');
    }

    public function update(UpdateRequest $request, FormData $formData)
    {
        $formData->update($request->validated());

        return $formData;
    }

    public function destroy(FormData $formData)
    {
        $formData->delete();
    }
}
