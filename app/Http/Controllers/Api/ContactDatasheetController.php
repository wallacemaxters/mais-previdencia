<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactDatasheet\StoreRequest;
use App\Http\Requests\ContactDatasheet\UpdateRequest;
use App\Models\ContactDatasheet;
use Illuminate\Http\Request;

class ContactDatasheetController extends Controller
{
    public function index(Request $request)
    {
        return ContactDatasheet::filter($request)->latest('id')->paginate();
    }

    public function show(ContactDatasheet $contactDatasheet)
    {
        return $contactDatasheet;
    }

    public function destroy(ContactDatasheet $contactDatasheet)
    {
        $contactDatasheet->delete();
    }
}
