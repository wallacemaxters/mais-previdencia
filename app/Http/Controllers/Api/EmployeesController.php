<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Employee\StoreRequest;
use App\Http\Requests\Employee\UpdateRequest;
use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeesController extends Controller
{
    public function index(Request $request)
    {
        return Employee::filter($request)->oldest('order')->paginate(24);
    }

    public function store(StoreRequest $request)
    {
        return Employee::create($request->validated());
    }

    public function show(Employee $employee)
    {
        return $employee;
    }

    public function update(UpdateRequest $request, Employee $employee)
    {
        $employee->update($request->validated());

        return $employee;
    }

    public function destroy(Employee $employee)
    {
        $employee->delete();
    }
}
