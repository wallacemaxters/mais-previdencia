<?php

namespace App\Http\Controllers\Api;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Post\StoreRequest;
use App\Http\Requests\Post\UpdateRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class PostsController extends Controller
{
    public function index(Request $request)
    {
        return Post::with('user')
                    ->forUser($request->user())
                    ->filter()
                    ->latest('id')
                    ->paginate(30);
    }

    public function show($id)
    {
        return Post::findOrFail($id)->load('tags');
    }

    public function store(StoreRequest $request)
    {
        $data = $request->validated() + [
            'user_id' => $request->user()->getKey()
        ];

        $post = Post::create($data);

        return $post;
    }

    public function update(UpdateRequest $request, Post $post)
    {
        $post->update($request->validated());

        return $post;
    }

    protected function saveUploadedImage(UploadedFile $image, $path)
    {
        $path = $image->storePublicly(Post::COVER_PATH);

        return [
            'path' => $path,
            Storage::url($path)
        ];
    }

    public function uploadCover(Request $request)
    {
        $request->validate([
            'image' => ['required', 'file']
        ]);

        return $this->saveUploadedImage($request->file('image'), Post::COVER_PATH);
    }

    public function uploadImage(Request $request)
    {
        $request->validate([
            'image' => ['required', 'file']
        ]);

        return $this->saveUploadedImage($request->file('image'), 'posts');
    }

    public function draft(Request $request, Post $post)
    {
        $this->validate($request, [
            'is_draft' => ['required', 'boolean']
        ]);

        $post->update(['is_draft' => $request->is_draft]);

        return $post;
    }

    public function destroy(Post $post)
    {
        $post->delete();
    }

    protected function processTags(Post $post, array $tags)
    {
        $post->tags()->detach();
        $post->tags()->attach($tags);
    }
}
