<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Project\StoreRequest;
use App\Http\Requests\Project\UpdateRequest;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    public function index(Request $request)
    {
        return Project::filter($request)->paginate();
    }

    public function store(StoreRequest $request)
    {
        return Project::create($request->validated());
    }

    public function show(Project $project)
    {
        return $project;
    }

    public function update(UpdateRequest $request, Project $project)
    {
        $project->update($request->validated());

        return $project;
    }

    public function destroy(Project $project)
    {
        $project->delete();
    }
}
