<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Form\StoreRequest;
use App\Http\Requests\Form\UpdateRequest;
use App\Models\Form;
use Illuminate\Http\Request;

class FormsController extends Controller
{
    public function index(Request $request)
    {
        return Form::filter($request)->paginate();
    }

    public function store(StoreRequest $request)
    {
        return Form::create($request->validated());
    }

    public function show(Form $form)
    {
        return $form;
    }

    public function update(UpdateRequest $request, Form $form)
    {
        $form->update($request->validated());

        return $form;
    }

    public function destroy(Form $form)
    {
        $form->delete();
    }

    public function formGroups(Form $form)
    {
        return $form->groups()->filter()->paginate(30);
    }

    public function formFields(Form $form)
    {
        return $form->fields()->oldest('index')->paginate(30);
    }
}
