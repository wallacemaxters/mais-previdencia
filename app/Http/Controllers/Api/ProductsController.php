<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\StoreRequest;
use App\Http\Requests\Product\UpdateRequest;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(Request $request)
    {
        return Product::filter($request)->paginate(24);
    }

    public function store(StoreRequest $request)
    {
        return Product::create($request->validated());
    }

    public function show(Product $product)
    {
        return $product->load('categories');
    }

    public function update(UpdateRequest $request, Product $product)
    {
        $product->update($request->validated());

        return $product;
    }

    public function destroy(Product $product)
    {
        $product->delete();
    }
}
