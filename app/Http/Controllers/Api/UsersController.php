<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function login(Request $request)
    {
        $data = $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required',
        ]);

        $user = User::where('email', $data['email'])->first();

        if ($user === null) {
            return response()->json(['message' => 'Login não encontrado'], 401);
        } elseif (app('hash')->check($data['password'], $user->password)) {
            return $user->makeVisible('api_token');
        }

        return response()->json(['message' => 'Senha inválida'], 401);
    }

    public function index(Request $request)
    {
        return User::with(['employee'])->filter($request)->paginate();
    }

    public function show(User $user)
    {
        return $user;
    }

    public function me(Request $request)
    {
        return $request->user('api')->load('employee');
    }

}
