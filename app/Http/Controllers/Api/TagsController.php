<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    public function index()
    {
        return Tag::withCount('posts')->filter()->paginate();
    }

    public function show($id)
    {
        return Tag::findOrFail($id)->loadCount('posts');
    }

    public function store(Request $request)
    {
        $tag = Tag::create($request->all());

        return $tag;
    }

    public function update(Request $request, $id)
    {
        $tag = Tag::findOrFail($id);

        $tag->update($request->all());

        return $tag;
    }
}
