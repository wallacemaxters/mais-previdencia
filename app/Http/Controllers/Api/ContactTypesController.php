<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactType\StoreRequest;
use App\Http\Requests\ContactType\UpdateRequest;
use App\Models\ContactType;
use Illuminate\Http\Request;

class ContactTypesController extends Controller
{
    public function index(Request $request)
    {
        return ContactType::filter($request)->oldest('name')->paginate();
    }
}
