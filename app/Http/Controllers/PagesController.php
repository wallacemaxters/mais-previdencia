<?php

namespace App\Http\Controllers;

use App\Http\Middleware\AntiSpam;
use App\Models\Post;
use App\Models\Product;
use App\Models\Category;
use App\Models\Employee;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
        $employees = Employee::oldest('order')->oldest('first_name')->get();

        $partners = [
            'seguralta.jpg',
            'metlife.jpg',
            'bhr.png',
            'sincor.png',
            'volant.png',
            'tgl.png',
            'google.png',
            'multseg.png',
        ];

        $products = Product::get();

        $services = [
            [
                'title'    => 'Para cuidar de você e de sua família',
                'category' => 'Serviços Financeiros',
                'video'     => 'videos/pages/icone_1.mp4'
            ],
            [
                'title' => 'Para cuidar do seu patrimônio',
                'video'     => 'videos/pages/icone_2.mp4',
                'category' => 'Serviços Contábeis'
            ],
            [
                'title' => 'Seguros e Benefícios',
                'video'     => 'videos/pages/icone_3.mp4',
                'category' => 'Proteções',
            ],
        ];


        $newPosts = Post::take(3)->latest('id')->get();

        $categories = Category::get();


        $introBlocks = [
            [
                'title'    => 'Por que escolher a Consultoria em Seguros Mais Previdência?',
                'url'     => route('products.index'),
                'video'   => 'icone_1.mp4',
                'subtitle' => 'Para cuidar de você e de sua família.',
                'text'     => "A Mais Previdência cuida do seu Futuro!\nImagina encontrar as melhores soluções em seguros, benefícios, investimentos, assessoria jurídica e consórcio em um só lugar, na sua entidade, além de ter a tranquilidade e a certeza de que conta com uma consultoria premium independente, que garante as melhores soluções para você.\nA Mais Previdência e a TGL Consultoria cuidam de você para que você cuide de quem mais ama. Faça como as mais de 30 mil pessoas e empresas: escolha a TGL Consultoria.",
            ],
            [

                'title'    => 'Serviços Financeiros',
                'subtitle' => 'Para cuidar do seu futuro e de sua família.',
                'text'     => "A TGL Consultoria trabalha com as melhores soluções em seguros, investimentos e consórcios de forma independente e com mais de 17 anos de experiência atendendo milhares de pessoas e empresas. Escolha um dos nossos consultores e receba um diagnóstico financeiro exclusivo.",
                'url'     => route('categories.show', 'servicos-financeiros'),
                'video'   => 'icone_3.mp4'
            ]
        ];

        return view(
            'pages.index',
            compact('introBlocks', 'employees', 'services', 'partners', 'products', 'newPosts')
        );
    }

    public function about()
    {
        $employees = Employee::oldest('order')->oldest('first_name')->get();

        $items = [
            [
                'icon'  => 'missao.svg',
                'title' => 'Missão',
                'text' => 'Cuidar e potencializar a vida financeira e o legado dos nossos clientes por meio de um atendimento consultivo, conectando propósitos e gerando prosperidade.'
            ],

            [
                'icon'  => 'visao.svg',
                'title' => 'Visão',
                'text' => 'Ser referência como plano de previdência complementar para os empregados da FIEMG, sindicatos, industriais e funcionários da indústria mineira e seus familiares.'
            ],
            [
                'icon' => 'valores.svg',
                'title' => 'Valores',
                'text' => 'Garantir o pagamento de renda complementar na inatividade, mediante a gestão eficaz dos recursos dos planos de benefícios, garantindo a transparência nas operações e relações com os participantes.'
            ],
        ];

        $title = 'SOBRE';

        return view('pages.about', compact('employees', 'items', 'title'));
    }

    public function aboutPrivacity()
    {
        return view('pages.about_privacity');
    }

    public function aboutCookies()
    {
        return view('pages.about_cookies');
    }

    public function idap()
    {
        return view('pages.idap');
    }

    public function quotation()
    {
        $tglProducts = $this->crmProducts();

        return view('pages.quotation', compact('tglProducts'));
    }

    public function postQuotation(Request $request)
    {
        $this->validate($request, [
            'pessoa_nome'       => 'required|string',
            'pessoa_email'      => 'required|email',
            'pessoa_celular'    => 'required|string|celular_com_ddd',
            'pessoa_profissao'  => 'required|string',
            'pessoa_observacao' => 'nullable|string',
            'produtos'          => ['array', 'min:1']
        ]);

        $tgl = app('tgl');

        $data = $request->only(
            'pessoa_nome',
            'pessoa_email',
            'pessoa_profissao'
        );

        $data['pessoa_celular'] = preg_replace('/\D+/', '', $request->get('pessoa_celular'));

        try {
            if ($request->has('marca')) {
                $auto = $request->only('marca', 'ano_fabricacao', 'ano_modelo', 'modelo', 'renovacao');
            }

            $response = $tgl->post('cliente', ['json' => $data]);

            $result = json_decode($response->getBody()->getContents(), true);

            $auto = [];

            $res = $tgl->post('processo/indicacao', [
                'json' => $auto + [
                    'cliente_id'          => $result['result']['data']['cliente_id'],
                    'empresa_indicada_id' => 1,
                    'observacoes'         => null,
                    'produtos'            => (array) $request->get('produtos'),
                    'site_empresa_id'     => 1,
                    'url_origem'          => url()->previous(),
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $result_error = json_decode($e->getResponse()->getBody()->getContents(), true);

            if (isset($result_error['errors'])) {
                return redirect()->back()->withErrors($result_error['errors'])->withInput();
            }
        }


        return redirect()->back()->with(['message' => 'Mensagem enviada com sucesso. Em breve entraremos em contato!']);
    }


    protected function crmProducts(): array
    {
        if (cache()->has('tgl.products')) {
            return (array) cache('tgl.products');
        }

        $json = app('tgl')->get('produto')->getBody()->getContents();

        $result = json_decode($json, true);

        cache(['tgl.products' => $result['result']['data']], 500);

        return $result['result']['data'];
    }

    public function maisPrevidencia()
    {
        return view('pages.mais-previdencia', [
            'honeyPot' => AntiSpam::generateHoneyPot()
        ]);
    }

    public function maisPrevidenciaNovosColaboradores()
    {
        return view('pages.mais-previdencia-colaboradores', [
            'honeyPot' => AntiSpam::generateHoneyPot()
        ]);
    }

    public function cootes()
    {
        return view('pages.cootes', [
            'honeyPot' => AntiSpam::generateHoneyPot()
        ]);
    }


    public function contactCootes()
    {
        return view('pages.contact_cootes');
    }

    public function tglSuit()
    {
        return view('pages.tgl_suit', [
            'honeyPot' => AntiSpam::generateHoneyPot()
        ]);
    }

    /**
     * Landing page relacionada ao Imposto de Renda
     *
     */
    public function ir()
    {
        return view('pages.ir', [
            'honeyPot' => AntiSpam::generateHoneyPot()
        ]);
    }
}
