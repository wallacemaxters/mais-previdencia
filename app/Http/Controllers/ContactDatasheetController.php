<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Swift_TransportException;
use Illuminate\Validation\Rule;
use App\Models\ContactDatasheet;
use App\Mail\ContactDatasheetMail;
use Illuminate\Support\Facades\Log;
use App\Models\ContactDatasheetType;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class ContactDatasheetController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'name'                                 => ['required', 'string', 'min:5'],
            'birth_date'                           => ['required', 'date_format:d/m/Y'],
            'cpf'                                  => ['required', 'cpf'],
            'rg'                                   => ['required'],
            'phone'                                => ['required', 'celular_com_ddd'],
            'email'                                => ['required', 'email'],
            'zip_code'                             => ['required', 'formato_cep'],
            'state'                                => ['required', 'uf'],
            'street'                               => ['required', 'string'],
            'district'                             => ['required', 'string'],
            'city'                                 => ['required', 'string'],
            'crm'                                  => ['required', 'string'],
            'position'                             => ['required', 'string'],
            'nit_number'                           => ['nullable'],
            'pis_number'                           => ['nullable'],
            'pasep_number'                         => ['nullable'],
            'ir_type'                              => ['required', Rule::in(['S', 'C'])],
            'has_private_pension_contribution'     => ['required', 'boolean'],
            'number_of_children'                   => ['required', 'numeric', 'min:0'],
            'childrens_age'                        => ['nullable', 'string'],
            'monthly_income'                       => ['required', 'numeric', 'min:0'],
            'procuracao_inss_attachment'           => ['nullable', 'file'],
            'residency_proof_attachment'           => ['nullable', 'file'],
            'last_irpf_attachment'                 => ['nullable', 'file'],
            'retired_concession_letter_attachment' => ['nullable', 'file'],
            'enrollment_proof_inss_attachment'     => ['nullable', 'file'],
            'pasep_or_pis_or_nit_attachment'       => ['nullable', 'file'],
            'ctc_attachment'                       => ['nullable', 'file'],
            'crm_attachment'                       => ['nullable', 'file'],
            'procuration_attachment'               => ['required', 'file']
        ], ['procuration_attachment.required' => 'Você precisa baixar a procuração e enviá-la assinada.']);


        $data = $request->all();

        foreach ([
            'procuracao_inss_attachment',
            'residency_proof_attachment',
            'last_irpf_attachment',
            'retired_concession_letter_attachment',
            'enrollment_proof_inss_attachment',
            'pasep_or_pis_or_nit_attachment',
            'ctc_attachment',
            'crm_attachment',
            'procuration_attachment'
        ] as $key) {
            $file = $request->file($key);

            if ($file === null) {
                $data[$key] = null;
                continue;
            }

            $data[$key] = $file->store('documentos-cootes', [
                'disk' => 'local'
            ]);
        }

        foreach (['cpf', 'zip_code', 'phone'] as $key) {
            $data[$key] = preg_replace('/\D+/', '', $data[$key]);
        }

        $contactDatasheet = ContactDatasheet::create($data);

        try {

            Mail::to('relacionamento@tglcontabil.com.br')
            ->bcc('cootes@cootes.com.br')
            ->send(
                new ContactDatasheetMail($contactDatasheet)
            );

        } catch (Swift_TransportException $e) {
            Log::error($e);
        }

        return redirect()->back()->with(['success' => true]);
    }

    public function file(Request $request, ContactDatasheet $contactDatasheet, string $name)
    {
        $disk = Storage::disk('local');
        $path = $contactDatasheet->getAttribute($name);

        if (! $request->hasValidSignature()) {
            abort(403);
        }

        return response()->stream(
            fn () => readfile($disk->getAdapter()->applyPathPrefix($path)),
            200,
            ['content-type' => $disk->mimeType($path)]
        );
    }
}
