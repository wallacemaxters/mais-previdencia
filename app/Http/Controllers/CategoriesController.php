<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    protected function renderShow(Category $category)
    {
        return view('products.index', [
            'category' => $category,
            'products' => $category->products()->get(),
        ]);
    }

    public function show(Category $category)
    {
        return $this->renderShow($category);
    }

    public function showBySlug(string $slug)
    {
        $category = Category::where(['slug' => $slug])->firstOrFail();

        return $this->renderShow($category);
    }
}
