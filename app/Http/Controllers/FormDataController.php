<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Models\FormData;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Middleware\AntiSpam;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class FormDataController extends Controller
{
    public function create(Form $form)
    {
        return view('form-data.create', [
            'form'     => $form
        ]);
    }

    public function store(Form $form, Request $request)
    {
        $rules = $this->generateValidation($form);

        $data  = $request->validate($rules);

        $uploads = $this->getUploads($form, $request);

        FormData::create(['data' => $uploads + $data, 'form_id' => $form->id]);

        return redirect()->back()->with(['success' => true]);
    }

    protected function getUploads(Form $form, Request $request)
    {
        $uploads = [];

        $disk = 'local';

        foreach ($form->fields()->where('type', 'upload')->cursor() as $field) {

            $file = $request->file($field->name);

            if ($file === null) continue;

            $path = $file->store('form-data', ['disk' => $disk]);

            $uploads[$field->name] = $path;
        }

        return $uploads;
    }

    protected function generateValidation(Form $form)
    {
        $rules = [];

        foreach ($form->fields as $field) {

            $attrs = collect($field->attributes);

            $required = $attrs->get('required') === false ? 'nullable' : 'required';
            $type     = ['upload' => 'file', 'number' => 'numeric'][$attrs->get('type', $field->type)] ?? 'string';

            $fieldRules = [
                $required,
                $type
            ];

            if ($field->rules) {
                $fieldRules = array_merge($fieldRules, (array) $field->rules);
            }

            if ($field->type === 'select') {

                $fieldRules[] = Rule::in(array_keys($attrs->get('items')));
            }


            $rules[$field->name] = $fieldRules;

        }

        return $rules;
    }


    public function file(Request $request, FormData $formData, string $name)
    {
        if (! $request->hasValidSignature()) {
            abort(401);
        }

        if (empty($formData->data[$name])) {
            return response('Não encontrado', 404);
        }

        $disk = Storage::disk('local');
        $path = $formData->data[$name];
        $fullpath = $disk->getAdapter()->applyPathPrefix($path);

        return response()->stream(fn () => readfile($fullpath), 200, [
            'content-type' => $disk->mimeType($path)
        ]);
    }
}
