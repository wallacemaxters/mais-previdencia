<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    public function handle($request, Closure $next)
    {
        if ($request->isMethod('OPTIONS')) {
            $response = response()->json(['method' => 'OPTIONS'], 200);
        } else {
            $response = $next($request);
        }

        static::apply($response);

        return $response;
    }

    public static function apply($response)
    {
        $headers = [
            'Access-Control-Allow-Origin'      => '*',
            'Access-Control-Allow-Methods'     => 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Credentials' => 'true',
            'Access-Control-Max-Age'           => '86400',
            'Access-Control-Allow-Headers'     => 'Content-Type, Authorization, X-Requested-With'
        ];

        foreach ($headers as $key => $value) {
            $response->headers->set($key, $value);
        }
    }
}
