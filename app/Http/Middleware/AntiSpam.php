<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AntiSpam
{
    public const KEY = 'ClientFullName';

    public function handle(Request $request, Closure $next)
    {
        if ($request->get(static::KEY)) {
            return abort(403, 'Não autorizado.');
        }

        return $next($request);
    }

    public static function generateHoneyPot()
    {
        return sprintf('<input
            tabindex="-1"
            type="text"
            name="%s"
            class="appearance-none"
            autocomplete="off"
            style="width:1px;height:1px;position:absolute;bottom:0;left:0;opacity:0.1;"
        />', static::KEY);
    }
}
