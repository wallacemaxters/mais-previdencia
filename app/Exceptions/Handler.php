<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var string[]
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var string[]
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            $this->sendRemoteReport($e);
        });
    }

    protected function sendRemoteReport($exception)
    {
        if ($this->shouldntReport($exception) || app()->isLocal()) {
            return;
        }

        $request = request();

        $guzzle = new \GuzzleHttp\Client([
            'timeout' => 5
        ]);

        $payload = [
            [
                'name'     => 'content',
                'contents' => 'Reportando erro do site da MAIS PREVIDÊNCIA',
            ],

            [
                'name'     => 'file',
                'contents' => (string) $exception,
                'filename' => 'trace.txt',
            ],

            [
                'name' => 'username',
                'contents' => 'MAIS PREVIDÊNCIA'
            ],

             [
                'name'     => 'origin',
                'contents' => var_export([
                    'fullUrl' => $request->fullUrl()

                ], true),
                'filename' => 'origin.txt',
            ],
        ];

        if (count($headers = $request->headers->all())) {
            $payload[] = [
                'name'     => 'headers',
                'contents' => var_export($headers, true),
                'filename' => 'headers.txt',
            ];
        }

        if (count($data = $request->all())) {
            $payload[] = [
                'name'     => 'request',
                'contents' => var_export($data, true),
                'filename' => 'request.txt',
            ];
        }

        $guzzle->post(config('logging.discord'), ['multipart' => $payload]);
    }
}
