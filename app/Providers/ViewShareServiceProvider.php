<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\View\View;
use Illuminate\Support\ServiceProvider;

class ViewShareServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('*', function ($view) {
            $this->configureViews($view);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }


    public function configureViews($view)
    {
        $MENU = [
            route('pages.home')  => 'Home',
            route('pages.about') => 'Sobre',
            // route('products.index') => 'Serviços Financeiros'
        ];

        foreach (Category::get() as $category) {
            $key = route('categories.show_by_slug', ['slug' => $category->slug]);

            $MENU[$key] = $category->name;
        }

        $MENU += [
            // route('employees.index') => 'Equipe',
            route('posts.index')     => 'Blog',
            route('contacts.create') => 'Contato',
        ];


        $view['MENU'] = $MENU;

        $view['POSTS'] = Post::latest('posted_at')->take(3)->get();
    }
}
