<?php

namespace App\Providers;

use App\Models\Category;
use Aws\S3\S3Client;
use Illuminate\Support\ServiceProvider;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        config([
            'filesystems.disks.public.url' => url('storage')
        ]);
    }

    public function boot()
    {
        $this->app->bind('tgl', function () {
            return new \GuzzleHttp\Client([
                'headers' => [
                    'accept' => 'application/json'
                ],
                'base_uri' => config('site.crm_api_base_url'),
                'verify' => false

            ]);
        });
    }
}
