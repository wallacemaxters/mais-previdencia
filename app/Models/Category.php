<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use LaravelLegends\EloquentFilter\Concerns\HasFilter;

class Category extends Model
{
    use HasFactory;
    use HasFilter;
    use SoftDeletes;

    protected $table = 'categories';

    protected $fillable = ['name', 'description', 'slug'];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'categories_products');
    }

    public function getVideoUrlAttribute()
    {
        return Storage::url($this->video);
    }

    public function getVideoIconUrlAttribute()
    {
        return Storage::url($this->video_icon);
    }
}
