<?php

namespace App\Models;

use App\Models\Traits\PerPage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use LaravelLegends\EloquentFilter\Concerns\HasFilter;

class FormGroup extends Model
{
    use HasFactory;
    use HasFilter;

    protected $fillable = [
        'name',
        'index',
        'attributes'
    ];

    protected $casts = [
        'attributes' => 'array'
    ];

    public function fields()
    {
        return $this->hasMany(FormField::class, 'form_group_id');
    }
}
