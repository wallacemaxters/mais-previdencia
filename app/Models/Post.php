<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use LaravelLegends\EloquentFilter\Concerns\HasFilter;

class Post extends Model
{
    use HasFilter;
    use HasFactory;
    use SoftDeletes;

    public const COVER_PATH = 'posts/cover';

    protected $table = 'posts';

    protected $fillable = [
        'title',
        'content',
        'slug',
        'excerpt',
        'cover',
        'posted_at',
        'user_id',
        // 'counts_views',
        'is_draft'
    ];

    protected $dates = ['posted_at'];

    protected $appends = ['cover_url', 'url'];

    protected $casts = [
        'is_draft' => 'boolean'
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'posts_tags');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getCoverUrlAttribute()
    {
        static $disk;

        if ($disk === null) {
            $disk = app('filesystem');
        }

        return $this->cover ? url('storage/' . $this->cover) : null;
    }

    public function getUrlAttribute()
    {
        return route('posts.show_by_slug', $this->slug);
    }


    public function scopePublished($query, $published = true)
    {
        return $query->where('is_draft', !$published)
                      ->whereDate('posted_at', '<=', now()->format('Y-m-d'));
    }

    public function scopeForUser($query, User $user)
    {
        if ($user->employee === null) {
            return $query;
        }

        return $query->where('user_id', $user->getKey());
    }
}
