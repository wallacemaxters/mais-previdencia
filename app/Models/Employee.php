<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use LaravelLegends\EloquentFilter\Concerns\HasFilter;

class Employee extends Model
{
    use HasFactory;
    use HasFilter;
    use SoftDeletes;

    protected $fillable = [
        'image',
        'first_name',
        'last_name',
        'position',
        'phone',
        'linkedin',
        'whatsapp',
        'skill',
        'user_id'
    ];

    protected $appends = ['full_name', 'image_url'];

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getImageUrlAttribute()
    {
        return Storage::url($this->image);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
