<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use LaravelLegends\EloquentFilter\Concerns\HasFilter;

class Tag extends Model
{
    use HasFilter;

    protected $table = 'tags';

    protected $fillable = ['name'];

    public $timestamps = false;

    protected $allowedFilters = [
        'name'     => ['starts_with', 'contains', 'ends_with'],
        'posts.id' => ['not_equal', 'equal'],
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'posts_tags');
    }
}
