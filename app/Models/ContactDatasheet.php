<?php

namespace App\Models;

use App\Models\Traits\PerPage;
use Illuminate\Support\Facades\URL;
use Illuminate\Database\Eloquent\Model;
use LaravelLegends\EloquentFilter\Concerns\HasFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ContactDatasheet extends Model
{
    use HasFactory;
    use HasFilter;

    protected $casts = [
        'has_private_pension_contribution' => 'boolean'
    ];

    protected $fillable = [
        'name',
        'birth_date',
        'cpf',
        'rg',
        'phone',
        'email',
        'zip_code',
        'state',
        'street',
        'district',
        'city',
        'crm',
        'position',
        'nit_number',
        'pis_number',
        'pasep_number',
        'ir_type',
        'has_private_pension_contribution',
        'number_of_children',
        'childrens_age',
        'monthly_income',
        'procuracao_inss_attachment',
        'residency_proof_attachment',
        'last_irpf_attachment',
        'retired_concession_letter_attachment',
        'enrollment_proof_inss_attachment',
        'pasep_or_pis_or_nit_attachment',
        'ctc_attachment',
        'crm_attachment',
        'procuration_attachment'
    ];

    protected $appends = ['file_urls'];

    public function getHasPrivatePensionContributionFormattedAttribute()
    {
        return $this->has_private_pension_contribution ? 'Sim' : 'Não';
    }

    public function getIrTypeFormattedAttribute()
    {
        return ['C' => 'Completa', 'S' => 'Simplificada'][$this->ir_type] ?? '-';
    }

    public function getFileUrlsAttribute()
    {
        $keys = [
            'procuracao_inss_attachment',
            'residency_proof_attachment',
            'last_irpf_attachment',
            'retired_concession_letter_attachment',
            'enrollment_proof_inss_attachment',
            'pasep_or_pis_or_nit_attachment',
            'ctc_attachment',
            'crm_attachment',
            'procuration_attachment'
        ];

        $urls = [];

        foreach ($keys as $key) {

            $path = $this->getAttribute($key);

            if ($path === null) continue;

            $urls[$key] = URL::signedRoute('contact_datasheets.file', [
                'contactDatasheet' => $this->id,
                'name'             => $key
            ]);
        }

        return $urls;
    }
}
