<?php

namespace App\Models;

use App\Models\Traits\PerPage;
use Illuminate\Support\Facades\URL;
use Illuminate\Database\Eloquent\Model;
use LaravelLegends\EloquentFilter\Concerns\HasFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FormData extends Model
{
    use HasFactory, HasFilter;

    protected $fillable = ['data', 'form_id'];

    protected $casts = [
        'data' => 'array'
    ];

    protected $appends = [
        'files'
    ];

    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    public function getFilesAttribute()
    {

        $data = [];

        $fields = $this->form->fields()->where('type', '=', 'upload')->get();

        $time = now()->addMinutes(30);

        foreach ($fields as $field) {

            $data[$field->name] = URL::signedRoute(
                'form_data.file',
                ['formData' => $this, 'name' => $field->name],
                $time,
            );
        }

        return $data;
    }
}
