<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use LaravelLegends\EloquentFilter\Concerns\HasFilter;

class Contact extends Model
{
    use HasFactory;
    use HasFilter;
    use SoftDeletes;

    protected $table = 'contacts';

    protected $fillable = ['name', 'email', 'phone', 'subject', 'message', 'contact_type_id', 'metadata'];

    protected $casts = [
        'metadata' => 'array'
    ];

    public function type()
    {
        return $this->belongsTo(ContactType::class, 'contact_type_id');
    }


    public function getFormattedPhoneAttribute()
    {
        $format = strlen($this->phone) === 11 ? '(%d%d) %d%d%d%d%d-%d%d%d%d' : '(%d%d) %d%d%d%d-%d%d%d%d';

        return sprintf($format, ...str_split($this->phone));
    }
}
