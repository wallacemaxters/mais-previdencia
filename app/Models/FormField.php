<?php

namespace App\Models;

use App\Models\Traits\PerPage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use LaravelLegends\EloquentFilter\Concerns\HasFilter;

class FormField extends Model
{
    use HasFactory;
    use HasFilter;

    protected $fillable = ['name', 'type', 'attributes', 'rules'];

    protected $casts = [
        'attributes' => 'array',
        'rules'      => 'array'
    ];
}
