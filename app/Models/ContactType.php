<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use LaravelLegends\EloquentFilter\Concerns\HasFilter;

class ContactType extends Model
{
    use HasFactory;
    use HasFilter;

    public $timestamps = false;

    protected $table = 'contact_types';

    protected $fillable = ['name'];
}
