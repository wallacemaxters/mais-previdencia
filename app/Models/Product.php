<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use LaravelLegends\EloquentFilter\Concerns\HasFilter;

class Product extends Model
{
    use HasFactory;
    use HasFilter;
    use SoftDeletes;

    protected $appends = [
        'image_url'
    ];

    protected $fillable = [
        'main',
        'name',
        'excerpt',
        'content',
        'image',
        'slug',
        'action_url'
    ];

    protected $casts = [
        'is_main' => 'boolean'
    ];

    public function getImageUrlAttribute()
    {
        return Storage::url($this->attributes['image']);
    }

    public function getUrlAttribute()
    {
        return route('products.show_by_slug', $this->slug);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'categories_products');
    }
}
