<?php

namespace App\Models;

use App\Models\Traits\PerPage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use LaravelLegends\EloquentFilter\Concerns\HasFilter;

class Form extends Model
{
    use HasFactory;
    use HasFilter;

    protected $fillable = ['name'];

    public function groups()
    {
        return $this->hasMany(FormGroup::class, 'form_id');
    }

    public function fields()
    {
        return $this->hasManyThrough(FormField::class, FormGroup::class);
    }
}
