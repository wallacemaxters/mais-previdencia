<?php

namespace App\Mail;

use App\Models\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable;
    use SerializesModels;

    protected $contact;

    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    public function build()
    {
        return $this->subject('Contato do site grupotgl.com')
                    ->markdown('contacts.mails.default', [
                        'contact' => $this->contact,
                        'logo'    => $this->contact->contact_type_id === 2 ? 'img/mais-previdencia.png' : null
                    ]);
    }
}
