<?php

namespace App\Mail;

use App\Models\ContactDatasheet;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactDatasheetMail extends Mailable
{
    use Queueable, SerializesModels;

    public $contactDatasheet;

    public function __construct(ContactDatasheet $contactDatasheet)
    {
        $this->contactDatasheet = $contactDatasheet;
    }

    public function build()
    {
        return $this->subject('Ficha Técnica - COOTES')->markdown('contact-datasheets.email', [
            'contactDatasheet' => $this->contactDatasheet
        ]);
    }
}
