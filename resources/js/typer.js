function promiseTimeout(delay) {
    return new Promise((resolve) => setTimeout(resolve, delay))
}

async function writeWord(el, text, delay) {

    const cleanerPromises = el.textContent.split('').map(async (_, key) => {
        return promiseTimeout(delay * key).then(() => el.textContent = el.textContent.toString().slice(0, -1))
    })

    await Promise.all(cleanerPromises);

    await promiseTimeout(500)

    const promises = text.split('').map(async (letter, key) => {
        return promiseTimeout(delay * key).then(() => el.textContent += letter)
    })

    return Promise.all(promises);
}



async function typer(options) {

    let promise = Promise.resolve()

    for (let text of options.words) {
        promise = promise
                .then(() => writeWord(options.el, text, options.typeDelay)
                .then(() => promiseTimeout(options.delay)))
    }

    await promise

    setTimeout(() => typer(options), options.delay);
}

export default typer
