import AOS from 'aos';
import VMasker from 'vanilla-masker';
import mousePointer from './mouse-pointer';
import qs from "./qs"
import typer from "./typer"
import slider from "./slider";
import "./parallax"
import "./x-select"

qs(() => {

    AOS.init();
    mousePointer()
    slider()

    const $menu = qs('#main-menu-links');
    const $btnMenu = qs('#btn-mobile-menu');

    $btnMenu.on('click', () => {
        $menu.removeClass('-translate-y-full')
    })

    qs('#main-menu-links, #btn-mobile-menu').clickOut((e) => {
        $menu.addClass('-translate-y-full')
    })


    qs('.typer-text').forEach(el => {
        typer({
            el,
            delay: 2000,
            typeDelay: 150,
            words: ['você.', 'quem você ama.', 'seu patrimônio.']
        })
    })

    qs('[data-mask]').forEach(el => VMasker(el).maskPattern(el.dataset.mask))


    qs('[data-cep]').on('input', async function (el) {

        const cep = (this.value ?? '').replace(/\D+/g, '');

        if (cep.length < 8) return;1

        const url = `https://viacep.com.br/ws/${cep}/json/unicode/`

        try {
            const response = await fetch(url)
            const data = await response.json()

            Object.keys(data).forEach((key) => {
                const field = qs(`[data-${key}]`).get(0);
                if (field) {
                    field.value = data[key]
                }
            })

        } finally {

        }

    });
})


window.qs = qs
