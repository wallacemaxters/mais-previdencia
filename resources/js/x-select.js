import qs from "./qs"

qs(() => {

    qs('.x-select').clickOut(() => {
        qs('.x-select-menu').addClass('hidden');
    }).forEach((select) => {

        const $menu = qs('.x-select-menu', select);
        const $input = qs('.x-select-input', select);

        qs('.x-select-menu-item', $menu[0]).on('input', (event) => {
            $input[0].value = event.target.dataset.label;
            $menu.addClass('hidden');
        }).forEach(item => {
            if (! item.checked) return;
            $input[0].value = item.dataset.label;
        });

        $input.on('click', () => $menu.removeClass('hidden'))
    })
})
