/**
 *
 * @param {Number} delay
 * @param {Function} callback
 * @returns {void}
 */
const debounce = (delay) => {
    let timeout = 0;

    return (callback) => {
        clearTimeout(timeout);
        timeout = setTimeout(callback, delay)
    };
}

export default debounce;
