import debounce from "./debounce"
/**
 *
 * @param {String} el
 * @param {Document|HTMLElement} context
 * @returns
 */
function qs(el, context = window.document) {

    if (el === window.document || el instanceof HTMLElement) {
        return new QsEvents(el);
    } else if (typeof el === 'function') {
        return document.addEventListener('DOMContentLoaded', el)
    }

    return QsEvents.from(context.querySelectorAll(el))
}



class QsEvents extends Array {

    /**
     * @param {String} eventName
     * @param {Function} callback
     * @returns QsEvents
     */
    on(eventName, callback) {
        this.forEach((el) => {
            el.addEventListener(eventName, callback)
        })

        return this;
    }

    /**
     *
     * @param {Number} key
     * @returns QsEvents
     */
    get(key) {
        return this[key]
    }

    /**
     *
     * @param {Object} attributeMap
     * @returns QsEvents
     */
    attributes(attributeMap) {

        Object.entries(attributeMap).forEach(([key, value]) => {
            this.forEach(el => el.setAttribute(key, value))
        });

        return this;
    }

    /**
     *
     * @param {String} className
     * @param {Function} callback
     * @returns QsEvents
     */
    _applyToClassList(callback) {
        this.forEach(el => callback(el.classList))
        return this;
    }

    /**
     *
     * @param {String} className
     * @returns QsEvents
     */
    toggleClass(className) {
        return this._applyToClassList(list => list.toggle(className))
    }

    /**
     *
     * @param {String} className
     * @returns
     */
    removeClass(className) {
        return this._applyToClassList(list => list.remove(className))
    }


    /**
     *
     * @param {String} className
     * @returns
     */
    addClass(className) {
        return this._applyToClassList(list => list.add(className))
    }

    /**
     *
     * @param {Function} callback
     * @returns QsEvents
     */
    clickOut(callback) {

        document.addEventListener('click', (event) => {

            if (this.some(el => el.contains(event.target))) return;

            callback(event)
        })

        return this
    }

    swipe(callback) {

        const getCoordinates = (event) => {

            const touch = (event.touches)[0]

            const x = touch.clientX;
            const y = touch.clientY;
            const time = (new Date).getTime()

            return { x, y, time };
        }

        const diffCoordinates = (a, b) => {

            const diff = Object.fromEntries(Object.entries(a).map(([key, value]) => {
                return [key, value - b[key]];
            }))

            diff.isRight = diff.x >= 0;

            return diff;
        }


        this.forEach((el) => {

            const lastCoordinates = { x: 0, y: 0 }

            el.addEventListener('touchstart', (event) => {

                Object.assign(lastCoordinates, getCoordinates(event));

                callback(event, { state: 'start' })

            }, { passive: true })

            const delay = debounce(30);

            el.addEventListener('touchmove', event => {

                const currentCoordinates = getCoordinates(event)

                const diff = diffCoordinates(lastCoordinates, currentCoordinates);

                delay(() => {

                    callback.call(el, event, {
                        isRight: diff.x >= 0,
                        ...diff,
                        currentCoordinates,
                        state: 'moving'
                    });

                    Object.assign(lastCoordinates, currentCoordinates)
                })
            }, { passive: true })

            el.addEventListener('touchend', (event) => {

                callback.call(el, event, {
                    state: 'end',
                    lastCoordinates
                })
            }, { passive: true })
        })
    }
}

export default qs;
