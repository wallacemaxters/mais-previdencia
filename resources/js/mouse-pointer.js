import qs from "./qs"

export default function mousePointer () {

    const $mouseCursor = qs('#mouse-cursor')[0];

    const lastPosition = {
        x: 0,
        y: 0,

        detectMousePosition(left, top) {
            return {
                left: this.left > left,
                top: this.top > top,
            }
        },

        calculateAngle(x, y) {

            const deltaX = x - this.x;
            const deltaY = y - this.y;
            const radius = Math.atan2(deltaY, deltaX);
            const deg    = radius * (180 / Math.PI)

            return { deg, radius, deltaX, deltaY };
        }
    }

    const mouseHandler = (e) => {

        const x = e.clientX
        const y = e.clientY;

        const { deg } = lastPosition.calculateAngle(x, y)

        window.requestAnimationFrame(() => {

            Object.assign($mouseCursor.style, {
                transform: `rotate(${deg + 100}deg)`,
                left: `${x}px`,
                top: `${y}px`,
            })
        })

        Object.assign(lastPosition, {x, y});

    }

    $mouseCursor && qs(document)
                    .on('mousemove', mouseHandler)
                    .on('mouseover', mouseHandler)
}
