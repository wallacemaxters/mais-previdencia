import qs from "./qs";

const createScroller = (el) => {

    const $scroller = el.querySelector('.slider-scroller');

    const $progressBar = el.querySelector('.slider-progress-bar')

    return (delta = 1, resetable = true) => {

        const max = Math.ceil($scroller.scrollWidth / $scroller.offsetWidth)

        const currentIndex = parseInt(el.dataset.index || 0);

        let index = currentIndex + delta;

        if (resetable) {
            if (index === max) {
                index = 0;
            } else if (index == -1) {
                index = max;
            }
        }

        const percent = (index / (max - 1)) * 100;

        const scrollLeft = $scroller.offsetWidth * index;

        $scroller.scrollLeft = scrollLeft;

        $progressBar.style.width = `${percent}%`;

        el.dataset.index = index;

        return $scroller;
    };
};

export default function slider () {

    qs('.slider').forEach(el => {

        const moveScroll = createScroller(el);

        const timeInterval = parseInt(el.dataset.interval, 10);

        let runningInterval = 0;

        if (timeInterval > 0) {
            runningInterval = setInterval(() => requestAnimationFrame(() => moveScroll()), timeInterval);
        }

        qs('.slider-scroller', el).swipe(function (event, data) {

            if (data.state === 'moving') {
                this.style.opacity = 0.8
                clearInterval(runningInterval);
                requestAnimationFrame(() => moveScroll(data.isRight ? 1 : -1, false))
            }

            if (data.state === 'end') {
                this.style.opacity = 1
            }
        })


        qs('.slider-control', el).on('click', function () {

            clearInterval(runningInterval);

            const delta = (this.dataset.direction === 'right') ? 1 : -1;

            requestAnimationFrame(() => moveScroll(delta));

        })
    });
}
