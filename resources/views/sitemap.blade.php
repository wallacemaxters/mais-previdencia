<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
@foreach($urls as $url)
    <url>
        <loc>{!! $url!!}</loc>
    </url>
@endforeach
@foreach($posts as $item)
<url>
    <loc>{!! route('posts.show_by_slug', ['slug' => $item->slug]) !!}</loc>
    <lastmod>{!! $item->updated_at->format('c') !!}</lastmod>
</url>
@endforeach
</urlset>
