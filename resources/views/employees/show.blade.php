@extends('layouts.main')

@push('content')

<div class="hidden lg:block">
    <x-layout.main-menu />
</div>

<div class="flex-1 flex justify-center items-center bg-image-glass lg:py-20 h-screen">
    <div class="m-auto shadow-lg">
        <x-employees.business-card :employee="$employee" />
    </div>
</div>

@endpush


@section('meta')
<x-head.meta
    :title="$employee->full_name"
    :image="$employee->image_url"
    :description="sprintf('%s na TGL - %s', $employee->position, $employee->skill->name ?? 'Especialista em Benefícios')"></x-head.meta>
@stop
