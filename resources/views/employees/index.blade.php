@extends('layouts.default')

@section('content')
    <div class="bg-third bg-fixed bg-cover bg-image-glass overflow-hidden">
        <x-layout.container :padding="true">
            <x-layout.bordered-title class="text-white">
                <x-slot name="title">
                    <h1>Nossa Equipe</h1>
                </x-slot>
            </x-layout.bordered-title>
            <div class="grid lg:grid-cols-3 gap-12 justify-center items-center">
                @foreach($employees as $employee)
                    <div class="shadow-lg transition-all duration-600 ease-in transform hover:scale-105">
                        <x-employees.card :employee="$employee" data-aos="zoom-out" />
                    </div>
            @endforeach
            </div>
        </x-layout.container>
    </div>
@stop

@section('meta')
<x-head.meta description="Conheça os profissionais que ajudam a construir a história da TGL Consultoria Financeira" title="Nossa Equipe" />
@stop
