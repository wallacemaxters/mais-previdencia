@php

$OG = ($OG ?? []) + [
    'type'        => 'website',
    'title'       => $__env->yieldContent('title') ?: config('site.title'),
    'description' => config('site.short_description'),
    'image'       => url('/static/img/logo.webp'),
    'url'         => app('url')->current(),
];

@endphp
<!DOCTYPE html>
<html lang="pt">
<head>
    <title>
        @yield('title') @hasSection('title') | @endif {!! config('site.title') !!}
    </title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    @hasSection('meta')
    @yield('meta')
    @else
    <x-head.meta />
    @endif

    <link rel="canonical" href="{!! app('url')->current() !!}">
    <link rel="shortcut icon" href="{!! asset('static/img/favicon.ico') !!}" type="image/x-icon">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="preconnect" href="https://cdnjs.cloudflare.com" crossorigin>

    <x-head.preload-style href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet" />
    <x-head.preload-style href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700&display=swap" rel="stylesheet" />
    <x-head.preload-style href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
    <link rel="stylesheet" href="{!! asset('static/css/app.css')!!}">

    <script src="{!! asset('static/js/app.js') !!}" defer></script>
    @stack('head')
</head>
<body class="flex flex-col min-h-full bg-white text-neutral-800 font-sans">@stack('content')</body>
</html>
