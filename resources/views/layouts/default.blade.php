@extends('layouts.main')

@push('content')
    <x-layout.main-menu />
    <div class="flex-grow">@yield('content')</div>
    <x-layout.footer />

    <x-layout.mouse-cursor-logo id="mouse-cursor" />

@endpush
