@extends('layouts.default')


@section('title', $product->name)

@section('content')
<x-layout.hero :image="$product->image_url">
    <x-slot name="title">{{ $product->name }}</x-slot>
</x-layout.hero>
<x-layout.container padding>
    <div class="post-content max-w-[44rem] mx-auto text-justify">{!! $product->content !!}</div>
    <div class="flex flex-col items-center justify-center mt-16">
        <x-button
            class="mt-5 px-12 py-5 rounded gap-2"
            :href="$product->action_url">
            <span>Entre em contato</span>
        </x-button>
    </div>
</x-layout.container>
@endsection

@section('meta')
<x-head.meta :title="$product->name" :description="$product->excerpt" :image="url($product->image_url)" />
@stop
