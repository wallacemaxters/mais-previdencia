@extends('layouts.default')

@section('content')
    <x-layout.hero
        :title="$category->name ?? 'Conheça todas nossas soluções'"
        :video="$category->video_url ?? null"
    />
    <x-layout.container padding>
        <x-layout.bordered-title :title="$category->description ?? 'Nossos produtos'" />
        <div class="grid gap-y-5 lg:grid-cols-3 lg:gap-10">
            @foreach ($products as $key => $item)
                <div data-aos="zoom-out" data-aos-delay="{!! 300 * ($key % 3) !!}" class="h-full">
                    <x-products.card :product="$item" class="h-full" />
                </div>
            @endforeach
        </div>
    </x-layout.container>
@endsection

@section('meta')
@isset($category)
<x-head.meta
    title="Produtos {!! Str::lower($category->name) !!}"
    :image="$products[0]->image_url ?? null"
    :description="$category->description ?? 'Conheça nosso produtos'"
/>
@endisset
@stop

