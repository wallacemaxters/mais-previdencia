<fieldset class='block'>
    <legend class="text-xl font-bold mb-5 block">{{ $formGroup->name }}</legend>
    <div>
        <div @class($formGroup->attributes['class'] ?? null)>
            @foreach($formGroup->fields as $field)
                @if('select' === $field->type)
                    <x-select
                        :label="$field->label"
                        :name="$field->name"
                        :type="$field->type"
                        :items="$field->attributes['items']"
                        :label-class="$field->attributes['labelClass'] ?? null"
                    />

                @elseif ('upload' === $field->type)
                    <x-layout.upload-box
                        :title="$field->label"
                        :name="$field->name"
                        :label-class="$field->attributes['labelClass'] ?? null"
                    />
                @else
                    <x-input
                        :label="$field->label"
                        :name="$field->name"
                        :type="$field->type"
                        :hint="$field->attributes['hint'] ?? null"
                        :label-class="$field->attributes['labelClass'] ?? null"
                    >
                        @php
                            $component->attributes->setAttributes((array) $field->attributes)
                        @endphp
                    </x-input>
                @endif
            @endforeach
        </div>
    </div>
</fieldset>
