@php
$OG = [
    'site_name'   => config('site.title'),
    'type'        => $type ?? 'website',
    'title'       => $title ?? config('site.title'),
    'description' => $description ?? config('site.short_description'),
    'image'       => $image ?? url('static/img/logo.webp'),
    'url'         => $url ?? app('url')->current(),
];

if ($OG['type'] === 'article') {

    $OG += [
        'article:published_time' => $publishedTime->format('c'),
        'article:modified_time'  => $modifiedTime->format('c'),
    ];
}

@endphp
<meta name="description" content="{{ Str::limit($OG['description'], 165) }}">
@foreach($OG as $key => $value)
    <meta property="og:{!! $key !!}" content="{{ $value }}">
@endforeach

<meta name='twitter:card' content='summary_large_image'>

@foreach(['image', 'title', 'description'] as $key)
    <meta name="twitter:{{ $key }}" content="{{ $OG[$key] }}">
@endforeach
