@props(['theme', 'name' => '', 'value' => null])

@php

$themes = [
    'primary'   => 'ring-primary checked:bg-primary',
    'secondary' => 'ring-secondary checked:bg-secondary',
    'mp'        => 'ring-mp checked:bg-mp',
];

$radioClasses = ['h-4 w-4 rounded-full appearance-none bg-neutral-300', $themes[$theme ?? 'primary']];
@endphp

<label class="flex items-center gap-x-2 py-1 cursor-pointer">
    <input {{ $attributes->class($radioClasses)->merge(['checked' => old($name) === $value]) }} type="radio" name="{{ $name }}" value={{ $value }}>
    <span>{{ $slot }}</span>
</label>
