<label class="flex gap-x-4 items-center py-2 cursor-pointer">
    <input
        {{ $attributes->except('class') }}
        type="checkbox"
        class="appearance-none ring-1 ring-secondary bg-white checked:bg-secondary h-3 w-3 transition shadow-lg" />
    <span class='user-select-none text-gray-500'>{{ $slot }}</span>
</label>
