@props([
    'name'       => null,
    'value'      => null,
    'label'      => null,
    'labelClass' => null,
    'hint'       => null,
    'gutter'     => false,
    'type'       => 'text'
])

@aware(['theme' => 'light'])

@php

$classes = [
    'appearance-none',
    'flex',
    'py-4',
    'outline-none',
    'transition',
    'flex-1',
    'w-full',
    'group-focus-within:placeholder:opacity-0 placeholder:transition-all',
    'font-sans',
    '-mx-3' => $gutter
];


$themes = [
    'outlined' => 'px-0 text-current placeholder:text-neutral-300 bg-transparent border-solid border-b-2 border-secondary',
    'outlined-dark' => 'px-0 text-white placeholder:text-neutral-300 bg-transparent border-solid border-b-2 border-white',
    'light'    => 'group-focus-within:border-secondary px-6 text-black bg-white border-2 border-gray-200',
    'dark'     => 'px-6 text-white bg-primary',
];

$classes[] = $themes[$theme];


@endphp

<label @class(['group block flex-1', $labelClass, 'py-4' => empty($label), 'px-3' => $gutter ])>
    <span @class([
        'block',
        'transition-all',
        'leading-none',
        'text-sm',
        'opacity-0
        text-current
        absolute
        group-focus-within:-translate-y-4
        group-focus-within:opacity-100' => empty($label),
        'font-bold',
        '-mx-3' => $gutter
    ])>
        {{ $label ?? $attributes->get('placeholder') }}
    </span>
    @if($type === 'textarea')
        <textarea name="{{ $name }}" {{ $attributes->class($classes) }}>{{ $value ?? old($name ?? '') }}</textarea>
    @else
        <input type="{{ $type }}" name="{{ $name }}" {{ $attributes->class($classes) }} value="{{ $value ?? old($name ?? '') }}" />
    @endif
    @if($hint)
        <span class="text-[0.65rem] text-neutral-400">{{ $hint }}</span>
    @endif

    @if($name && $errors->has($name))
        <x-layout.input-message>{{ $errors->first($name) }}</x-layout.input-message>
    @endif
</label>

