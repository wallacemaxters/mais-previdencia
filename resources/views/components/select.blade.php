@props([
    'name',
    'items' => [],
    'value' => null,
    'labelClass' => null,
])
<div class="w-full relative group x-select">
    <div class="relative flex items-center cursor-pointer">
        <x-svg src="input-select-icon.svg" height="30" width="30" class="absolute right-2 pointer-events-none text-secondary" />
        <x-input
            :readonly="true"
            :value="$items[$value] ?? $items[old($name ?? '')] ?? null"
            {{ $attributes->class('mb-0 x-select-input') }} />
    </div>
    @if($name && $errors->has($name))
        <x-layout.input-message>{{ $errors->first($name) }}</x-layout.input-message>
    @endif
    <div class="relative divide-y hidden x-select-menu">
        <ul class="absolute w-full bg-white top-0 left-0 max-h-[20rem] overflow-auto">
            @foreach($items as $key => $item)
                @php
                    $optionValue = is_numeric($key) ? $item : $key;
                @endphp
                <label class="block cursor-pointer text-neutral-500">
                    <input
                        value="{{ $optionValue }}"
                        data-label="{{ $item }}"
                        type="radio"
                        class="peer x-select-menu-item"
                        hidden
                        name="{{ $name }}"
                        @if( $value === $optionValue || ($items[old($name ?? '')] ?? null) )
                            checked
                        @endif
                    />
                    <li class="px-5 py-3 peer-checked:bg-secondary peer-checked:text-white top-0">
                        {{ $item }}
                    </li>
                </label>
            @endforeach
        </ul>
    </div>
</div>
