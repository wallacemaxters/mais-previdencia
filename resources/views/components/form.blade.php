@props(['theme' => 'light'])
<form {{ $attributes->merge(['autocomplete' => 'off']) }}>
    @csrf
    {{ $slot }}
</form>
