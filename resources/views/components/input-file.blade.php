 <input type="file" {{ $attributes->class(
    'block
    w-full
    text-sm text-current
    cursor-pointer
    file:mr-4
    file:py-2
    file:px-4
    file:rounded file:border-0
    file:text-sm
    file:bg-violet-50 file:text-secondary')
}}/>
