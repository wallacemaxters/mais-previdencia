@props(['href', 'gradient', 'color', 'theme'])
@php
$classes = [
    'appearance-none',
    'px-6 py-4 text-sm',
    'flex flex-nowrap',
    'items-center',
    'justify-center',
    'focus:right-2',
    'transition-all',
    'rounded-sm',
    'font-bold',
    '2xl:text-xl',
    isset($gradient) && $gradient === true ? 'bg-gradient-to-br from-primary to-secondary' : null,
];

$themes = [
    'default'   => 'bg-secondary hover:bg-secondary-600 text-white',
    'secondary' => 'bg-secondary hover:bg-secondary-600 text-white',
    'primary'   => 'bg-primary hover:bg-primary-600 text-white',
    'third'     => 'bg-third hover:bg-third-600 text-white',
    'dark'      => 'bg-black/70 hover:bg-black text-white',
    'outlined-dark' => 'border-2 border-black/70 hover:bg-black hover:text-white',
    'outlined-primary' => 'border-2 border-primary/70 hover:bg-primary text-primary hover:text-white',
    'outlined-secondary' => 'border-2 border-secondary/70 hover:bg-secondary text-secondary hover:text-white'
];

$classes[] = $themes[$color ?? $theme ?? 'default'] ?? null;

@endphp

@isset($href)
    <a {{ $attributes->class($classes) }} href="{{ $href }}">{{ $slot }}</a>
@else
    <button {{ $attributes->class($classes) }} @class($classes)>{{ $slot }}</button>
@endif
