@props(['title' => null, 'size' => 'default', 'space' => true])
@php
$sizes = [
    'small'   => '2xl:text-2xl text-xl',
    'default' => '2xl:text-4xl text-3xl',
    'large'   => '2xl:text-5xl text-4xl'
];

$titleClasses = [
    'text-center',
    $sizes[$size]
];

@endphp

<header {{ $attributes->class(['flex flex-col justify-center items-center group', 'mb-12' => $space]) }}>
    <div class="text-center text-xl">{{ $slot }}</div>
    <div @class($titleClasses)>{{ $title }}</div>
    <div class="h-[0.3rem] w-12 mt-1 group-hover:w-12 rounded-md bg-primary"></div>
</header>
