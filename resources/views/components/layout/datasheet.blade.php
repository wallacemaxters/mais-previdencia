@props(['contactDatasheetType' => null])
@php
$legendClasses = ['py-3 text-3xl'];
@endphp
<div>
    @if($errors->any())
    <div>
        @foreach($errors->all() as $value)
            <div>{{ $value }}</div>
        @endforeach
    </div>
    @endif

    @csrf
    <input type="hidden" name="contact_datasheet_type_id" value="{{ $contactDatasheetTypeId }}">
    <legend @class($legendClasses)>Informações Pessoais</legend>
    <x-input name="name" placeholder="Nome Completo" />
    <div class="grid md:grid-cols-3 gap-x-3">
        <x-input name="birth_date" type="tel" data-mask="99/99/9999" placeholder="Data de Nascimento" required />
        <x-input name="cpf" inputmode="numeric" data-mask="999.999.999-99" placeholder="CPF" />
        <x-input name="rg" placeholder="RG" />
    </div>

    <legend @class($legendClasses)>Dados de Contato</legend>
    <div class="grid md:grid-cols-2 gap-3">
        <x-input name="phone" data-mask="(99)99999-9999" placeholder="Telefone" />
        <x-input name="email" placeholder="E-mail" />
    </div>

    <legend @class($legendClasses)>Endereço</legend>
    <div class="grid grid-cols-2 md:grid-cols-7 gap-3">
        <div>
            <x-input name="zip_code" data-cep data-mask="99.999-999" placeholder="CEP" />
        </div>
        <div class="col-span-2 md:col-span-4">
            <x-input name="street" data-logradouro placeholder="Logradouro, Número e Complemento" />
        </div>
        <div class="md:col-span-2">
            <x-input name="district" data-bairro placeholder="Bairro" />
        </div>
    </div>

    <div class="grid md:grid-cols-2 gap-3">
        <x-input name="city" data-complemento placeholder="Cidade" />
        <x-select data-uf placeholder="Estado" name="state" :items="config('site.brazil_states')" />
    </div>

    <legend @class($legendClasses)>Informações Profissionais</legend>

    <div>
        <x-input name="crm" placeholder="Número do CRM" />
    </div>
    <div>
        <x-input name="position" placeholder="Atividade Profissional/Especialidade" />
    </div>

    <legend @class($legendClasses)>Dados do NIT/PIS/PASEP</legend>

    <div class="grid md:grid-cols-3 gap-3">
        <x-input name="nit_number" placeholder="NIT" />
        <x-input name="pis_number" placeholder="PIS" />
        <x-input name="pasep_number" placeholder="PASEP" />
    </div>

    <legend @class($legendClasses)>Informações Adicionais</legend>

    <div class="grid gap-6 md:grid-cols-2 items-center">
        <div>
            Modelo de Declaração de Imposto de Renda Pessoa Física:
            <x-radio theme="secondary" name="ir_type" value="C">Completo</x-radio>
            <x-radio theme="secondary" name="ir_type" value="S">Simplificado</x-radio>
        </div>
        <div>
            Contribui para o Plano de Previdência Privada?
            <x-radio value="1" theme="secondary" name="has_private_pension_contribution">Sim</x-radio>
            <x-radio value="0" theme="secondary" name="has_private_pension_contribution">Não</x-radio>
        </div>
    </div>


    <div class="grid md:grid-cols-3 gap-x-6 place-items-center">
        <x-input name="number_of_children" placeholder="Número de Filhos" type="number" />
        <x-input name="childrens_age" placeholder="Idade dos Filhos" />
        <x-input name="monthly_income" placeholder="Renda Mensal" type="number" step="0.01" min="0.00" />
    </div>

    <legend @class($legendClasses)>Documentos Necessários</legend>
    <div class='grid md:grid-cols-2 gap-6'>
        @foreach([
            'procuracao_inss_attachment'           => 'Procuração INSS devidamente assinada',
            'residency_proof_attachment'           => 'Comprovante de residência recente (máximo 3 meses)',
            'last_irpf_attachment'                 => 'Cópia das duas últimas declarações de IRPF completas',
            'retired_concession_letter_attachment' => 'Se for aposentado, anexar a carta de concessão com a memória de calculo.',
            'enrollment_proof_inss_attachment'     => 'Comprovante de inscrição individual - INSS (se possuir)',
            'pasep_or_pis_or_nit_attachment'       => 'Cópia do PIS, PASEP ou NIT',
            'ctc_attachment'                       => 'Se emitiu certidão para órgão público, anexar cópia da CTC',
            'crm_attachment'                       => 'Cópia do CRM',
        ] as $name => $item)
            <x-layout.upload-box :name="$name" :title="$item" />
        @endforeach
    </div>


    <div class="mt-5">
        <legend @class($legendClasses)>Procuração</legend>
        <x-layout.upload-box name="procuration_attachment" title="Enviar Procuração Assinada">
        </x-layout.upload-box>
        <div class="flex justify-end mt-5">
            <x-button download="Producação Inss.pdf" theme="primary" class="gap-2 bg-opacity-30" :href="asset('static/procuracao_inss_modelo.pdf')">
                <x-svg src="pdf.svg" width="25"  />
                <span>Baixar Procuração</span>
            </x-button>
        </div>
    </div>


    <div class="text-white mt-5">
        <legend @class($legendClasses)>Informações para apresentação do relatório</legend>
        <p>
            A data de apresentação do relatório é de até 30 dias da data da entrega da documentação completa pelo cooperado.
        </p>
    </div>

    <div class="flex justify-center md:justify-end mt-12">
        <x-button theme="secondary" class="px-12">Enviar</x-button>
    </div>
</div>
