<img
    {{ $attributes->except(['src', 'class']) }}
    loading="eager"
    src="{{ $src }}"
    @class([
        'pointer-events-none',
        'w-full',
        'h-full',
        'top-0 left-0',
        $position ?? 'absolute',
        'object-center',
        'object-cover',
        $attributes->get('class')
    ])
>
