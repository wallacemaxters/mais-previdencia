<article class="shrink-0 w-full">
        <blockquote class="bg-primary-700 bg-opacity-50 px-10 py-5 lg:h-32 rounded-2xl text-center shadow-lg mb-5 lg:text-lg leading-2 flex items-center lg:w-3/4 mx-auto">
        <i class="fas fa-quote-left fa-4x absolute mix-blend-overlay text-gray-600 animate-pulse"></i>
        <span class="z-10 relative">
        A {{ config('site.title') }} é uma empresa muito pellentesque ipsum pede malesuada non vulputate at euismod et
        lorem nullam tincidunt urna
        eget nisl
        eleifend vulputate sed vitae risus sed enim fermentum venenatis sed augue eros tempor sit amet.</span>
    </blockquote>
    <figure class="flex flex-col items-center">
        <img alt="comentários" loading="lazy" src="{{ asset('static/img/pages/home/comment.jpg') }}" class="rounded-full h-32 w-32 border-4 border-secondary border-opacity-50 object-center object-cover">
        <figcaption class="text-center mt-2">
            <h6 class="text-lg font-semibold leading-none">Marília Úria Pinhal</h6>
            <em class="text-xs">CEO na Zinfa LTDA.</em>
        </figcaption>
    </figure>
</article>
