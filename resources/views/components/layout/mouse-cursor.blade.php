<div {{ $attributes }} style="top: 0; left: 0; z-index: 9999"
    class="animate-beat
    flex justify-center items-center
    transition-all
    border border-gray-400
    rounded-full
    h-6 w-6
    shadow-lg
    fixed">
    <div class="h-3 w-3 bg-gray-400 rounded-full opacity-50"></div>
</div>
