<div @class(['text-red-500 text-xs', $class ?? null])>{{ $slot }}</div>
