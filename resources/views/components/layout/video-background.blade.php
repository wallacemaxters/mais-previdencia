@props([
    'height' => 'h-full',
    'width'  => 'w-full',
    'position' => 'absolute',
    'fit' => 'object-cover',
    'src'
])
<video
    muted
    autoplay
    loop
    loading="lazy"
    src="{{ $src }}"
    {{ $attributes->class([ 'pointer-events-none', 'top-0 left-0', 'object-center', $width, $height, $position, $fit ]) }}>
</video>
