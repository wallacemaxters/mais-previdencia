<header @class(['bg-white shadow-sm fixed w-full z-50 top-0 left-0', $class ?? null])>
    <div class="mx-auto container">
        <div class="flex 2xl:h-24 h-20 items-center">
            <a href="/" title="{{ config('site.title') }}" class='h-full'>
                <x-svg src="logo.svg" class="2xl:w-48 w-32 h-full" width="150" height="80" />
            </a>

            <nav class="flex-grow">
                <ul id="main-menu-links" @class([
                    'flex uppercase transform',
                    'flex-col translate-y-24 -translate-y-full w-full absolute top-0 bg-white left-0 z-10' => (boolean) 'mobile',
                    'lg:translate-y-0 lg:text-sm lg:h-full lg:static lg:flex-row lg:justify-end lg:items-center lg:gap-x-5 lg:bg-transparent' => (boolean) 'desktop',
                ])>
                    @foreach($items ?? $MENU as $url => $title)
                    <a href="{!! $url !!}"
                        @class([
                            'flex flex-col',
                            'lg:px-0 lg:bg-transparent p-5',
                            'hover:text-primary-200',
                            request()->fullUrl() == $url ? 'bg-white' : 'bg-gray-100',
                            'group'
                        ])>
                        <span class="lg:py-1">{!! $title !!}</span>
                        <span @class([
                            'lg:h-1 lg:w-5',
                            'rounded-lg mx-auto',
                            'bg-white group-hover:bg-primary transform translate-y-1 group-hover:translate-y-0 transition-all',
                            'bg-primary animate-pulse' => request()->fullUrl() == $url
                        ])></span>
                    </a>
                    @endforeach
                </ul>
            </nav>

            <a id="btn-mobile-menu" class='lg:hidden flex-grow flex justify-end cursor-pointer'>
                <i class="fas fa-bars text-secondary fa-2x"></i>
            </a>
        </div>
    </div>
</header>
@empty($noGutter)
<div class="2xl:h-24 h-20"></div>
@endempty
