<div class="slider relative w-full" data-index="{{ $index ?? 0 }}" data-interval="{{ $interval ?? 6000 }}">
    <div @class(['overflow-hidden transition-all duration-700', 'w-full flex relative flex-nowrap slider-scroller', $class ?? null])>
        {{ $slot }}
    </div>

    <div class="h-1 w-96 max-w-full mx-auto bg-gray-300 bg-opacity-50 rounded-full overflow-hidden mt-5">
        <div
            style="width: 0%"
            class="slider-progress-bar
            h-full
            bg-gradient-to-br from-primary to-secondary
            duration-700
            transition-all"></div>
    </div>

    @empty($disableControls)

        @foreach([
            [
                'icon'      => 'fa-arrow-left',
                'position'  => 'left-0',
                'direction' => 'left',
            ],
            [
                'icon'      => 'fa-arrow-right',
                'position'  => 'right-0',
                'direction' => 'right',
            ]
        ] as $button)

            <a @class([
                'hidden',
                'absolute top-1/2',
                'transform -translate-y-1/2',
                'slider-control',
                'text-white',
                'cursor-pointer',
                'rounded-full',
                'bg-white bg-opacity-90 text-secondary shadow',
                'w-12 h-12',
                'md:flex justify-center items-center',
                $button['position']
            ])
                data-direction="{{ $button['direction'] }}"
            >
                <i @class(['fas text-xl', $button['icon']])></i>
            </a>
        @endforeach

    @endempty
</div>
