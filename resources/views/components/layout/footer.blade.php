@php

$links = array_merge($items ?? $MENU, [
    route('pages.mais_previdencia_novos_colaboradores') => 'Atendimento aos Novos Colaboradores'
]);

@endphp

<div class="bg-gradient-to-br from-primary  to-secondary text-white">
     <x-layout.container :padding="true">
        <div class="grid grid-cols-1 gap-12 lg:grid-cols-3 lg:grid-rows-1 lg:gap-24">
            <nav>
                <h4 class="font-semibold text-xl mb-3">Mapa do Site</h4>
                <ul class="divide-y divide-gray-400">
                   @foreach($links as $key => $item)
                       <li><a href="{!! $key !!}" class="block py-3">{!! $item !!}</a></li>
                   @endforeach
                </ul>
            </nav>
            <nav>
                <h4 class="font-semibold text-xl mb-3">Privacidade</h4>
                    <ul class="divide-y divide-gray-400">
                    @foreach([
                        route('pages.about_cookies')    => 'Aviso sobre uso de Cookies',
                        route('pages.about_privacity')  => 'Aviso sobre Privacidade',
                    ] as $key => $item)
                        <li><a href="{!! $key !!}" class="block py-3">{!! $item !!}</a></li>
                    @endforeach
                </ul>

            </nav>
            <div>
                <h4 class="font-semibold text-xl mb-3">Blog</h4>
                <ul class="flex flex-col gap-y-5 divide-y divide-gray-400 divide-reverse -mt-5">
                    @foreach($POSTS as $item)
                    <li @class(['pt-5', 'text-gray-300' => $item->url === request()->fullUrl()])>
                        <a href="{!! $item->url !!}">
                           <h5>{{ $item->title }}</h5>
                           <time class="block text-xs text-gray-400 mt-2" datetime="{!! $item->posted_at->format('Y-m-d') !!}">
                               {!! $item->posted_at->format('d/m/Y') !!}
                           </time>
                       </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
     </x-layout.container>
 </div>
 <div class="bg-gray-900 py-5 text-gray-400">
     <x-layout.container class='flex flex-col gap-y-5 justify-center text-center lg:flex-row lg:justify-between'>
        <p>
            &copy; {!! date('Y') !!} - Todos os direitos reservados - {!! config('site.title') !!}.
        </p>
        <p>Desenvolvido por <a target="_blank" href="https://trigmainc.com/" title="TrigmaInc.">TrigmaInc.</a></p>
     </x-layout.container>
 </div>
