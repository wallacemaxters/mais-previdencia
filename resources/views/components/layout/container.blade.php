<div @class([
    'container',
    'mx-auto',
    'py-10 lg:py-20' => $padding ?? false,
     $class ?? null
]) {{ $attributes->except('padding') }}>{{ $slot }}</div>
