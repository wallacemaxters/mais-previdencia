@props(['name', 'title', 'labelClass' => null   ])
<label @class(['bg-secondary/30 hover:bg-primary/60 transition p-5 flex flex-col rounded cursor-pointer shadow-md', $labelClass])>
    <strong class="block mb-2 grow">{{ $title }}</strong>
    <x-input-file :name="$name" {{ $attributes }} />

    @if($errors->has($name))
    <div class="mt-5">
        <x-layout.input-message>{{ $errors->first($name) }}</x-layout.input-message>
    </div>
    @endif
</label>
