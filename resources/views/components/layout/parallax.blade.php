@props(['speed' => 3])
<section {{ $attributes->class('rellax')->merge(['data-rellax-speed' => $speed]) }}>{{ $slot }}</section>
