<div @class([
    'h-1 w-5',
    'rounded-lg',
    'bg-white group-hover:bg-primary transition-all',
    'bg-primary animate-pulse',
    $attributes['class']
])></div>
