<div {{ $attributes }}
    style="top: 0; left: 0; z-index: 9999; will-change: top, left;"
    class="hidden md:block h-5 w-5 transition-all duration-400 ease-linear fixed pointer-events-none">
    <x-svg src="icon.svg" class="animate-beat w-full h-full" />
</div>
