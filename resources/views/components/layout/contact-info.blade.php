@php
$items = [
    'fa-phone animate-pulse' => [
        'title' => 'Fale com a gente',
        'value' => config('site.phones.landline')
    ],
    'fa-envelope' => [
        'title' => 'Envie-nos um e-mail',
        'value' => config('site.emails.contact')
    ],
    'fa-clock animate-spin' => [
        'title' => 'Segunda - Sexta',
        'value' => 'De 09:00 às 17:00'
    ],
    'fa-map-marker-alt animate-bounce' => [
        'title' => 'Nosso Endereço',
        'value' => config('site.address')
    ]
];
@endphp
<div class='bg-gradient-to-t from-primary to-secondary relative text-white'>
    <x-layout.video-background :src="asset('static/videos/pages/address.mp4')" class="mix-blend-multiply" />
    <x-layout.container :padding="true">
        <ul class="grid grid-rows-4 justify-center gap-y-5 lg:gap-y-0 lg:grid-cols-4 lg:grid-rows-1 relative z-5">
            @foreach($items as $icon => $item)
            <li class="flex gap-x-5">
                <div class="flex-none rounded-full border border-gray-300 h-14 w-14 flex justify-center items-center text-2xl font-light">
                    <i @class(['fas', $icon, 'text-gray-300'])></i>
                </div>
                <div class="grid grid-flow-row auto-rows-min gap-1">
                    <strong>{!! $item['title'] !!}</strong>
                    <small class="text-gray-200">{!! $item['value'] !!}</small>
                </div>
            </li>
            @endforeach
        </ul>
    </x-layout.container>
</div>
