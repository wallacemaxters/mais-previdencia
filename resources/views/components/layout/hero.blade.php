@props([
    'subtitle' => null,
    'title',
    'image' => null,
    'video' => null
])
@php
$coverClasses = [
    'mix-blend-soft-light',
    'pointer-events-none',
    'w-full h-full',
    'absolute top-0 left-0',
    'object-cover object-center',
];
@endphp

<section @class([
        'bg-gradient-to-br from-primary-500 to-secondary-500',
        'text-white',
        'relative',
        'px-12 lg:px-32',
        'group'
    ]) >
    @isset($image)
        <img src="{{ $image }}" @class($coverClasses) alt="{{{ strip_tags($title) }}}">
    @endisset

    @isset($video)
        <video
            muted
            autoplay
            loop
            src="{{ $video }}"
            @class($coverClasses)>
        </video>
    @endisset
    <x-layout.container class="h-96 flex flex-col justify-center items-center relative z-1">
        <h1 class="lg:text-5xl text-3xl uppercase text-center">{{ $title }}</h1>
        @isset($subtitle)
        <div class="text-center lg:text-3xl font-light mt-5">{{ $subtitle }}</div>
        @endisset
    </x-layout.container>
</section>
