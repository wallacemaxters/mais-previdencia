@php
$socialNetworks = [
    [
        'icon' => 'whatsapp.svg',
        'url'  => $employee->whatsapp ? sprintf('https://api.whatsapp.com/send?phone=%d&text=Olá+Gostaria+de+uma+consultoria', $employee->whatsapp) : null
    ],
    ['icon' => 'instagram.svg', 'url' => $employee->instagram],
    ['icon' => 'linkedin.svg', 'url' => $employee->linkedin],
    ['icon' => 'mail.svg', 'url' => $employee->email ? 'mailto:' . $employee->email : null],
    ['icon' => 'phone.svg', 'url' => $employee->phone ? 'tel:' . $employee->phone : null],
    ['icon' => 'web.svg', 'url' => $employee->web ?: route('employees.show_by_slug', $employee->slug, false)]
];

$bg = asset('static/img/background-employee-card.webp');

@endphp
<div
    {{ $attributes->except(['class', 'employee']) }}
    style="background-image: url('{!! $bg !!}')"
    class="bg-gray-700 text-third radius-sm shadow-xl font-secondary bg-[length:100%_100%]">
    <div class="flex justify-end items-center text-left ml-auto w-10/12 lg:w-4/5 divide-x-2 2xl:h-24 h-20 px-12 text-gray-50">
        <div class="pr-3 text-xs">{{ $employee->skill ?? 'Especialista em Benefícios' }}</div>
        <div class="pl-3">
            <x-svg src="logo-white.svg" width="60" />
        </div>
    </div>

    <div>
        <div
            style="background-image: url({!! $employee->image_url !!});"
            class="block bg-top bg-cover border-2 border-gray-800 ring-4 ring-current rounded-full overflow-hidden 2xl:w-32 2xl:h-32 w-28 h-28 mx-auto relative z-10">
        </div>
        <div class="bg-black 2xl:h-8 2xl:w-40 h-6 w-32 mx-auto rounded-full filter blur-lg -mt-1"></div>
        <div class="flex flex-col justify-center items-center 2xl:mt-5 mt-3 text-center">
            <span class="2xl:text-2xl text-xl uppercase text-current font-bold">{{ $employee->full_name }}</span>
            <span class="text-gray-200 2xl:text-lg text-md uppercase tracking-wider">{{ $employee->position }}</span>
        </div>
        <div class="border-t border-secondary rounded-full w-1/3 mx-auto 2xl:mb-8 2xl:mt-4 mb-6 mt-3"></div>
    </div>
    <div class="px-20 grid grid-cols-3 gap-5 place-items-center mt-5 2xl:pb-10 pb-6">
        @foreach($socialNetworks as $item)
            <div>
                <a href="{{ $item['url'] }}" rel="nofollow" @class([
                    'flex justify-center items-center',
                    '2xl:w-12 2xl:h-12 h-10 w-10',
                    'transition bg-current hover:bg-secondary',
                    'hover:scale-105 rounded-lg relative z-10',
                    'pointer-events-none filter grayscale' => $item['url'] === null
                ])>
                    <x-svg :src="$item['icon']" width="24" height="24" class="text-white" />
                </a>
                <div class="bg-black h-4 w-full mx-auto rounded-full filter blur-lg -mt-1"></div>
            </div>
        @endforeach
    </div>
</div>
