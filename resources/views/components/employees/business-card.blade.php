@php
$socialNetworks = [
    [
        'icon' => 'whatsapp.svg',
        'url'  => $employee->whatsapp ? sprintf('https://api.whatsapp.com/send?phone=%s&text=Olá+Gostaria+de+uma+consultoria', urlencode('+55' . $employee->whatsapp)) : null
    ],
    ['icon' => 'instagram.svg', 'url' => $employee->instagram],
    ['icon' => 'linkedin.svg', 'url' => $employee->linkedin],
    ['icon' => 'mail.svg', 'url' => $employee->email ? 'mailto:' . $employee->email : null],
    ['icon' => 'phone.svg', 'url' => $employee->phone ? 'tel:' . $employee->phone : null],
    ['icon' => 'web.svg', 'url' => $employee->web ?: route('employees.show_by_slug', $employee->slug, false)]
];

$bg = asset('static/img/background-employee-card.webp');

@endphp

<!-- Favor não fuçar, esse é o do PDF -->
<div class="bg-gray-700 font-employee text-white text-third h-full shadow-lg" style="background-size: 100% 100%; background-image: url('{{ $bg }}')">
    <div class="flex justify-end items-center text-right w-10/12 lg:w-3/4 ml-auto divide-x-2 h-24 px-12 text-gray-50 mb-5">
        <div class="pr-4 text-left text-sm whitespace-pre-wrap">{{ $employee->skill ?? 'Especialista em Benefícios' }}</div>
        <a class="pl-4" href="{!! route('pages.home') !!}">
            <x-svg src="logo-white.svg" width="75" />
        </a>
    </div>

    <div>
        <a class="block border-4 border-black ring-8 ring-current rounded-full overflow-hidden w-48 h-48 mx-auto relative z-10">
            <img loading="lazy" alt="{{ $employee->full_name }}" src="{!! $employee->image_url !!}" class="object-center object-cover w-64 h-64">
        </a>
        <div class="bg-black h-7 w-40 mx-auto rounded-full filter blur-lg -mt-3"></div>
        <div class="flex flex-col justify-center items-center mt-3 text-center">
            <span class="text-2xl font-bold uppercase text-current tracking-widest">{{ $employee->full_name }}</span>
            <span class="text-gray-200 text-lg uppercase" style="letter-spacing: 4px">{{ $employee->position }}</span>
        </div>
        <div class="border-t border-current rounded-full w-1/3 mx-auto mt-1"></div>
        <div class="tracking-widest uppercase text-md text-center mt-5 text-white" style="letter-spacing: 3.84px">Clique nos ícones:</div>
        <div class="grid grid-cols-3 gap-5 place-items-center px-20 lg:px-24 mt-8">
            @foreach($socialNetworks as $item)
                <div>
                    <a href="{{ $item['url'] }}" @class([
                        'flex justify-center items-center',
                        '2xl:w-16 2xl:h-16 h-14 w-14',
                        'transition bg-current hover:bg-secondary',
                        'hover:scale-105 rounded-lg relative z-10',
                        'pointer-events-none filter grayscale' => $item['url'] === null
                    ])>
                        <x-svg :src="$item['icon']" width="25" height="25" class="text-white" />
                    </a>
                    <div class="bg-black h-4 w-full mx-auto rounded-full filter blur-lg -mt-1"></div>
                </div>
            @endforeach
        </div>
        <div class="text-center py-12 text-xl tracking-widest text-white">
            <a href="/"><span>grupotgl</span><span class='font-light'>.com</span></a>
        </div>
    </div>
</div>
