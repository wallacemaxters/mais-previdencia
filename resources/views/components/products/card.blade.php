<div @class(['flex flex-col items-center bg-white shadow-lg transition-all transform hover:scale-105', $class ?? null]) {{ $attributes->except('class') }}>
    <div class="h-64 bg-gradient-to-br from-secondary-500 to-primary-500 px-5 w-full relative overflow-hidden flex flex-col items-center justify-center">
        <img
            width="256"
            height="256"
            loading="lazy"
            alt="{{ $product->name }}"
            title="{{ $product->name }}"
            src="{{ $product->image_url }}" class="absolute object-cover h-full w-full top-0 left-0 mix-blend-multiply">
        <h3 class="text-white text-xl text-center relative z-10">
            {{ $product->name }}
        </h3>
    </div>

    <div class="text-sm px-8 py-8 flex-grow text-gray-500 text-justify">{{ Str::limit($product->excerpt, 170) }}</div>
    <x-button
        :href="route('products.show_by_slug', $product->slug)"
        class="mb-8 py-3 px-8" color='outlined-dark'
        text-color='text-secondary hover:text-white'>
            Contrate Agora
    </x-button>     
</div>
