<a class="w-full block" href="{!! route('products.show_by_slug', $product->slug) !!}">
    <figure @class(['shadow-lg', 'flex flex-col w-full', $attributes['class']]) {{ $attributes->except(['class', 'product']) }}>
        <div class="block bg-gradient-to-tl from-secondary-500 via-transparent to-transparent">
            <img
                loading="lazy"
                src="{!! $product->image_url !!}"
                alt="{{ $product->name }}"
                title="{{ $product->name }}"
                height="320"
                width="248"
                class="h-80 w-full object-center object-cover mix-blend-multiply">
        </div>
        <figcaption class="px-5 py-3 flex items-center justify-center grow">
            <div class="text-sm text-center uppercase">{{ $product->name }}</div>
        </figcaption>
    </figure>
</a>
