@php
$svg = file_get_contents(public_path('static/svg/' . $src));
@endphp

{!! strtr($svg, ['<svg' => '<svg ' . $attributes]) !!}
