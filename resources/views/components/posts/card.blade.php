<article class="bg-white flex flex-col shadow-lg overflow-hidden pb-5" data-id="{{ $post->id }}">
    <div class="px-5 py-3">
        <a
            class="flex items-center"
            href="{!! $post->user && $post->user->employee ? route('employees.show_by_slug', ['slug' => $post->user->employee->slug]) : '#' !!}">
            <img
                lazy="loading"
                src="{{ $post->user->employee->image_url ?? asset('static/svg/icon.svg') }}"
                width="50"
                loading="lazy"
                height="50"
                alt="{{ $post->title }}"
                class="h-12 shadow-lg w-12 object-cover object-top rounded-full">
            <div class='grid ml-2'>
                <span class="text-sm font-bold">{!! $post->user->employee->full_name ?? config('site.title') !!}</span>
                <span class='text-xs text-secondary'>{!! $post->user->employee->position ?? 'Marketing' !!}</span>
            </div>
        </a>
    </div>
    <div class="h-64 relative bg-gradient-to-br from-primary to-secondary">
        @if($post->cover_url)
        <img
            width="340"
            height="256"
            title="{{ $post->title }}"
            alt="{{ $post->title }}"
            loading="lazy"
            src="{!! $post->cover_url !!}"
            class="w-full h-full object-cover object-center mix-blend-overlay">
        @endif
        <div class="text-sm font-light absolute bottom-0 right-0 p-3 text-white">
            {{ $post->posted_at->format('d/m/Y') }}
        </div>
    </div>
    <div class="px-8 mt-5 flex flex-col h-80">
        <h3 class="text-xl mb-5">{{ Str::limit($post->title, 80) }}</h3>
        <p class="text-justify text-md grow">{{ Str::limit($post->excerpt, 170) }}</p>
        <div class="flex justify-end shrink">
            <x-button theme="outlined-dark" class="px-8 py-3" :href="$post->url" outlined>Leia mais</x-button>
        </div>
    </div>
</article>
