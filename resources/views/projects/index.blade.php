@extends('layouts.default')

@section('main.title', 'Projects')
@section('title')
    <span class="text-gray-400">Know Our</span> Projects
@stop

@section('content')

<div class="grid md:grid-cols-3 md:gap-x-5 grid-cols-1 gap-y-5">
    @foreach($projects as $item)
    <a class='bg-white p-5 shadow-md hover:bg-gray-200 transition' href="{!! $item->link !!}" rel="nofollow">
        <h2 class="text-primary text-xl mb-4">{{ $item->name }}</h2>
        <p class="text-sm">{{ $item->description }} </p>
    </a>
    @endforeach
</div>

@endsection
