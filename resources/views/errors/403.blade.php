@extends('layouts.default')

@section('content')
<div class="bg-image-glass text-white">
    <x-layout.container padding>
        <x-layout.bordered-title title="Acesso não autorizado" />
        <div class="flex justify-center">
            <x-svg src="denied.svg" height="200" width="200" class="text-white filter drop-shadow-xl" />
        </div>
    </x-layout.container>
</div>
@stop
