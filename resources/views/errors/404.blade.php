@extends('layouts.default')

@section('content')
<div class="bg-white">
    <x-layout.container padding>
        <x-layout.bordered-title title="Página não encontrada" />
        <div class="flex justify-center">
            <x-svg src="warning.svg" height="200" width="200" class="text-secondary filter drop-shadow-xl" />
        </div>
    </x-layout.container>
</div>
@stop
