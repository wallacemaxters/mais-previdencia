@extends('layouts.default')

@section('title', 'Contato')

@section('content')
    <x-layout.hero
        title="Entre em contato"
        :video="asset('static/videos/pages/contact.mp4')"
        subtitle="Preencha os campos para nossa equipe entrar em contato com você!"
    />
    <div class="bg-gray-100">
        <x-layout.container :padding="true">
            <div class="grid lg:grid-cols-2 gap-12">
                <x-form theme="light" class="flex flex-col text-secondary" method="POST" :action="route('contacts.store')">
                    {!! $honeyPot !!}
                    @csrf
                    <x-input required name="name" placeholder="Seu nome" />
                    <x-input required name="email" type="email" placeholder="Seu e-mail" />
                    <x-input data-mask="(99)99999-9999" required name="phone" type="tel" placeholder="Seu telefone/celular" />
                    <x-input required name="subject" placeholder="Assunto" />
                    <x-input required name="message" type="textarea" placeholder="Mensagem" />

                    <div class='flex justify-end mt-5'>
                        <x-button class="text-lg px-8 w-full lg:w-auto" type="submit">
                            Enviar
                        </x-button>
                    </div>
                </x-form>
                <div>
                    <iframe
                        src="https://www.google.com/maps/embed/v1/place?q=-19.9537667,-43.93969249999998&key=AIzaSyARNP6PBrYdKNmsYf09tU8jRnffQb-sFQg&language=pt-br"
                        class="p-2 rounded shadow-lg bg-white w-full h-64 lg:h-96"
                        frameborder="0"
                        style="border:0"
                        allowfullscreen>
                    </iframe>
                </div>
            </div>
        </x-layout.container>
    </div>
    <x-layout.contact-info></x-layout.contact-info>
@endsection

@section('meta')
<x-head.meta title="Fale Conosco" />
@stop

