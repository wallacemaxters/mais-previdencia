@component('mail::message', ['logo' => $logo ?? null])

@php
$fields = [
    'name'            => 'Nome Completo',
    'subject'         => 'Assunto',
    'email'           => 'E-mail',
    'formatted_phone' => 'Telefone',
    'subject'         => 'Assunto',
    'message'         => 'Mensagem',
];
@endphp

Olá! Você acaba de receber um contato do site {{ config('site.title') }}.


@foreach($fields as $name => $title)

**{{ $title }}:**<br>
{{ $contact->getAttribute($name) }}

@endforeach

@endcomponent
