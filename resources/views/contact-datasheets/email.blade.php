@component('mail::message', ['logo' => $logo ?? null])

@php
$fields = [
    'name'                                       => 'Nome Completo',
    'birth_date'                                 => 'Data de Nascimento',
    'cpf'                                        => 'CPF',
    'rg'                                         => 'RG',
    'phone'                                      => 'Celular',
    'email'                                      => 'E-mail',
    'zip_code'                                   => 'CEP',
    'state'                                      => 'Estado',
    'street'                                     => 'Logradouro',
    'district'                                   => 'Bairro',
    'city'                                       => 'Cidade',
    'crm'                                        => 'Nº CRM',
    'position'                                   => 'Atividade Profissional/Especialidade',
    'nit_number'                                 => 'NIT',
    'pis_number'                                 => 'PIS',
    'pasep_number'                               => 'PASEP',
    'ir_type_formatted'                                    => 'Modelo de Declaração de IR',
    'has_private_pension_contribution_formatted' => 'Contribui para o Plano de Previdência Privada?',
    'number_of_children'                         => 'Número de Filhos',
    'childrens_age'                              => 'Idade dos Filhos',
    'monthly_income'                             => 'Renda Mensal',
];

$files = [
    'procuracao_inss_attachment'           => 'Procuração do INSS Devidademente Assinada',
    'residency_proof_attachment'           => 'Comprovante de Residência',
    'last_irpf_attachment'                 => 'Cópia das duas últimas declaração de IRPF',
    'retired_concession_letter_attachment' => 'Carta de Concessão',
    'enrollment_proof_inss_attachment'     => 'Comprovante de Inscrição Individual - INSS',
    'pasep_or_pis_or_nit_attachment'       => 'PIS/PASEP/NIT',
    'ctc_attachment'                       => 'CTC Anexo',
    'crm_attachment'                       => 'Anexo do CRM',
    'procuration_attachment'               => 'Procuração Assinada'
];

@endphp

Olá! Você acaba de receber uma ficha técnica da Cootes através do site {{ config('site.title') }}.
Segue as informações abaixo:

@foreach($fields as $name => $title)

**{{ $title }}:**<br>
{{ $contactDatasheet->getAttribute($name) }}

@endforeach

### Anexos

@foreach($files as $name => $title)
@if($contactDatasheet[$name])
- [{{ $title }}]({{ url()->signedRoute('contact_datasheets.file', ['contactDatasheet' => $contactDatasheet->id, 'name' => $name]) }})<br>
@endif
@endforeach

@endcomponent
