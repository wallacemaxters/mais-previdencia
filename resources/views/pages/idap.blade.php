@extends('layouts.default')

@section('content')
    <x-layout.hero title="Conheça o IDAP" :video="asset('static/videos/pages/moleques-futebol.mp4')" />

    <section class="bg-white">
        <x-layout.container :padding="true">
            <x-layout.bordered-title >
                <x-slot name="title">
                    <h2>SUA CONFIANÇA GERA FRUTOS</h2>
                </x-slot>
            </x-layout.bordered-title>
            <div class="post-content px-32 text-justify">
                <p>Em 2010, seguindo um direcionamento de Deus, iniciamos um trabalho social, com crianças e adolescentes em Ribeirão das Neves, município da região metropolitana de Belo Horizonte, um dos municípios mais populosos do estado de Minas Gerais e um dos menores índices de IDH – Índice de Desenvolvimento Humano do país.</p>
                <p>Em 2020 um ranking avaliando as 100 cidades mais populosas do país nos quesitos ambiente regulatório, infraestrutura, mercado, capital humano e cultura empreendedora, Ribeirão das Neves ficou apenas em 91º lugar.</p>
                <p>Esse trabalho deu origem ao IDAP - INSTITUTO DE DESENVOLVIMENTO SUSTENTAVEL E APOIO A PESSOA HUMANA DE RIBEIRAO DAS NEVES, criado em 09/11/2010.</p>
                <p>Desde 2010, já passaram pelo projeto mais de 500 crianças e adolescentes, seja nas atividades do futebol, cursos de inclusão digital, atividades artísticas, atendimento odontológico, em parceria com a entidade Dentistas do Bem.</p>
                <p>Tivemos, infelizmente, ao longo desses anos, perdas irreparáveis, crianças e jovens vítimas da violência e das drogas. Perdas que nos marcaram bastante, mas que também nos mostram a importância do nosso trabalho, a importância de darmos atenção e proporcionar a essas crianças e jovens alternativas e esperança de um futuro melhor.</p>
                <p>Temos também muito a comemorar, crianças, que ao chegarem no projeto, tinham poucas perspectivas para o futuro, mas muitos sonhos e desejos de mudarem àquela realidade, hoje são atleta profissional de futebol, biomédica, contador, psicólogos, formandos em administração de empresas, dentre outras profissões, somente no nosso grupo, na TGL, temos 6 (seis) excelentes colaboradores oriundos do projeto social (IDAP).</p>
                <p>Até aqui o projeto tem sido mantido com os recursos próprios dos sócios da TGL e com as doações de amigos que se solidarizaram com as nossas crianças e suas famílias e nos ajudam a manter o projeto vivo.</p>
                <p>Mas queremos mais, transformar mais vidas, ofertar mais oportunidades, e você, nosso cliente é peça fundamental nesse ecossistema, já que parte das receitas geradas pelo nosso relacionamento, são direcionados ao nosso projeto social.</p>
                <p>Lhe convidamos a conhecer um pouco mais sobre o IDAP, vamos municiá-lo de informações, acompanhe, aqui pelo nosso site e nossos canais de comunicação.</p>
            </div>
            <div class="justify-center flex mt-12">
                <x-button class="w-64 h-16 text-lg" :href="route('contacts.create')">Quero Ajudar!</x-button>
            </div>
        </x-layout.container>
    </section>

    <div class="bg-cover bg-image-glass text-white">
        <x-layout.container :padding="true">
            <x-layout.bordered-title >
                <x-slot name="title"><h3>Nossas Crianças</h3></x-slot>
            </x-layout.bordered-title>
            <x-layout.slider :interval="2500">
                @foreach([
                    '01.jpeg',
                    '02.jpeg',
                    '03.jpeg',
                    '04.jpeg',
                    '05.jpeg',
                    '06.jpeg',
                    '07.jpeg',
                    '08.jpeg',
                    '09.jpeg',
                    '10.jpeg',
                    '11.jpeg'
                ] as $image)
                    <div class="md:w-1/3 w-full shrink-0 md:px-5">
                        <img
                            class="object-cover object-center h-80 shadow-lg"
                            alt="IDAP"
                            title="IDAP"
                            height="344"
                            width="384"
                            loading="lazy"
                            src="{!! asset('static/img/pages/home/idap/' . $image) !!}">
                    </div>
                @endforeach
            </x-layout.slider>
            <div class="justify-center flex mt-12">
                <x-button :href="route('contacts.create')" class="w-64 h-16 text-lg bg-opacity-50 hover:bg-opacity-100">Quero Ajudar!</x-button>
            </div>
        </x-layout.container>
    </div>
@stop
