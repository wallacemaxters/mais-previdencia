@php
$inputClasses = 'rounded-lg group-focus-within:border-mp text-gray-600';
@endphp

@extends('layouts.main')

@push('content')

<header class="relative font-secondary text-neutral-500">
    <div class="flex items-center min-h-screen">
        <x-layout.container :padding="true">
            <div class="grid grid-cols-1 md:grid-cols-2 relative gap-12 items-center">
                <div class="grid gap-y-5 text-center md:text-left">
                    <x-svg src="mais-previdencia.svg" class="mb-5" :height="100" :width="400"
                        class="max-w-full p-2" />
                    <h2 class="md:text-4xl text-2xl font-bold uppercase">Curta a vida enquanto cuidamos do seu patrimonio
                    </h2>

                    <p>Plano Mais Previdência Família: <strong>o plano de previdência privada do colaborador FIEMG,
                            SENAI, CIEMG e IEL</strong>.</p>
                    <p>Segundo dados do IBGE, apenas 1% dos aposentados no Brasil conseguem manter o mesmo padrão de
                        vida durante a aposentadoria.</p>
                    <p>
                        Sabendo disso, para que <strong>você fique amparado e tranquilo</strong>, a <strong>Mais
                            Previdência</strong>, a gestora dos planos de previdência privada da FIEMG, fez uma parceria
                        com a <strong>TGL Consultoria</strong>, empresa renomada do mercado de benefícios, para cuidar
                        da área de Atendimento do Plano Mais Previdência Família.</p>
                    <p class="font-bold">Faça hoje mesmo o seu agendamento!</p>
                    <p>A cada R$ 1,00 poupado, até o limite de 4% (quatro por cento) do seu salário base, a FIEMG aporta o mesmo valor para você.</p>
                    <p>Receba a sua consultoria, conheça as regras do plano e faça a sua adesão, <strong>o seu futuro depende das suas escolhas hoje!</strong></p>
                </div>
                <div>
                    @if (session()->has('success'))
                        <div class="flex flex-col items-center">
                            <x-svg src="check.svg" class="w-48 h-48 bg-mp rounded-full" />
                            <p class="text-2xl mt-5 text-center">
                                Sua mensagem foi enviada com sucesso!
                            </p>
                        </div>
                    @else
                        <form action="{!! route('contacts.store_mais_previdencia_novos_colaboradores') !!}" method="POST">
                            {!! $honeyPot !!}
                            @csrf
                            <header class="text-center mb-5">
                                <h2 class="text-3xl md:text-4xl mb-2 font-bold">Preencha os seus dados</h2>
                                <p>Entraremos em contato</p>
                            </header>
                            <section class="grid gap-y-3 auto-rows-min text-mp">
                                <x-input type="text" name="name" placeholder="Nome" required :class="$inputClasses" />
                                <x-input type="email" name="email" placeholder="E-mail" required
                                    :class="$inputClasses" />
                                <x-input type="tel" data-mask="(99)99999-9999" name="phone" required
                                    placeholder="Telefone" :class="$inputClasses" />

                                <p class='text-neutral-500'>Você já investe em Previdência Privada?*</p>
                                <div>
                                    <x-radio theme="mp" name="previdencia_privada" value="1">Sim</x-radio>
                                    <x-radio theme="mp" name="previdencia_privada" value="0">Não</x-radio>

                                    @if($errors->has('previdencia_privada'))
                                    <x-layout.input-message>{{ $errors->first('previdencia_privada') }}</x-layout.input-message>
                                    @endif
                                </div>

                                <div class="mt-5">
                                    <x-button
                                        class="rounded-2xl px-12 bg-mp/80 shadow hover:bg-mp">Solicitar Atendimento !</x-button>
                                </div>
                            </section>
                        </form>
                    @endif
                </div>
            </div>
        </x-layout.container>
    </div>
</header>

    <x-layout.container :padding="true">
        <section class="flex flex-col md:flex-row flex-wrap justify-center">
            @foreach (['Zero taxa de carregamento', 'Flexibilidade na escolha do valor de contribuição mensal, Diminuição da base de cálculo do IR em até 12% da renda anual tributável por meio das contribuições mensais', 'Possibilidade de realizar contribuições voluntárias de caráter facultativo, periódicas ou não', 'Taxa de administração de 0,6% ao ano', 'Opção de resgates parciais para algum projeto de curto prazo a partir do 3º ano e maior rentabilidade sobre a gestão dos seus recursos', 'Viabilidade de manutenção do plano mesmo se desligando da empresa vinculada', 'Alternativa de contratar seguro de vida (morte e/ou invalidez) com preços mais atrativos que os de mercados'] as $i => $text)
                <div class="w-full md:w-1/2 h-64 p-8 text-center" data-aos="fade-up"
                    data-aos-delay="{!! $i * 150 !!}">
                    <x-svg src="check.svg" class="w-16 h-16 mb-5 text-mp mx-auto my-6" />
                    <p>{!! $text !!}</p>
                </div>
            @endforeach
        </section>
    </x-layout.container>

@endpush

@section('meta')
    <x-head.meta title="Mais Previdência"
        description="Conheça o plano de previdência privada para o colaborador FIEMG, SENAI, CIEMG e IEL."
        :image="asset('static/img/mais-previdencia.webp')" />
@endsection
