@extends('layouts.default')

@section('content')

<x-layout.hero :video="asset('static/videos/pages/about.mp4')" :title="$title" />

<x-layout.container :padding="true">
    <x-layout.bordered-title title="Por que nos escolher?" />
    <div class="post-content grid gap-5">
        <div class="grid gap-5 text-justify">
            <p>Porque nós, da Mais Previdência, ajudamos você a pensar e a planejar o seu futuro por meio da oferta de soluções de previdência privada seguras, alinhadas às tendências de mercado e flexíveis para membros da FIEMG, seus familiares, pessoas físicas e todas aquelas associadas à indústria mineira.</p>
            <p>Com a Consultoria em Seguros Mais Previdência passamos também a cuidar do seu patrimônio, seus projetos, sonhos e de quem você mais ama!
            Passamos a atuar não somente no seu planejamento mas também na sua proteção financeira e de sua família, no modelo consultivo, buscando as melhores soluções do mercado para atender às suas necessidades.
            A TGL Consultoria tem a missão de cuidar e potencializar a vida financeira e o legado dos seus mais de 30 mil clientes, por meio de um atendimento consultivo, conectando propósitos e gerando prosperidade.</p>
            <p>É uma Consultoria Financeira independente e comprometida com os seus clientes, que oferece os melhores produtos do mercado. Desde 2004 vem entregando soluções personalizadas em seguros, previdência complementar e demais benefícios, investimento, consórcio, além de consultoria estratégica contábil e tributária, que já gerou milhões em economia para os seus clientes.</p>
            <p>Tudo em um só lugar por meio de um atendimento premium. Potencialize e facilite sua vida financeira: venha ser Consultoria em Seguros Mais Previdência TGL.</p>

        </div>
    </div>
</x-layout.container>

<x-layout.container :padding="true">
    <div class="grid lg:grid-cols-3 gap-5">
        @foreach($items as $item)
        <div class="p-6 flex flex-col gap-5 shadow-lg rounded-lg">
            <x-layout.bordered-title :space="false" size="small" :title="$item['title']" />
            <x-svg :src="$item['icon']" class="w-32 mx-auto" />
            <div class="text-justify text-sm">{{ $item['text'] }}</div>
        </div>
        @endforeach
    </div>
</x-layout.container>

@unless($employees->empty())
<div id="nossa-equipe" class="bg-cover bg-image-glass text-white">
    <x-layout.container :padding="true">
        <x-layout.bordered-title title='Nosso Time'>
            <p>Esses são os profissionais que ajudam a construir nossa história</p>
        </x-layout.bordered-title>

        <x-layout.slider>
            @foreach ($employees as $employee)
            <div class='lg:w-1/3 lg:px-6 w-full shrink-0 py-12'>
                    <x-employees.card :employee="$employee" class="w-full" />
                </div>
            @endforeach
        </x-layout.slider>
    </x-layout.container>

    <div class="pb-12 flex justify-center">
        <x-button class="bg-opacity-70">Conheça nosso time</x-button>
    </div>
</div>
@endunless

@stop


@section('meta')
<x-head.meta
    title="Sobre a Consultoria "
    description="Temos a missão de cuidar e potencializar a vida financeira e o legado dos nossos mais de 30 mil clientes, por meio de um atendimento consultivo, conectando propósitos e gerando prosperidade."
/>
@stop
