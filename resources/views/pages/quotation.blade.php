@extends('layouts.default')

@section('content')

<x-layout.hero :video="asset('static/videos/pages/about.mp4')">
    <x-slot name='title'>
        Faça uma cotação
    </x-slot>
</x-layout.hero>

<x-layout.container :padding="true">

    <form action="{{ route('pages.post_quotation') }}" method="post" class="px-32 block">
        @csrf
        <div class="grid gap-y-5">
            <x-input name="pessoa_nome" placeholder="Nome" />
            <x-input name="pessoa_email" type="email" placeholder="E-mail" />
            <x-input name="pessoa_celular" type="tel" placeholder="Celular" data-mask="(99) 99999-9999" />
            <x-input name="pessoa_profissao" placeholder="Profissão" />
        </div>

        <fieldset id="seguro-auto-container" style="display: none" disabled>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <input required class="input" name="marca" type="text" placeholder="Marca do veículo"
                            value="{{ old('marca') }}" id="marca">
                        @if($errors->has('marca'))
                        <span class="error">{{ $errors->first('marca') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <input required class="input" name="modelo" type="text" placeholder="Modelo do veículo"
                            value="{{ old('modelo') }}" id="modelo">
                        @if($errors->has('modelo'))
                        <span class="error">{{ $errors->first('modelo') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <input required min="1900" class="input" name="ano_fabricacao" type="number"
                            placeholder="Ano da fabricação do veículo" value="{{ old('ano_fabricacao') }}"
                            id="ano_fabricacao">
                        @if($errors->has('ano_fabricacao'))
                        <span class="error">{{ $errors->first('ano_fabricacao') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <input min="1900" required class="input" name="ano_modelo" type="number"
                            placeholder="Ano do modelo do veículo" value="{{ old('ano_modelo') }}" id="ano_modelo">
                        @if($errors->has('ano_modelo'))
                        <span class="error">{{ $errors->first('ano_modelo') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group">

                <select name="renovacao" class="input">
                    <option value="">Renovação?</option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>
        </fieldset>

        <label class="text-gray-600 my-4 block">Selecione os produtos desejados:</label>
        <nav class="grid lg:grid-cols-3">
            @foreach($tglProducts as $item)
                <x-checkbox
                    name="produtos[]"
                    :value="$item['produto_id']"
                    :checked="in_array($item['produto_id'], (array) old('produtos'))">{{ $item['produto_nome'] }}</x-checkbox>
            @endforeach
        </nav>

        <x-layout.input-message class="mt-5">{{ $errors->first('produtos') }}</x-layout.input-message>


        <x-button type="submit" gradient class="mt-5 ml-auto">Enviar</x-button>
    </form>

    </div>
</x-layout.container>
@stop
