@extends('layouts.main')

@push('content')

<header class="relative font-secondary bg-[url(/static/img/pages/ir/ir.webp)] bg-secondary bg-blend-multiply text-white">
    <div class="flex items-center min-h-screen">
        <x-layout.container :padding="true">
            <div class="grid grid-cols-1 md:grid-cols-2 relative gap-16 items-center">
                <div class="text-center md:text-left">
                    <x-svg src="logo.svg" class="mx-auto max-w-full" :height="250"  />
                   <p class="text-lg text-justify">
                        Para saber como nós já ajudamos mais de 30 mil médicos a economizar milhões de reais em suas declarações de IR, basta preencher o formulário para que um de nossos especialistas entre em contato o mais breve possível.
                    </p>
                </div>
                <div>
                    @if (session()->has('success'))
                        <div class="flex flex-col items-center">
                            <x-svg src="check.svg" class="w-48 h-48 bg-secondary rounded-full" />
                            <p class="text-2xl mt-5 text-center">
                                Sua mensagem foi enviada com sucesso!
                            </p>
                        </div>
                    @else
                        <x-form theme="outlined" action="{!! route('contacts.store_ir') !!}" method="POST">
                            {!! $honeyPot !!}
                            @csrf
                            <header class="text-center mb-5">
                                <h2 class="text-3xl md:text-4xl mb-2 font-bold">Preencha os seus dados</h2>
                                <p>Entraremos em contato</p>
                            </header>
                            <section class="grid gap-y-3 auto-rows-mi">
                                <x-input type="text" name="name" placeholder="Nome" required />
                                <x-input type="email" name="email" placeholder="E-mail" required
                                 />
                                <x-input type="tel" data-mask="(99)99999-9999" name="phone" required
                                    placeholder="Celular" />


                                <div class="flex justify-end">
                                    <x-button>Enviar</x-button>
                                </div>
                            </section>
                        </x-form>
                    @endif
                </div>
            </div>
        </x-layout.container>
    </div>
</header>

@endpush

@section('meta')
    <x-head.meta title="Declaração do IR"
        description="Para saber como nós já ajudamos mais de 30 mil médicos a economizar milhões de reais em suas declarações de IR, basta preencher o formulário para que um de nossos especialistas entre em contato o mais breve possível." />
@endsection
