@extends('layouts.main')

@push('content')

<div class="relative pb-8 md:min-h-screen flex flex-col justify-center items-center bg-gradient-to-br from-black/50 to-primary/50">
    <x-layout.image-background
        class="mix-blend-multiply"
        position="absolute"
        :src="asset('static//img/pages/tgl-suit/fundo.jpg')"
    />
    <div class="relative">
        <div class="flex justify-center">
            <img data-aos="fade-up" src="{{ asset('static/img/pages/tgl-suit/logo.webp') }}" class="w-80 md:w-1/4" />
        </div>
        <x-layout.container>
            <div class="grid md:grid-cols-2 gap-12 text-white">
                <div class="grid gap-6 place-content-center">
                    <h1 class="text-2xl md:text-3xl" data-aos="fade-up" data-aos-delay="150">Conheça a evolução do Mercado de Benefícios</h1>
                    <div class="text-sm md:text-xl grid gap-6" data-aos="fade-up" data-aos-delay="300">
                        <p>
                            Os benefícios que nos tornaram referência no atendimento a mais de 30 mil médicos, agora estão disponíveis, e ainda melhores, para você, advogado.
                        </p>
                        <p>Preencha o formulário para entrarmos em contato.</p>
                    </div>
                </div>
                <div class="md:backdrop-blur-sm md:p-8">
                    @if(session('success'))
                        <div class="flex flex-col justify-center items-center gap-6">
                            <x-svg src="check.svg" class="max-w-full bg-secondary rounded-full" height="200" width="200" />
                            <div>Enviado com sucesso</div>
                        </div>
                    @else
                        <x-form
                            :action="route('contacts.store_tgl_suit')"
                            method="POST"
                            theme="outlined-dark"
                            class="grid gap-6"
                            data-aos="fade-up"
                            data-aos-delay="450">
                            <input type="hidden" name="contact_type_id" value="4" />
                            <x-input name="name" label="Nome Completo" />
                            <x-input name="email" label="E-mail" type="email"/>
                            <x-input name="phone" label="Celular" type="tel" data-mask="(99) 99999-9999" />
                            <x-input name="oab" label="OAB" />
                            <div class="flex justify-end">
                                <x-button theme="secondary" class="px-12">Enviar</x-button>
                            </div>
                        </x-form>
                    @endif
                </div>
            </div>
        </x-layout.container>
    </div>
</div>
<div>
    <x-layout.container padding>

    <div class="grid md:grid-cols-2 gap-12 place-items-center">
        @foreach([
            'Sem taxa de carregamento',
            'Taxa administrativa de 0,4% ao ano deduzida do rendimento',
            'Plano extensivo aos familiares',
            'Contribuição para crianças a partir de R$ 50,00',
            'Todo resultado é revertido para os participantes',
            'Até 12% de benefício fiscal em sua declaração de IR',
            ] as $item)
            <div class='flex flex-col items-center justify-center gap-y-6'>
                <x-svg src="icon.svg" class="w-16 h-16" />
                <div class="text-xl text-center md:w-2/3">{{ $item }}</div>
            </div>
        @endforeach
    </div>
    </x-layout.container>
</div>
@endpush

@section('title', 'TGL Suit')

@section('meta')
<x-head.meta
    title="Conheça o TGL Suit"
    :image="asset('static/img/pages/tgl-suit/logo.webp')"
    description='Os benefícios que nos tornaram referência no atendimento a mais de 30 mil médicos, agora estão disponíveis, e ainda melhores, para você, advogado.'
/>
@stop
