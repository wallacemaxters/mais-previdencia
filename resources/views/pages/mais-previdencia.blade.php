@php
$inputClasses = 'rounded-lg group-focus-within:border-yellow-600 text-gray-600';
@endphp

@extends('layouts.main')

@push('content')

<header class="bg-[url('/static/img/pages/mais-protecao/banner.webp')] bg-no-repeat bg-cover relative font-secondary">
    <div class="bg-gray-800/70 flex items-center min-h-screen">
        <x-layout.container :padding="true">
            <div class="grid grid-cols-1 md:grid-cols-2 relative gap-12 items-center">
                <div class="text-white grid gap-y-5 text-center md:text-left">
                    <x-svg src="mais-previdencia.svg" class="mb-5" :height="100" :width="400" class="max-w-full p-2"/>
                    <h2 class="md:text-4xl text-2xl font-bold uppercase">Curta a vida enquanto cuidamos do seu patrimonio</h2>
                    <p>Em comemoração ao Dia Nacional do Aposentado, celebrado em 24 de janeiro, nós da Mais Previdência iremos sortear 10 kits viagem personalizados!</p>
                    <p>Para participar, você precisa preencher os campos ao lado, esperar o contato de um dos nossos consultores para fazer um Plano Mais Previdência Família para você, seus filhos (as), cônjuges e/ou demais atuais dependentes econômicos até o dia 24/02/2022 e aguardar.</p>
                    <p>O resultado será divulgado no início de março!</p>
                    <p>Participe!</p>
                </div>
                <div>
                    @if(session()->has('success'))
                    <div class="text-white flex flex-col items-center">
                        <x-svg src="check.svg" class="w-48 h-48 text-white bg-yellow-600 rounded-full" />
                        <p class="text-2xl mt-5 text-center">
                            Sua mensagem foi enviada com sucesso!
                        </p>
                    </div>
                    @else
                    <form action="{!! route('contacts.store_mais_previdencia') !!}" method="POST">
                        {!! $honeyPot !!}
                        @csrf
                        <header class="text-center text-white mb-5">
                            <h2 class="text-3xl md:text-4xl mb-2 font-bold">Preencha os seus dados</h2>
                            <p>Entraremos em contato</p>
                        </header>
                        <section class="grid gap-y-3 auto-rows-min text-yellow-600">
                            <x-input type="text" name="name" placeholder="Nome" required :class="$inputClasses" />
                            <x-input type="email" name="email" placeholder="E-mail" required :class="$inputClasses" />
                            <x-input type="tel" data-mask="(99)99999-9999" name="phone" required placeholder="Telefone" :class="$inputClasses" />
                            <x-button color="bg-yellow-600/80 shadow hover:bg-black/50" required class="rounded-2xl w-full md:w-96 mx-auto ">Participe!</x-button>
                        </section>
                    </form>
                    @endif
                </div>
            </div>
        </x-layout.container>
    </div>
</header>

<x-layout.container :padding="true">
    <section class="flex flex-col md:flex-row flex-wrap justify-center">
        @foreach([
            'Zero taxa de carregamento',
            'Flexibilidade na escolha do valor de contribuição mensal, Diminuição da base de cálculo do IR em até 12% da renda anual tributável por meio das contribuições mensais',
            'Possibilidade de realizar contribuições voluntárias de caráter facultativo, periódicas ou não',
            'Taxa de administração de 0,6% ao ano',
            'Opção de resgates parciais para algum projeto de curto prazo a partir do 3º ano e maior rentabilidade sobre a gestão dos seus recursos',
            'Viabilidade de manutenção do plano mesmo se desligando da empresa vinculada',
            'Alternativa de contratar seguro de vida (morte e/ou invalidez) com preços mais atrativos que os de mercados',
        ] as $i => $text)
            <div class="w-full md:w-1/2 h-64 p-8 text-center" data-aos="fade-up" data-aos-delay="{!! $i * 150 !!}">
                <x-svg src="check.svg" class="w-16 h-16 mb-5 text-yellow-600 mx-auto my-6" />
                <p>{!! $text !!}</p>
            </div>
        @endforeach
    </section>
</x-layout.container>

@endpush

@section('meta')
<x-head.meta
    title="Mais Previdência"
    description="Em comemoração ao Dia Nacional do Aposentado, celebrado em 24 de janeiro, nós da Mais Previdência iremos sortear 10 kits viagem personalizados!"
    :image="asset('static/img/mais-previdencia.webp')"
/>
@endsection
