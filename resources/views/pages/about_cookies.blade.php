@extends('layouts.default')

@section('content')

<x-layout.hero title="Avisos sobre o uso de Cookies" :image="asset('static/img/pages/about-cookies/cookies.jpg')" />

<x-layout.container :padding="true">
    <div class="post-content">
        <h2 class="uppercase">O QUE É UM COOKIE?</h2>
        <p>Cookie é um pequeno arquivo instalado no seu dispositivo móvel ou computador quando um navegador de internet é
            usado. Os cookies servem a muitos propósitos. Eles podem ajudar um site a lembrar suas preferências e oferecer
            anúncios direcionados ou conteúdo personalizado.</p>
        <p>Quando você visita um site novamente ou navega em outro que reconhece um cookie que baixou anteriormente, seu
            dispositivo se comunica com o site e este pode ler as informações contidas nesse cookie.</p>
        <p>O uso de cookies é uma prática normal entre e os websites na internet, sendo que a maioria dos navegadores
            permite que um usuário aceite, recuse ou apague os cookies por meio das suas configurações.</p>
        <p>Assim, um cookie não dá acesso a seu computador ou revela informações além daquelas que você escolhe compartilhar
            conosco, sendo que o usuário do site tem total controle sobre o aceite do cookie.</p>
        <h2 class="uppercase">QUAIS TIPOS DE COOKIES A {{ config('site.title') }} UTILIZA</h2>
        <p>Conheça os tipos de cookies que podem ser inseridos em seus dispositivos:</p>

        <ol>
            <li>
                <p><span><strong>Cookies Essenciais/Obrigatórios:</strong></span> permitem a navegação no site e a
                    utilização de todas as suas aplicações, tal como acessar áreas seguras do site depois de realizar o
                    login. Esses cookies não deverão ser deletados de seu dispositivo, uma vez que sem eles alguns serviços
                    não poderão ser executados. Além disso, eles não armazenam dados pessoais.</p>
            </li>
        </ol>
        <p><br></p>
        <ol>
            <li>
                <p><span><strong>Cookies de desempenho:</strong></span> coletam dados estatísticos como fontes de tráfego, o
                    número de acessos, páginas e seções visitadas, buscas, duração da visita e a forma como os utilizadores
                    navegam para medirmos e melhorarmos o desempenho do nosso site. As informações recolhidas são anônimas e
                    recorremos a cookies de terceiros.</p>
            </li>
        </ol>
        <p><br></p>
        <ol>
            <li>
                <p><span><strong>Cookies de mídia social:&nbsp;</strong></span>facilitam o compartilhamento em redes
                    sociais, fornecem ferramentas para que você se conecte ao site e nos ajudam a entender melhor tanto o
                    público que visita o nosso site como a nossa performance nas divulgações de mídia social. Trata-se de
                    cookies de terceiros e as suas escolhas em relação a estes cookies são determinadas pelas plataformas de
                    mídia social nas quais você tem conta. Web beacons que são pequenas imagens (conhecidas como pixel tags
                    e clear GIFs) que utilizadas em combinação com cookies nos ajudam a identificar e analisar o
                    comportamento do usuário nas plataformas digitais.</p>
            </li>
        </ol>
        <p><br></p>
        <ol>
            <li>
                <p><span><strong>Cookies de Funcionalidade:&nbsp;</strong></span>relembram as preferências do usuário, de
                    forma que não seja necessário voltar a configurar o site cada vez que o visita. Dependendo do contexto,
                    estes cookies poderão armazenar certos tipos de dados pessoais, conforme necessário para oferecer a
                    funcionalidade a que se refere.</p>
            </li>
        </ol>
        <p><br></p>
        <ol>
            <li>
                <p><span><strong>Cookies de Segmentação:&nbsp;</strong></span>registram sua visita aos nossos e outros
                    sites, incluindo as páginas da web que você visitou e os links que seguiu. Usamos a segmentação para
                    fins de marketing direcionado e para mostrar a você conteúdos e anúncios pertinentes aos seus
                    interesses. Os cookies de segmentação se baseiam na identificação exclusiva do seu navegador e disposto
                    de internet, não armazenando diretamente informações pessoais. São colocados por terceiros, sendo estes
                    nossos parceiros de publicidade, se as configurações do seu navegador permitirem.</p>
            </li>
        </ol>
        <h2 class="page-subtitle">PARA QUAIS FINALIDADES UTILIZAMOS OS COOKIES?</h2>
        <p>Nós usamos os cookies para melhorar a sua experiência ao navegar em nosso site e personalizar o conteúdo que você
            recebe, como:</p>
        <ul>
            <li>
                <p>Lembrar as suas preferências, escolhas de privacidade, para economizar o seu tempo por não ter que
                    incluir novamente as suas preferências cada vez que você visitar o site da {{ config('site.title') }};</p>
            </li>
            <li>
                <p>Tornar a navegação mais fácil e permitir que as nossas páginas sejam exibidas corretamente;</p>
            </li>
            <li>
                <p>Verificar como os usuários interagem com o nosso site para que possamos fazer melhorias e torná-lo mais
                    fácil de usar;</p>
            </li>
            <li>
                <p>Analisar o desempenho do nosso site com base em dados anônimos relacionados com a sua navegação (por
                    exemplo, páginas visitadas e número de visitas);</p>
            </li>
            <li>
                <p>Adequar a publicidade e banners que o usuário vê em sites. Por exemplo, se você visitar o nosso site e
                    nos permitir colocar um cookie de publicidade comportamental, você poderá ver um dos nossos anúncios
                    quando navegar um outro site, como de um jornal. Nesse caso, o cookie pode ter sido usado para
                    direcionar a publicidade para você;</p>
            </li>
            <li>
                <p>Fornecer conteúdo e comunicações de marketing sob medida para seus interesses, com base nas informações
                    de sua visita ao nosso site;</p>
            </li>
            <li>
                <p>Disponibilizar aos parceiros de publicidade informações sobre a sua visita para que lhe mostrem anúncios
                    relevantes online; e</p>
            </li>
            <li>
                <p>Enviar às redes sociais (por exemplo, LinkedIn ou Facebook) informações sobre a sua visita aos nossos
                    sites.</p>
            </li>
        </ul>
        <p>Você pode desabilitar os cookies nas configurações do seu navegador. Porém, lembre-se de que alguns deles são
            necessários para possibilitar a utilização do nosso site.</p>

        <h2 class="page-subtitle">COMO CONTROLAR OU ELIMINAR OS COOKIES?</h2>
        <p>A instalação, permanência e existência de cookies em seu computador ou dispositivo depende de sua escolha,
            podendo ser removidos quando desejar. Para saber como remover os cookies do sistema, é necessário verificar a
            seção Ajuda ou Configurações do seu navegador (aquele que você utiliza para acessar a internet em seu
            dispositivo).</p>
        <p>A qualquer momento você pode excluir os cookies armazenados, configurar o navegador para que ele solicite
            aprovação antes de armazenar cookies ou impedir diretamente o armazenamento de cookies. Este procedimento é
            realizado de forma distinta em diferentes navegadores, sendo necessário fazê-lo em cada navegador que utilizar.
        </p>
        <p>Trazemos aqui os navegadores mais comuns utilizados pelos usuários com o direcionamento ao suporte a respeito de
            Cookies de cada um deles (disponível clicando no link).</p>
        <ul>
            <li>
                <a href="https://support.microsoft.com/pt-br/topic/excluir-e-gerenciar-cookies-168dab11-0753-043d-7c16-ede5947fc64d"
                    target="_blank">Microsoft Internet Explorer</a>
            </li>
            <li>
                <a href="https://support.microsoft.com/pt-br/microsoft-edge/excluir-cookies-no-microsoft-edge-63947406-40ac-c3b8-57b9-2a946a29ae09"
                    target="_blank">Microsoft Edge</a>
            </li>
            <li>
                <a href="https://support.google.com/chrome/answer/95647?hl=pt" target="_blank">Chrome</a>
            </li>
            <li>
                <a href="https://support.mozilla.org/pt-BR/products/firefox/protect-your-privacy/cookies"
                    target="_blank">Firefox</a>
            </li>
            <li>
                <a href="https://support.apple.com/kb/index?q=cookies&amp;src=globalnav_support&amp;type=organic&amp;page=search&amp;locale=pt_BR"
                    target="_blank">Safari</a>
            </li>
            <li>
                <a href="https://help.opera.com/en/latest/web-preferences/#cookies" target="_blank">Opera</a>
            </li>
        </ul>
        <h2 class="page-subtitle">DÚVIDAS?</h2>
        <p>Informações sobre o tratamento de dados pessoais, bem como sobre o exercício de direito dos titulares estão
            descritas em nosso Aviso de Privacidade.</p>
    </div>
</x-layout.container>


@stop
