@extends('layouts.default')

@push('head')
<link rel="preload" href="{!! asset('static/videos/pages/banner_home.webm') !!}" as="video">
@endpush

@section('content')
    <div class="bg-primary relative">
        <x-layout.video-background :src="asset('static/videos/pages/banner_home.webm')" class="mix-blend-multiply" />
        <div class="overfow-hidden relative" id="banner">
            <x-layout.container>
                <article class="w-3/4 h-screen  mx-auto flex flex-col justify-center items-center relative z-10">
                    <p class="text-3xl text-center text-white font-light mb-12 2xl:mb-20">
                        Nós temos as melhores soluções<br>
                        para <span class="typer-text text-gray-300">você</span>
                    </p>
                    <h1 class="text-xs opacity-0 absolute">{{ config('site.title') }}</h1>
                    <div>
                        <footer class="justify-center flex">
                            <x-button
                                class="2xl:px-16 2xl:py-5 lg:px-12 lg:py-4 bg-opacity-70 shadow-lg"
                                href="#o-que-fazemos-e-porque-nos">SAIBA MAIS</x-button>
                        </footer>
                    </div>
                    <a href="#o-que-fazemos-e-porque-nos" class="z-10 absolute bottom-6 lg:bottom-32 animate-bounce 2xl:w-5 w-4">
                        <img alt="mouse" title="role para baixo" loading="eager" src="{{ asset('static/svg/mouse.svg') }}" class="w-full" height="40" width="20">
                    </a>
                </article>
            </x-layout.container>
        </div>
    </div>
    <div id="o-que-fazemos-e-porque-nos" class="bg-white overflow-hidden">
        <x-layout.container :padding="true">
            <x-layout.bordered-title data-aos="fade-right" >
                <x-slot name="title"><h2>O que fazemos e por que nos escolher?</h2></x-slot>
            </x-layout.bordered-title>
            <div class='grid auto-rows-auto gap-32 2xl:mx-16 lg:mx-32'>
                @foreach ($introBlocks as $key => $item)
                    <div class="flex flex-col-reverse md:flex-row gap-16 justify-center items-center">
                        <div
                            class="flex-1"
                            data-aos="{{ $key % 2 == 0 ? 'fade-left' : 'fade-right' }}"
                            data-aos-delay="400">
                            <h3 class="2xl:text-2xl lg:text-xl font-light text-third">{{ $item['title'] }}</h3>
                            <x-layout.underline class="mb-2" />
                            <h4 class='2xl:text-3xl text-2xl mb-5'>{{ $item['subtitle'] }}</h4>
                            <div class='2xl:text-lg lg:text-md text-justify text-gray-700 whitespace-pre-line'>{!! $item['text'] !!}</div>
                            <div
                                class="mt-5 flex justify-center lg:justify-start"
                                data-aos="{{ $key % 2 == 0 ? 'fade-left' : 'fade-right' }}"
                                data-aos-delay="800">
                                <x-button :href="$item['url']" class="2xl:text-xl rounded-sm shadow-lg gap-2">
                                    <span>
                                        Saiba Mais
                                    </span>
                                    <x-svg src="play.svg" height="30" width="30" class="h-full w-4 2xl:w-6"  />
                                </x-button>
                            </div>
                        </div>
                        <div
                            data-aos="{{ $key % 2 == 0 ? 'fade-right' : 'fade-left' }}"
                            @class([
                                'w-1/3',
                                '2xl:h-48 lg:h-40 h-32',
                                'flex items-center justify-center',
                                'relative',
                                'bg-gradient-to-r from-primary to-secondary',
                                'lg:order-first' => $key % 2 == 0]) data-aos="fade-up">
                            <x-layout.video-background
                                :src="asset('static/videos/pages/' . $item['video'])"
                                fit="object-contain"
                                class="bg-white mix-blend-screen w-full" />
                        </div>
                    </div>
                @endforeach
            </div>
        </x-layout.container>
    </div>
    <div
        class="relative overflow-hidden bg-gradient-to-br from-primary to-secondary hover:from-primary-400"
        id="deixa-que-a-gente-cuida-pra-voce">
        <x-layout.video-background
            data-rellax-speed="-2"
            :src="asset('static/videos/pages/banner_home.webm')"
            height="h-[32rem]"
            class="rellax mix-blend-multiply"
        />
        <div class="relative z-10 h-96 flex flex-col items-center justify-center">
            <h3 class="text-center 2xl:text-4xl lg:text-3xl font-light text-white mb-12">Deixa que a gente cuida para você!</h3>
            <x-button class="gap-2 bg-opacity-50" :href="route('products.index')">
                <span>
                    Soluções
                </span>
                <x-svg src="play.svg" height="25" width="25" class="2xl:w-6 w-4 2xl:h-6 h-4" />
            </x-button>
        </div>
    </div>

    <div>
        <x-layout.container :padding="true" class="relative z-10">
            <x-layout.bordered-title>
                <x-slot name="title">
                    <h2>Nossas Soluções</h2>
                </x-slot>
                <p>Conheça mais sobre como podemos ajudar</p>
            </x-layout.bordered-title>
            <x-layout.slider>
                @foreach($products as $key => $item)
                    <div class='w-full lg:w-1/4 flex-shrink-0 flex justify-center py-10 px-5'>
                        <x-products.small-card :product="$item" class='w-full h-full' />
                    </div>
                @endforeach
            </x-layout.slider>
        </x-layout.container>
    </div>

    @unless($employees->empty())
    <div id="nossa-equipe">
        <x-layout.container :padding="true">
            <x-layout.bordered-title>
                <x-slot name="title">
                    <h2>Nosso Time</h2>
                </x-slot>
                <p class="text-gray-500">Esses são os profissionais que ajudam a construir nossa história.</p>
            </x-layout.bordered-title>

            <x-layout.slider>
                @foreach ($employees as $employee)
                <div class='lg:w-1/3 2xl:px-6 lg:px-4 w-full shrink-0 pt-2 pb-6'>
                        <x-employees.card :employee="$employee" class="w-full" />
                    </div>
                @endforeach
            </x-layout.slider>
        </x-layout.container>
    </div>
    @endif

    <div class="bg-white hidden">
        <x-layout.container :padding="true">
            <x-layout.bordered-title>
                <x-slot name="title">
                    <h2>Esses são nossos parceiros</h2>
                </x-slot>
                <p>Acreditamos no poder dos relacionamentos</p>
            </x-layout.bordered-title>
            <ul class="flex flex-wrap justify-center lg:flex-row lg:flex-nowrap gap-12">
                @foreach ($partners as $k => $item)
                    <li class="w-64 filter grayscale hover:grayscale-0" data-aos="fade-up" data-aos-delay="{{ $k * 150 }}">
                        <img
                            alt="{{ $item }}"
                            loading="lazy"
                            width="120"
                            height="40"
                            src="{!! asset("static/img/pages/home/parceiros/{$item}") !!}" alt="{{ $item }}">
                    </li>
                @endforeach
            </ul>
        </x-layout.container>
    </div>

    @unless($newPosts->empty())
    <section>
        <x-layout.container :padding="true">
            <x-layout.bordered-title>
                <x-slot name="title">
                    <h2>Novidades</h2>
                </x-slot>
                <p>Aqui você tem artigos que ajudam no conhecimento sobre nosso produtos e serviços</p>
            </x-layout.bordered-title>

            <div class="grid lg:grid-cols-3 gap-12">
                @foreach($newPosts as $item)
                    <x-posts.card :post="$item"></x-posts.card>
                @endforeach
            </div>
        </x-layout.container>
    </section>
    @endunless
    <x-layout.contact-info></x-layout.contact-info>
@endsection
