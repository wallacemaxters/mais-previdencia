@extends('layouts.default')

@section('title', 'Aviso sobre privacidade')


@section('content')

<div class="bg-white">

    <x-layout.hero title="AVISO DE PRIVACIDADE TGL CONSULTORIA" :image="asset('static/img/pages/about-cookies/cookies.jpg')"  />

    <x-layout.container :padding="true">
        <div class="post-content">
 <p>Este Aviso se aplica, em geral, a todos clientes, usuário e outros titulares de dados que venham a utilizar
            dos serviços oferecidos pela TGL, incluindo titular de dados do site ou outros meios que possam ser operados
            pela TGL, e resume como os dados pessoais poderão ser coletados, produzidos, receptados, classificados,
            utilizados, acessados, reproduzidos, transmitidos, distribuídos, arquivados, armazenados, eliminados,
            avaliados, controlados, modificados, transferidos, incluindo as informações de identificação pessoal, de
            acordo com as bases legais aplicáveis e todas as leis de privacidade e proteção de dados vigentes.</p>
        <p>Para entender nossas práticas em relação aos dados pessoais e como vamos tratá-los, leia o texto abaixo.
            Estamos empenhados em tratá-lo com respeito e transparência.</p>
        <p>Caso tenha alguma dúvida com relação ao presente Aviso de Privacidade, pedimos que entre em contato conosco
            pelo e-mail <a href="mailto:dpo@grupotgl.com.br" class="link">dpo@grupotgl.com.br</a></p>

        <hr />
        <h3 class="page-subtitle">Índice</h3>
        <ul class="ol">
            @foreach([
                'Quem somos',
                'Como e quando coletamos dados do titular de dados',
                'Dados pessoais que coletamos e por que os usamos',
                'Legitimo Interesse',
                'Compartilhamento de dados pessoais',
                'Coleta de Dados pessoais por meio de "Cookies"',
                'Provedores de Serviços',
                'Transferência internacional de dados pessoais',
                'Por quanto tempo mantemos seus dados?',
                'Segurança dos dados',
                'Seus direitos como titular de dados pessoais',
                'Encarregado de Proteção de Dados (DPO)',
                'Glossário',
                'Atualização da Política de Privacidade',
            ] as $item)
                <li >
                    <a class="text-primary" href="#{!! Str::slug($item) !!}">{!! $item !!}</a>
                </li>
            @endforeach
        </ul>
        <h3 id="quem-somos" class="page-subtitle">1. Quem somos</h3>
        <p>A TGL Consultoria atua desde 2004 no mercado segurador, sempre buscando excelência no atendimento e levando
            soluções e diferenciais a nossos clientes e parceiros. Para isso, promovemos soluções personalizadas em
            serviços de consultoria na área de seguros, previdência complementar e demais benefícios, preservando um
            relacionamento de longo prazo, garantindo a integridade do cliente, de sua família e de seu patrimônio.</p>
        <p>A TGL é um "controlador de dados" para os fins da Lei de Proteção de Dados Pessoais ("LGPD”), Lei
            nº13.709/2018. Isso significa que somos responsáveis e controlamos o tratamento de dados pessoais que são
            fornecidos ou coletados.</p>
        <h3 id="como-e-quando-coletamos-dados-do-titular-de-dados" class="page-subtitle">2. Como e quando coletamos dados do titular de dados</h3>
        <p>A TGL, como Controladora de dados, tem como objetivo fornecer uma abordagem clara, honesta e transparente
            sobre como e quando pode coletar e usar dados pessoais.</p>
        <p>O titular de dados fornece informações diretamente a TGL quando se registra para um evento (uma palestra, por
            exemplo), quando preenche seus dados no site ou formulário, quando informar o interesse na aquisição de
            algum serviço, quando acessa um link ou conteúdo nos canais de comunicação da TGL.</p>
        <p>Isso inclui quando o titular de dados faz contato telefônico, visita o site ou entra em contato por e-mail ou
            mensagem direta nos canais de mídia social.</p>
        <p>O titular de dados fornece dados pessoais também quando interage com a TGL em plataformas de mídia social
            como Facebook, WhatsApp, Instagram, YouTube ou LinkedIn, oportunidade que a TGL poderá coletar dados
            pessoais sobre o titular de dados.</p>
        <p>Neste caso, as informações que a TGL recebe depende das preferências de privacidade definidas pelo titular de
            dados em cada plataforma e da política de privacidade de cada plataforma. Para saber como alterar suas
            configurações nessas plataformas, consulte as orientações do provedor da respectiva plataforma.</p>
        <p>Em algumas ocasiões, o titular de dados será avisado acerca dos respectivos dados pessoais que poderão ser
            coletados pela TGL, podendo consentir pelo fornecimento ou não dessas informações, sendo que também terá
            ciência das consequências de sua decisão, que poderá impedir a utilização de alguma funcionalidade do site,
            ou prestação de serviço.</p>
        <p>A TGL também recebe dados pessoais compartilhados por parceiros com os quais mantém contratos, com o objetivo
            de ofertar e prestar serviços conforme oportunidade identificada pelo parceiro. Neste caso, a TGL irá
            comunicar diretamente o titular de dados a respeito da fonte do compartilhamento de dados.</p>
        <h3 id="dados-pessoais-que-coletamos-e-por-que-os-usamos" class="page-subtitle">3. Dados pessoais que coletamos e por que os usamos</h3>
        <p>Quando o cliente ou titular de dados contata a TGL por telefone, correio, pessoalmente ou online, a TGL
            poderá coletar informações, aqui referidas como “dados pessoais”.</p>
        <p>Isso pode incluir seu nome, endereço, e-mail, número de telefone, data de nascimento, estado civil, número de
            Identificação Profissional (“CRM” e “OAB”, por exemplo), profissão, localização geográfica, Endereço de IP,
            Cookies e dados de uso, aparelho utilizado, detalhes de sua educação e carreira, porque está interessado na
            TGL e outras informações que o cliente ou titular de dados pode optar por fornecer para a contratação de um
            serviço.</p>
        <p>A Lei Geral de Proteção de Dados Pessoais reconhece que certos tipos de informações pessoais são mais
            sensíveis. Isso é conhecido como dado pessoal 'sensível' e trata informações que revelam origem racial ou
            étnica, crenças religiosas ou filosóficas e opiniões políticas, associação a sindicatos, dados genéticos ou
            biométricos, informações sobre saúde ou dados relativos à vida sexual de uma pessoa ou orientação sexual.
        </p>
        <p>A TGL coleta dados pessoais sensíveis dos clientes quando diretamente fornecidos, utilizando-se sempre da
            base legal do consentimento.</p>
        <p>A TGL poderá usar os respectivos Dados Pessoais para:</p>
        <p>- Contatar cliente, ou possível cliente: quando um parceiro com a qual o titular de dados mantém um contrato
            vigente aponta a o interesse do titular na contratação de algum serviço e informa seus dados cadastrais,
            utilizaremos essas informações para entrar em contato com o possível cliente.</p>
        <p>- Verificar identidade: Podemos usar os Dados Pessoais para verificar a identidade do cliente ou titular de
            dados conforme o cadastro realizado.</p>
        <p>- Operar, avaliar e melhorar a nosso negócio, incluindo a anonimização e a análise de dados.</p>
        <p>- Monitorar e avaliar os projetos e atividades da TGL: podemos usar os dados pessoais coletados para melhorar
            a entrega atual e futura do nosso serviço.</p>
        <p>- Enviar materiais de comunicação: Dados de cadastro poderão ser utilizados em eventual ação de marketing
            visando à oferta de produtos ou divulgação de eventos, a propagação de notícias sobre a TGL e outras
            novidades. O cliente ou titular de dados poderá optar por não receber nossos materiais de marketing, podendo
            cancelar o referido recebimento a qualquer momento. Para mais informações sobre tal cancelamento, entrar em
            contato com <a>dpo@TGL.com.br</a></p>
        <p>- Responder a uma solicitação: para fornecer os serviços ou informações que o cliente ou titular de dados
            solicitou, por exemplo, em resposta a uma consulta ou reclamação.</p>
        <p>- Desenvolver site, software e aplicativo: podemos usar os dados pessoais coletados para ajudar a fornecer
            melhor acesso, personalizar a experiência do cliente e melhorar e desenvolvê-la ainda mais. Isso nos ajudará
            a direcionar as comunicações de maneira focada, eficiente e econômica, ajudando-nos a reduzir as chances do
            recebimento de comunicações inadequadas ou irrelevantes.</p>
        <p>- Fornecer suporte e receber feedback: Podemos solicitar os Dados Pessoais para prestar suporte ao cliente e
            titular de dados. No caso de menores, seus dados também poderão ser utilizados para prestar informações aos
            respectivos responsáveis legais.</p>
        <p>- Perfil e análise: Queremos melhorar a forma como conversamos com o cliente ou possível cliente. Para fazer
            isso, algumas vezes usamos métodos de perfil e triagem para entender melhor nosso cliente, suas preferências
            e necessidades para fornecer uma melhor experiência.</p>
        <p>- Cumprir de ordem legal ou judicial: Podemos usar os Dados Pessoais para cumprir com leis aplicáveis ou,
            ainda, para responder a um processo legal, bem como para proteger os direitos, propriedade ou segurança da
            TGL, nossos titular de dadoss e/ou o público em geral.</p>
        <p>- Conformidade legal, regulatória e tributária: quando estamos sujeitos a uma obrigação legal, podemos tratar
            suas informações pessoais para cumprir essa obrigação.</p>
        <p>Ainda, considerando a possibilidade o Titular de dados interagir com a TGL demonstrando o interesse em
            trabalhar conosco:</p>
        <p>- Podemos tratar os dados pessoais do titular de dados se for enviado currículo via e-mail, sendo que os
            dados serão utilizados para fim de avaliar sua adequação e respondê-lo.</p>
        <p>- Outras finalidades em geral (ex.: pesquisa interna ou de mercado, análises, segurança). De acordo com as
            leis aplicáveis, utilizaremos os seus Dados Pessoais, dados de geolocalização, quantidade, mapeamento do
            comportamento e frequência de uso do site para outras finalidades gerais de negócio, conduzir pesquisas
            internas ou de mercado e medir a efetividade de nossas ações. Também usamos seus dados pessoais para
            gerenciamento e operação de nossas comunicações, TI e sistemas de segurança, todavia tais dados serão
            utilizados de forma consolidada e, na medida do possível, anonimizada para garantir a segurança do titular
            de dados.</p>
        <p>Para outras finalidades, a TGL fornecerá um aviso específico no momento da coleta, ou, de outro modo,
            conforme autorizado ou exigido por lei.</p>
        <h3 class="page-subtitle" id="legitimo-interesse">4. Legitimo Interesse</h3>
        <p>A lei permite que os dados pessoais sejam coletados e usados se for necessário para um legitimo interesse da
            organização, exceto no caso de prevalecerem direitos e liberdades fundamentais do titular que exijam a
            proteção dos dados pessoais.</p>
        <p>Em muitas situações, a melhor abordagem para a TGL é tratar dados pessoais por causa do legitimo interesse,
            em vez de solicitar consentimento. Se o Titular de dados quiser alterar o uso de seus dados pessoais para
            atividades de marketing e recebimento de conteúdo, poderá fazê-lo a qualquer momento entrando em contato com
            nossa equipe pelo e-mail <a href="mailto:dpo@grupotgl.com.br">dpo@grupotgl.com.br</a>.</p>

        <h3 id="compartilhamento-de-dados-pessoais" class="page-subtitle">
            5. Compartilhamento de dados pessoais
        </h3>
        <p>A TGL somente usará os dados pessoais para os fins para os quais foram coletados. Em nenhuma circunstância,
            venderemos ou compartilharemos os dados pessoais do Titular de dados com terceiros para seus próprios fins,
            e o Titular de dados não receberá marketing de outras empresas ou outras organizações como resultado do
            fornecimento de seus dados à TGL.</p>
        <p>A TGL poderá compartilhar os dados pessoais apenas para os seguintes fins:</p>
        <ul class="ul">
            <li>
                <p>Seguradoras: considerando a prestação de serviço realizada pela TGL na qualidade de corretora de
                    seguros, iremos compartilhar os dados pessoais com as seguradoras para cotação de seguros, proposta,
                    emissão de endossos, envio de informações para regulação de sinistros e, demais informações
                    necessárias para regular execução do contrato mantido pelo cliente junto a Seguradora,</p>
            </li>
            <li>
                <p>Parceiros: quando um parceiro indicar o possível cliente, informaremos posteriormente a ele se houve
                    a contratação de um serviço, no termo do acordo comercial firmado.</p>
            </li>
            <li>
                <p>Fornecedores terceirizados: podemos precisar compartilhar suas informações com prestadores de
                    hospedagem de dados ou prestadores de serviços que nos ajudem a fornecer nossos serviços, projetos
                    ou atividades. Esses fornecedores agirão apenas de acordo com nossas instruções e estão sujeitos a
                    análise pré-contratual e obrigações contratuais que contêm cláusulas de proteção de dados.</p>
            </li>
            <li>
                <p>Quando legalmente exigido: atenderemos a solicitações quando a divulgação é exigida por lei, por
                    exemplo, podemos compartilhar dados pessoais com o governo para fins de investigação tributária ou
                    às agências policiais para a prevenção e detecção de crimes.</p>
            </li>
        </ul>
        <h3 class="page-subtitle" id="coleta-de-dados-pessoais-por-meio-de-cookies">6. Coleta de dados por meio de "Cookies"</h3>
        <p>Cookies são arquivos de texto que contêm pequenas quantidades de informações que são baixadas no seu
            computador ou dispositivo móvel quando você visita uma determinada página da internet. Os cookies
            armazenados são então enviados de volta à página de origem em cada visita ou a outra página que reconhece
            esse cookie.</p>
        <p>Os cookies são amplamente utilizados para fazer um site funcionar, ou para trabalhar com mais eficiência,
            além de fornecer informações aos proprietários de um site.</p>
        <p>Utilizamos cookies para:</p>
        <ul class="ul">
            <li>
                acompanhar o fluxo de tráfego e os padrões de navegação em conexão com o nosso Site;
            </li>
            <li>
                <p>entender o número total de visitantes do Site continuamente e os tipos de navegadores da Internet
                    (por exemplo, Firefox, Chrome, Safari) e sistemas operacionais (por exemplo, Windows ou macOS)e
                    tipos de dispositivos (tablet, mobile, desktop) usados por nossos visitantes;</p>
            </li>
            <li>
                <p>monitorar o desempenho geral do Site e melhorá-lo continuamente;</p>
            </li>
            <li>
                <p>ajudar o TGL a identificar e resolver erros que os visitantes encontram no Site;</p>
            </li>
            <li>
                <p>por meio do uso de remarketing, personalizar e aprimorar sua experiência on-line;</p>
            </li>
            <li>
                <p>permitir que o TGL anuncie dentro e fora do Site.</p>
            </li>
            <li>
                <p>Permitir que o TGL mensure a eficácia da sua publicidade com base nas ações que os titular de dadoss
                    realizam no Site.</p>
            </li>
        </ul>
        <p>Para saber mais, acesso nossa <u>Política de Cookies.</u></p>
        <h3 id="provedores-de-servicos" class="page-subtitle">7. Provedores de Serviços</h3>
        <p>Utilizamos plataformas digitais e serviços de terceiros e de parceiros que podem colocar cookies em seu
            navegador quando você interage digitalmente por meio deles com o TGL.</p>
        <p>Eles podem aplicar seus próprios cookies, web beacons e outras tecnologias para rastrear e coletar
            informações sobre você e suas atividades on-line em diferentes sites, sistemas, dispositivos e aplicativos.
            Essas plataformas e recursos tecnológicos nos permitem gerenciar nossos conteúdos, páginas, publicações,
            aplicativos, sistemas, etc. e fazer nossas ações de publicidade.</p>
        <p>Permitem também a esses fornecedores direcionar ações de marketing com base em seus interesses e histórico de
            navegação anterior.</p>
        <p>Recomendamos fortemente que o Titular de dados conheça as políticas desses fornecedores.</p>
        <p>Google Analytics, Google Adwords, WordPress, são exemplos de plataformas que utilizamos.</p>
        <p>Ao navegar nas diversas plataformas web, em páginas nossas ou de outros, é importante que o titular de dados
            saiba que redes de anúncios de terceiros podem coletar automaticamente dados, como seu endereço IP, seu
            provedor de serviços de Internet e seu navegador. Elas fazem isso usando cookies, beacons ou outras
            tecnologias.</p>
        <p>Vale lembrar que os avanços e as inovações tecnológicas permitem constantes melhorias na experiência das
            relações humanas e digitais, como é o caso dos assistentes virtuais, da aplicação de inteligência
            artificial, das atividades de remarketing, etc. Utilizaremos também esses recursos desde que suas aplicações
            estejam coerentes com os termos desta política.</p>
        <p>Nossos conteúdos podem fazer referência e conter links para conteúdo de terceiros.</p>
        <p>Esses destinos são mantidos por seus proprietários de forma totalmente independente e podem coletar
            informações sobre você. A TGL não tem qualquer controle sobre isso e não pode assumir qualquer
            responsabilidade sobre a política deles.</p>
        <p>Nossas páginas e publicações, nas diversas plataformas digitais, podem eventualmente ter espaço para que você
            publique comentários e conteúdo. Fique atento para o fato de que esses espaços são públicos e estão
            presentes em plataformas que normalmente são de terceiros. Suas publicações poderão ser lidas, coletadas e
            utilizadas por outras pessoas e organizações.</p>
        <p>Esses Provedores de Serviços terão acesso aos seus Dados Pessoais somente para executar essas tarefas em
            nosso nome e estamos obrigados a não as divulgar ou utilizá-las para qualquer outra finalidade.</p>
        <h3 class="page-subtitle">8. Transferências internacionais de dados pessoais</h3>
        <p>Podemos, em raras ocasiões, decidir usar os serviços de um fornecedor fora do Brasil. Isso significa que seus
            dados pessoais e/ou sensíveis poderão ser transferidos, tratados e armazenados fora do Brasil.</p>
        <p>Nessas circunstâncias, a TGL tomará medidas para implementar salvaguardas adequadas para proteger suas
            informações pessoais quando tratadas pelo prestador de serviço, como por exemplo, utilizar nas cláusulas
            contratuais padrão aprovadas pela Comissão Europeia, e acordos internacionais específicos de transferência
            de dados.</p>
        <h3 class="page-subtitle" id="por-quanto-tempo-mantemos-seus-dados">9. Por quanto tempo mantemos seus dados pessoais</h3>
        <p>O TGL tratará seus dados pelo período necessário para o cumprimento de obrigações legais, regulatórias e
            contratuais. Além disso, usaremos os dados coletados para continuar nossas atividades, aprimorar nossos
            serviços, para o exercício regular de direito em processos administrativos, judiciais e arbitrais e para as
            demais finalidades previstas neste Aviso de Privacidade.</p>
        <p>Caso o Titular de dados deixe de utilizar os serviços da TGL ou o visite de forma transitória o site e redes
            sociais, seus dados pessoais poderão ser mantidos para fins de auditoria e preservação de direitos, podendo
            ser excluídos, a pedido do Titular de dados, conforme disposto pela LGPD e pela Lei nº 12.965/2014, conforme
            aplicável, desde que já tenha decorrido o prazo legal prescricional relacionado às provas às quais os
            referidos registros e informações possam estar relacionados</p>
        <h3 class="page-subtitle" id="seguranca-de-dados">10. Segurança de dados</h3>
        <p>A TGL tem implementado medidas físicas, técnicas e organizacionais apropriadas para proteger os dados
            pessoais que tem, nos meios físicos e digitais, contra acesso, uso, alteração, destruição e perda
            inadequados.</p>
        <h3 id="seus-direitos-como-titular-de-dados-pessoais" class="page-subtitle">11. Seus direitos como titular de dados pessoais</h3>
        <p>A LGPD garante direitos aos Titulares dos Dados. Como Titular dos seus Dados Pessoais, o titular de dados
            pode nos fazer os seguintes requerimentos:</p>
        <p>- Confirmação da existência de tratamento;</p>
        <p>- Acesso aos seus dados pessoais;</p>
        <p>- Correção de dados incompletos, inexatos ou desatualizados;</p>
        <p>- Anonimização, bloqueio ou eliminação de dados desnecessários, excessivos ou tratados ilicitamente;</p>
        <p>- Portabilidade dos dados a outro fornecedor de serviço ou produto;</p>
        <p>- Eliminação dos dados pessoais tratados com o consentimento do titular;</p>
        <p>- Informação das entidades públicas e privadas com as quais o controlador realizou uso compartilhado de
            dados;</p>
        <p>- Informação sobre a possibilidade de não fornecer consentimento e sobre as consequências da negativa;</p>
        <p>- Revogação do consentimento;</p>
        <p>- Revisão das decisões tomadas exclusivamente com base em tratamento automatizado de dados pessoais;</p>
        <p>- Oposição a tratamento irregular.</p>
        <h3 id="encarregado-de-protecao-de-dados-dpo" class="page-subtitle">12. Encarregado de Proteção de Dados (DPO)</h3>
        <p>Para obter mais informações sobre como tratamos os seus dados pessoais, ou para que possamos ajudar a
            resolver qualquer problema ou consulta, fale diretamente conosco no e-mail abaixo. Nossa equipe terá prazer
            em ajudar a resolver quaisquer problemas, dúvidas ou preocupações que você possa ter.</p>
        <p>Encarregado (DPO)</p>
        <p>E-mail: <a class="link" href="mailto:dpo@grupotgl.com.br">dpo@grupotgl.com.br</a>.</p>

        <h3 id="glossario" class="page-subtitle">13. Glossário</h3>

        <p>Os termos abaixo são usados no Aviso. de Privacidade.</p>
        <p><strong>Controlador de dados</strong>: pessoa física ou jurídica, de direito público ou privado, a quem
            competem as decisões referentes ao Tratamento de Dados Pessoais. Neste caso o <strong>TGL</strong> é um
            controlador de dados.</p>
        <p><strong>Dado Pessoal</strong>: informação relacionada a pessoa física identificada ou identificável.</p>
        <p><strong>Dado Pessoal Sensível</strong>: dado pessoal sobre origem racial ou étnica, convicção religiosa,
            opinião política, filiação a sindicato ou a organização de caráter religioso, filosófico ou político, dado
            referente à saúde ou à vida sexual, dado genético ou biométrico, quando vinculado a uma pessoa física.</p>
        <p><strong>IP – (Endereço de Protocolo de Internet)</strong>: código atribuído a um terminal de uma rede para
            permitir sua identificação, definido segundo parâmetros internacionais.</p>
        <p><strong>LGPD</strong>: refere-se à Lei Geral de Proteção de Dados (Lei nº 13.709/2018)</p>
        <p><strong>Site</strong>: site do TGL que pode ser acessado por Titular de dados.</p>
        <p><strong>Titular de Dados Pessoais</strong>: pessoa física a quem se referem os dados pessoais que são objeto
            de tratamento.<strong>&nbsp;</strong></p>
        <p><strong>Tratamento</strong>: toda operação realizada com Dados Pessoais, como as que se referem a coleta,
            produção, recepção, classificação, utilização, acesso, reprodução, transmissão, distribuição, processamento,
            arquivamento, armazenamento, eliminação, avaliação ou controle da informação, modificação, comunicação,
            transferência, difusão ou extração.</p>
        <p><strong>Titular de dados</strong>: toda pessoa que visitar e acessar o Site.</p>

        <h3 class="page-subtitle" id="atualizacao-da-politica-de-privacidade">14. Atualização da Política de Privacidade</h3>
        <p>Esta <i>Política de Privacidade</i> pode mudar de tempos em tempos. Por exemplo, continuaremos atualizando-a
            para refletir novos requisitos legais. Visite esta página do Site para manter-se atualizado com as
            alterações.</p>
        <p>Esta versão foi atualizada em {!! date('d/m/Y', strtotime('2021-09-15')) !!}.</p>
        </div>
    </x-layout.container>
</div>
@stop
