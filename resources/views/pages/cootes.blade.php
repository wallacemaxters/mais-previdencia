@php
$inputClasses = 'rounded-lg group-focus-within:border-primary text-gray-600';
@endphp

@extends('layouts.main')

@push('content')

<section class="bg-[url('/static/img/pages/cootes/bg.webp')] bg-no-repeat bg-cover bg-fixed relative font-secondary">
    <div class="bg-gray-800/70 flex items-center min-h-screen py-20 md:py-12">
        <x-layout.container>
            <div class='flex flex-col md:flex-row justify-center gap-12 items-center md:mb-12 2xl:mb-20'>
                <img src="{{ asset('static/img/pages/cootes/cootes.webp') }}" class="w-40" />
                <img src="{{ asset('static/img/pages/cootes/pro-doctor.webp') }}" class="w-40" />
                <x-svg src="logo-white.svg" class="text-white w-40" />
            </div>
            <div class="grid grid-cols-1 md:grid-cols-2 relative gap-16 items-center">
                <div class="text-white text-center md:text-left">
                    <h2 class="md:text-3xl text-2xl font-bold uppercase mb-5">
                        Palestra para os Cooperados COOTES via <a href="https://zoom.us/">ZOOM</a>
                    </h2>
                    <div class="text-xl grid gap-y-5">
                        <p>Planejamento Contábil, Financeiro e de Aposentadoria do Médico.</p>
                        <p>Dia 30/03 às 20:00</p>
                    </div>
                </div>
                <div>
                    @if(session()->has('success'))
                    <div class="text-white flex flex-col items-center">
                        <x-svg src="check.svg" class="w-48 h-48 text-white bg-secondary rounded-full" />
                        <p class="text-2xl mt-5 text-center">
                            Sua mensagem foi enviada com sucesso!
                        </p>
                    </div>
                    @else
                    <x-form :action="route('contacts.store_cootes')" method="POST">
                        {!! $honeyPot !!}
                        @csrf
                        <header class="text-center text-white mb-5">
                            <h2 class="text-2xl md:text-3xl mb-2 font-bold">Preencha o formulário para se cadastrar na palestra</h2>
                            <p>Entraremos em contato em breve</p>
                        </header>
                        <section class="grid gap-y-1 auto-rows-min text-primary">
                            <x-input type="text" name="name" placeholder="Nome" required :class="$inputClasses" />
                            <x-input type="email" name="email" placeholder="E-mail" required :class="$inputClasses" />
                            <x-input type="tel" data-mask="(99)99999-9999" name="phone" required placeholder="Telefone" :class="$inputClasses" />
                            <x-button color="secondary" required class="rounded-2xl w-full md:w-96 mx-auto ">Participe!</x-button>
                        </section>
                    </x-form>
                    @endif
                </div>
            </div>
        </x-layout.container>
    </div>
</section>

@endpush

@section('meta')
<x-head.meta
    title="Cootes"
    description="Palestra para cooperados Cootes com o tema: Planejamento Contábil, Financeiro e de Aposentadoria do Médico. No dia 30/03 às 20:00."
    :image="asset('static/static/img/pages/cootes/cootes.webp')"
/>
@endsection
