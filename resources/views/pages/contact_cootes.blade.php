@extends('layouts.main')

@push('content')

<div class="bg-image-glass bg-no-repeat bg-fixed min-h-screen flex flex-col justify-center">
    <x-layout.container :padding="true">
        <div class="flex justify-center mb-5">
            <img class="w-48" src="{{  asset('static/img/pages/cootes/cootes.webp') }}">
        </div>

        @if(session()->has('success'))
        <div class="text-white flex flex-col items-center p-5 bg-secondary/30 mb-5 rounded-md shadow-lg">
            <x-svg src="check.svg" class="w-48 h-48 text-secondary bg-white drop-shadow rounded-full" />
            <p class="text-2xl mt-5 text-center">
                Sua mensagem foi enviada com sucesso!
            </p>
        </div>
        @else
        <form
            enctype="multipart/form-data"
            method="POST"
            action="{{ route('contact_datasheets.store') }}"
            class="flex flex-col items-center text-white bg-gradient-to-t from-primary/20 to-secondary/30 md:shadow-lg md:px-12 md:py-6">
            <x-layout.datasheet :contact-datasheet-type-id="1" />
        </form>
        @endif
    </x-layout.container>
</div>
@endpush


@section('meta')
<x-head.meta
    title="Formulário de Atendimento COOTES"
    description="NOTA: A data de apresentação do relatório é de até 30 dias da data da entrega da documentação completa pelo cooperado.!"
    :image="asset('static/img/pages/cootes/cootes.webp')"
/>
@endsection
