@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
<div style="text-align: center">
    <img height="100" src="{{ asset($logo ?? config('site.logo')) }}">
</div>
{{ config('site.title') }}
@endcomponent
@endslot

{{-- Body --}}
{{ $slot }}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

@slot('footer')
@component('mail::footer')
{{ date('Y') }} {{ config('site.title') }}. @lang('Todos os direitos reservados.')
@endcomponent
@endslot
@endcomponent
