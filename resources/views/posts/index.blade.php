@extends('layouts.default')

@section('title', 'Blog')

@section('content')

<x-layout.hero
    title="Blog"
    :video="asset('static/videos/pages/blog.mp4')"
    subtitle="Confira as novidades que a {{ config('site.title') }} preparou para você!">
</x-layout.hero>

<x-layout.container :padding="true">
    <form class="flex items-center mb-5 py-5" method="GET" action="{!! url()->current() !!}#posts">
        <x-input label-class="mt-0 mb-0" placeholder="Digite um termo de pesquisa" name="q" :value="$searchTerm" />
        <x-button class="h-full">
            <i class="fas fa-search"></i>
        </x-button>
    </form>

    @if($posts->isEmpty())
        <p class='bg-white p-5 shadow-md'>Nenhuma publicação encontrada com o termo <q>{{ $searchTerm }}</q> </p>
    @else
    <section class="grid md:grid-cols-3 grid-flow-rows gap-10" id="posts">
        @foreach($posts as $key => $item)
            <div data-aos="zoom-out" data-aos-delay="{{ 300 * ($key % 3) }}">
                <x-posts.card :post="$item"></x-post.card>
            </div>
        @endforeach
    </section>
    @endif
</x-layout.container>

@endsection



@section('meta')
<x-head.meta
    title="Blog"
    description="Confira as novidades que a TGL preparou para você!"
/>
@stop
