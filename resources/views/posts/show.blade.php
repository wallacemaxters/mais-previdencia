    @extends('layouts.default')

@section('title', $post->title)

@section('content')

<div class="bg-third bg-cover bg-image-glass">
    <x-layout.container :padding="true">
        <article class="bg-white shadow-lg">
            <header class="md:px-16 px-7">
                <div class="flex flex-wrap items-center md:h-auto h-48 justify-between md:flex-row py-5">
                    <a
                        href="{!! $post->user && $post->user->employee ? route('employees.show_by_slug', ['slug' => $post->user->employee->slug]) : '#' !!}"
                        class="flex md:flex-row flex-col items-center md:mr-5 md:mb-0 md:w-auto w-full">
                        <img
                            lazy="loading"
                            src="{{ $post->user->employee->image_url ?? asset('static/svg/icon.svg') }}"
                            width="50"
                            height="50"
                            alt="{{ $post->title }}"
                            loading="lazy"
                            class="h-12 shadow-lg w-12 object-cover object-top rounded-full mr-3">

                        <div class="flex flex-col">
                            <span class="text-sm font-bold">{!! $post->user->employee->full_name ?? config('site.title') !!}</span>
                            <span class="text-xs text-secondary">{!! $post->user->employee->position ?? 'Marketing' !!}</span>
                        </div>
                    </a>
                    @if($post->posted_at)
                    <time
                        class="text-gray-400 md:w-auto w-full text-center">{{ $post->posted_at->format('d/m/Y') }}</time>
                    @endif
                </div>
            </header>
            <x-layout.hero :image="$post->cover_url">
                <x-slot name="title">
                    <span class="text-4xl leading-none block">{{ $post->title }}</span>
                </x-slot>
            </x-layout.hero>

            <section class="post-content py-7 md:px-16 px-7">{!! $post->content !!}</section>
        </article>
    </x-layout.container>
</div>

@endsection


@section('meta')
<x-head.meta
    type="article"
    :title="$post->title"
    :description="$post->excerpt"
    :image="$post->cover_url"
    :published-time="$post->posted_at"
    :modified-time="$post->updated_at" />
@stop
