@extends('layouts.main')

@section('meta')
<x-head.meta
    :title="$form->name"
    :description="'Envie seus dados e documentos digitalmente para TGL Consultoria Financeira'"
/>
@stop

@push('content')

<div class="text-neutral-600 bg-fixed bg-no-repeat bg-fixed min-h-screen bg-gradient-to-br from-secondary to-primary animate-bg-size">
    <x-layout.container :padding="true">
        <div class="flex justify-center">
            <x-svg src="logo.svg" class="w-64 text-white" />
        </div>
        <x-layout.bordered-title :title="$form->name" class="text-white" />
        @if(session()->has('success'))
        <div class="flex flex-col items-center p-5 bg-secondary/30 mb-5 rounded-md shadow-lg">
            <x-svg src="check.svg" class="w-48 h-48 text-secondary bg-white drop-shadow rounded-full" />
            <p class="text-2xl mt-5 text-center text-white">
                Sua mensagem foi enviada com sucesso!
            </p>
        </div>
        @else
        <x-form
            theme="outlined"
            enctype="multipart/form-data"
            method="POST"
            class="w-full grid gap-12 bg-white px-10 py-8 shadow-lg">
            @foreach($form->groups as $group)
                <x-forms.form-group :form-group="$group" />
            @endforeach
            @csrf
            <div class="flex justify-end">
                <x-button theme="secondary" size="small">ENVIAR</x-button>
            </div>
        </x-form>
        @endif

    </x-layout.container>
</div>
@endpush
